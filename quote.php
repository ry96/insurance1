<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="styles.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
 <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
    @media screen and (min-width: 992px) {
        .lgh {
          max-width: 1250px; /* New width for large modal */
          margin-left: 5px;
          min-height:850px;
        }
    }
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<?php
  require 'handle/db.php';
 
?>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Account</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
   
  <div class="row">
       
    <div class="container-fluid" style="max-width: 970px;">
        <div class="row no-gutters">
       
        <?php
        
        if(isset($_POST['name'])){
            $name  = $_POST["name"];
            $email = $_POST["email"];
            $phno  = $_POST["ph-no"];
            $vehicleClass = $_POST["vehicleClass"];
            $cover = $_POST["cover"];
            //$vehicleMake = $_POST["vehicleMake"]; 
            $vehicle_reg = $_POST["vehicle_reg"];
            $vehicleClass = trim(explode(".", $vehicleClass)[1]);
            $sql = "SELECT * FROM Product"; #where COVERAGE = '$cover' AND RISK_COVERED ='$vehicleClass'";
            if (isset($_POST["coverperiod"])){
              $coverperiod = $_POST["coverperiod"];
              }else{
                  $coverperiod = "";
              }
            if($result = mysqli_query($conn, $sql)){
               
                if(mysqli_num_rows($result) > 0){
              $coverperiod = $_POST["coverperiod"];
              $coverperiod = $_POST["coverperiod"];
                    echo '<div class="col-md-12 text-info h1 text-center">Quotation for vehicle registration <b> '.$vehicle_reg.'  </b> Coverage <b>'.$_POST['cover'].'</b> for a period of <b>'.$coverperiod.'</b>. Choose your prefered underwriter</div>'; 
                while($row = mysqli_fetch_array($result)){
                    if(isset($_POST['sum_insured'])){
                        $sum_insured = $_POST['sum_insured'];
                        $annualRate = $row["ANNUAL_RATES"]; 
                        $basicpremium = $annualRate /100 * $sum_insured; 
                    } else{
                        $basicpremium = 1000;
                    }
                    $basicpremium = strval($basicpremium);
                    echo '<div class="col-md-4 card secondary" style="padding:6px; padding:2px;" data-toggle="modal" data-target="#'.$row["PRODUCT_NAME"]. $row["UNDERWRITER"].'">',
                    '<img class="card-img-top" src="https://via.placeholder.com/150/000000/FFFFFF/?text='.$basicpremium.'" height="200" width="100%" alt="Card image cap">',
                    '<div class="card-body">',
                    '<p class="card-text h1 text-center">'.$row["UNDERWRITER"].'</p>',
                    '</div>',
                    '</div>',
                            '<!-- Modal -->',

                            '<div class="modal fade" id="'.$row["PRODUCT_NAME"]. $row["UNDERWRITER"].'" tabindex="-1" role="dialog" aria-labelledby="underwriterModalLabel" aria-hidden="true">',
                    '<div class="modal-dialog" role="document">',
                        '<div class="modal-content lgh1">',
                            '<div class="modal-header">',
                            '<h5 class="modal-title" id="exampleModalLabel">'.$row['PRODUCT_NAME'].'</h5>',
                            '<button type="button" class="close" data-dismiss="modal" aria-label="Close">',
                                '<span aria-hidden="true">&times;</span>',
                            '</button>',
                            '</div>',
                            '<div class="modal-body">',
                            '<!-- Page Container -->',
                            '<div class="w3-content w3-margin-top" style="max-width:1400px;">',

                                '<!-- The Grid -->',
                                '<div class="w3-row-padding">',
                                
                                '<!-- Left Column -->',
                                '<div class="w3-third">',
                                
                                    '<div class="w3-white w3-text-grey w3-card-4">',
                                    '<div class="w3-display-container">',
                                        '<img src="'.$row['PRODUCT_LOGO'] .'" style="width:100%" alt="Avatar">',
                                        '<div class="w3-display-bottomleft w3-container w3-text-black">',
                                        
                                        '</div>',
                                    '</div>',
                                    '<div class="w3-display-container">',
                                        '<img src="'.$row['PRODUCT_LOGO'] .'" style="width:100%" alt="Avatar">',
                                        '<div class="w3-display-bottomleft w3-container w3-text-black">',
                                        
                                        '</div>',
                                    '</div>',
                                    '<div class="w3-container">',
                                        
                                        '<div class="w3-twothird">',
                                    '</div>',
                                    '</div>',
                                    '</div><br>',

                                '<!-- End Left Column -->',
                                '</div>',

                                '<!-- Right Column -->',
                                '<div class="w3-twothird">',
                                
                                    '<div class="w3-container w3-card w3-white w3-margin-bottom">',
                                    '<div class="w3-container">',
                                        '<br><br>',
                                        '<h5 class="w3-opacity"><b>Risk Covered</b></h5>',
                                        '<h6 class="w3-text-teal"> '.$row["RISK_COVERED"].' </h6>',
                                        '<hr>',
                                        
                                        '<h5 class="w3-opacity"><b>Under Writer</b></h5>',
                                        '<h6 class="w3-text-teal"> '.$row["UNDERWRITER"].' </h6>',
                                        '<hr>',

                                        '<h5 class="w3-opacity"><b>Coverrage</b></h5>',
                                        '<h6 class="w3-text-teal"> '.$row["COVERAGE"].' </h6>',
                                        '<hr>',
                               

                                        '<h5 class="w3-opacity"><b>Basic Premium</b></h5>',
                                        '<h6 class="w3-text-teal"> '.$basicpremium.'</h6>',
                                        '<hr>',

                                        '<h5 class="w3-opacity"><b>Clauses</b></h5>',
                                        '<h6 class="w3-text-teal"> '.$row["CLAUSES"].' </h6>',                                 
                                        '<hr>',

                                        '<h5 class="w3-opacity"><b>Conditions and Waranties</b></h5>',
                                        '<h6 class="w3-text-teal"> '.$row["CONDITIONS_AND_WARRANTIES"].' </h6>',                                      
                                        '<hr>',

                                        '<div class="nav-item dropdown" >',
                                        '<!--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">',
                                          'Add Optional Benefits',
                                        '</a>-->',
                                        '<div class="dropdown-menu" aria-labelledby="navbarDropdown">',
                                      '<form action="handle/product_benefits.php" method="post" >',
                                        '<fieldset style="width:200px">',
                                          '<div class="form-group">',
                                            '<input type="text" id=""  name="prodopt1" class="form-control" placeholder="EG Access protector">',
                                          '</div>',
                                          '<div class="form-group">',
                                            '<input type="number" step="any" id=""  name="prodopt2" class="form-control" placeholder="Optional BeneMinimum Premium">',
                                          '</div>',
                                          '<div class="form-group">',
                                            '<input type="number" step="any" id="" name="prodopt3"  class="form-control" placeholder="Optional Benefits Rate(%)">',
                                          '</div>',
                                          '<div class="form-group">',
                                            '<input readonly type="text"  id="" name="prodopt4" value="'.$row["PRODUCT_IDENTIFIER"].'" class="form-control">',
                                          '</div>',
                                          '<button type="submit" class="btn btn-primary">Submit</button>',
                                        '</fieldset>',
                                      '</form>',
                                        '</div>',
                                      '</div>',
                                      '<hr>';
                                                                                                
                                        $additionalsql = 'SELECT * FROM Product_additional_benefits WHERE PRODUCT_IDENTIFIER = "'.$row['PRODUCT_IDENTIFIER'].'"'; 
                                        
                                        $OPTIONAL_ADDITIONAL_BENEFITS = [];
                                        $OP_BR = [];
                                        $OP_BMP = []; 
                                        if($result1 = mysqli_query($conn, $additionalsql)){
                                          if(mysqli_num_rows($result1) > 0){
                                            echo '<h5 class="w3-opacity"><b>Benefit</b></h5>',
                                                    '<table class="table table-dark">',
                                                      '<thead>',
                                                        '<tr>',
                                                          '<th scope="col">Name</th>',
                                                          '<th scope="col">Benefits Rate</th>',
                                                          '<th scope="col">Min Premium</th>',
                                                          '<th scope="col">Apply</th>',
                                                        '</tr>',
                                                        '</thead>';
                                            
                                            while($row1 = mysqli_fetch_array($result1)){
                                              echo '<table class="table table-dark">',
                                                      '</tbody>',
                                                        '<td>'.$row1[1].'</td>',
                                                        '<td>'.$row1[3].'</td>',
                                                        '<td>'.$row1[2].'</td>',
                                                        '<td> <input type="checkbox" class="form-check-input" id="exampleCheck1"></td>',
                                                      '</tbody>',
                                                      '</table>';
                                              } 
                                            }
                                          }#else{echo $row['PRODUCT_IDENTIFIER'];}  
                                          echo '<h5 class="w3-opacity"><b>Benefit</b></h5>';
                                          foreach ($OPTIONAL_ADDITIONAL_BENEFITS as $value1){
                                              
                                              echo'<h6 class="w3-text-teal"> '.$value1.' </h6>'; 
                                            }
                                          /*   
                                            #print_r($OP_BR); 
                                          echo '<hr>',
                                        '<h5 class="w3-opacity"><b>Minimum Premium</b></h5>';
                                        
                                        foreach ($OP_BMP as $value2){
                                              
                                              echo'<h6 class="w3-text-teal"> '.$value2.' </h6>'; 
                                            }
                                        echo '<hr>',
                                        '<h5 class="w3-opacity"><b>Rate</b></h5>';                                     
                                          foreach ($OP_BR as $value3){
                                              
                                              echo'<h6 class="w3-text-teal"> '.$value3.' </h6>'; 
                                            } 
                                            */ 
                                          echo '<hr>',                                                
                                    '</div>',

                                    '</div>',
                                '<!-- End Right Column -->',
                                '</div>',
                                
                                '<!-- End Grid -->',
                                '</div>',
                                
                                '<!-- End Page Container -->',
                            '</div>',
                            '</div>',
                            '<div class="modal-footer">',
                            '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>',
                            '<button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#'.'d'.$row['ID'].'d'.'">Buy</button>',
                            '</div>',
                            '<!-- Modal -->',
                            '<div class="modal fade" id="'.'d'.$row['ID'].'d'.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">',
                            '<div class="modal-dialog" role="document">',
                                '<div class="modal-content">',
                                '<div class="modal-header">',
                                    '<h5 class="modal-title" id="exampleModalLabel">Details</h5>',
                                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">',
                                    '<span aria-hidden="true">&times;</span>',
                                    '</button>',
                                  '</div>',
                                '<div class="modal-body">',
                                   '<form>',
                                        '<div class="form-group">',
                                            '<label for="start-date" class="col-form-label">Cover Start Date:</label>',
                                            '<input type="date" class="form-control" id="recipient-name" min='.date("yy-m-d").'>',
                                            '<label for="id-copy" class="col-form-label">Upload ID Copy:</label>',
                                            '<input type="file" class="form-control" id="recipient-name" placeholder="Cover Start Date">',
                                            '<label for="KRA-copy" class="col-form-label">Upload KRA Copy:</label>',
                                            '<input type="file" class="form-control" id="recipient-name" placeholder="Cover Start Date">',
                                            '<label for="Logbook" class="col-form-label">Upload Logbook Copy</label>',
                                            '<input type="file" class="form-control" id="recipient-name" placeholder="Cover Start Date">',
                                            '<label for="Delivery Mode" class="col-form-label">Choose Delivery Mode</label>',
                                            '<select id="deliverymode" class="form-control form-control-lg" name="deliverymode">',
                                              '<option>Email</option>',
                                              '<option>Whatsup</option>',
                                            '</select>', 
                                            '<label for="Valuation Center" class="col-form-label">Choose a Valuaion Center</label>',
                                            '<select id="valcenter" class="form-control form-control-sm" name="valcenter">';
                                            $sql4 = "select * from ValuationCenters";
                                                    $result = mysqli_query($conn, $sql4);
                                                    if(mysqli_num_rows($result) > 0){
                                                        
                                                        while($row = mysqli_fetch_array($result)){
                                                            echo '<option>'.$row["Town"].": ".$row["Location"].'</option>';
                                                        }
                                                        echo $row["Location"];
                                                    }                                         
                                           echo'</select>',
                                            '</div>',
                                            '<input type="text" class="form-control" id="recipient-name" placeholder="Payment Gateway">',
                                            '<label for="start-date" class="col-form-label">Cover end Date:</label>',
                                            '<input type="date" class="form-control" id="recipient-name" placeholder="Cover Start Date">',
 
                                        '</div>',
                                       
                                    '</form>',
                                    
                                
                                '<div class="modal-footer">',
                                    '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>',
                                    '<button type="button" class="btn btn-primary">Submit</button>',
                                '</div>',
                                '</div>',
                            '</div>',
                            '</div>',
                            '<div></div>', 
                        '</div>',
                        '</div>',
                        '</div>',
                    '</div>';
                    }
                }else {
                     echo '<div class="col-md-12 text-info h1 text-center">Creteria Not Matched. Kindly Try Again Latter</div>';
                }
            }
        }
        ?>
        </div>
    </div>
</div>
