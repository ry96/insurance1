<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Card Hover Effects</title>
    <link rel="stylesheet" href="../assets/css/procuct.css">
    
</head>
<body>
<?php
  require '../handle/db.php';
 
?>   
Available Underwriters
<div class="cards-list container">
    <?php
        $sql = "SELECT * FROM UnderwriterList";
        if($result = mysqli_query($conn, $sql)){
            while($row = mysqli_fetch_array($result)){
                //print_r($row);
                ?>
                <div class="card">
                            <div class="face face1">
                                <div class="content">
                                    <!--<img src=<?echo "../dashboard/dist/".$row["LOGO"]?>>-->
                                    <h3><?echo $row["Name"]?></h3>
                                </div>
                            </div>
                            <div class="face face2">
                                <div class="content">
                                    
                                        <form action="product.php" method="post">
                                            <input id="underwriterid" name="underwriterid" type="hidden" value="<?echo $row['Name']?>">
                                            <h3><input type="submit" value="View Products"></h3>
                                        </form>
                                        
                                </div>
                            </div>
                    </div>
                <?php
            }
        }
    ?>

</div>
</body>
</html>