<?php
    include 'credentials.php';
    include 'auth.php';
    $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',"Authorization:Bearer $tocken")); //setting custom header
    $phone = "254712962787";//"254722301062";
    $curl_post_data = array(
        //Fill in the request parameters with valid values
        'BusinessShortCode' => $shortcode,
        'Password' => $pass,
        'Timestamp' => $tyme,
        'TransactionType' => 'CustomerPayBillOnline',
        'Amount' => 1,
        'PartyA' => $phone,
        'PartyB' => $shortcode,
        'PhoneNumber' => $phone,
        'CallBackURL' => $callbackurl,
        'AccountReference' => 'pay',
        'TransactionDesc' => 'stk'
    );
    
    $data_string = json_encode($curl_post_data);
    
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
    
    $curl_response = curl_exec($curl);
    //print_r($curl_response);
    $obj  = json_decode($curl_response);
    //print_r($curl_response);
    if (isset($obj->CustomerMessage)) {
        $success = $obj->CustomerMessage;
    }else{
        $success =  $obj->errorMessage;
    }
    if ($success == "Success. Request accepted for processing"){
	    $responce = $success;
	    echo $responce;
        //$email = $_SESSION["email"];
        //include '../dist/mail/mail.php';
        //print_r($_SESSION);
       // echo $phone;
        //include "alert.php";
        //header( "refresh:2;url=../bimaplus/index.php" );
        //session_destroy();
    }else{
	    if ($success == "No ICCID found on NMS"){
		    $responce = "Kindly enter mpesa registered number";
	    }elseif ($success == "Bad Request - Invalid PhoneNumber"){
		    $responce = "Kindly Enter a valid phone number";	    
	    }
        //include "alert.php";
        //header("refresh:2;url=../bimaplus/gateway.php");
    }
    echo $responce;
    
    
    
  ?>
