<!DOCTYPE html>
<html lang="en">
    <?php
    require 'handle/db.php';
    session_start();
    $_SESSION["identifier"] = rand();
        
    ?>

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - SB Admin</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="index.php">Bima Plus Dashboard</a>
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <a class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="../../index.php"><i class="fas fa-home"></i></a>

            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="wallet.php">My Wallet</a>
                        <a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="login.html">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link" href="index.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <div class="sb-sidenav-menu-heading">Interface</div>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Layouts
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="layout-static.html">Static Navigation</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">Light Sidenav</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Pages
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                                        Authentication
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="login.html">Login</a>
                                            <a class="nav-link" href="register.html">Register</a>
                                            <a class="nav-link" href="password.html">Forgot Password</a>
                                        </nav>
                                    </div>
                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                                        Error
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="401.html">401 Page</a>
                                            <a class="nav-link" href="404.html">404 Page</a>
                                            <a class="nav-link" href="500.html">500 Page</a>
                                        </nav>
                                    </div>
                                </nav>
                            </div>
                            <div class="sb-sidenav-menu-heading">Addons</div>
                            <a class="nav-link" href="charts.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Charts
                            </a>
                            <a class="nav-link" href="tables.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Tables
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        <?php for ($i = 0; $i < 5; ++$i): ?>
                            Test <br>
                        <?php endfor;?>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid" id = "home">
                        <h1 class="mt-4">Dashboard</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">View</li>
                        </ol>
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body">Uderwriters</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#underwirters">View underwriters</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body">Agents</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#agents">View Agents</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                    <div class="card-body">Products</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#products">View Products</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body">Clients</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#clients">View Clients</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Add</li>
                        </ol>
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body">Uderwriters</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" data-toggle="modal" data-target="#underwritermodal">Add Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body">Agents</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" data-toggle="modal" data-target="#agentmodal">Add Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                    <div class="card-body">Products</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" data-toggle="modal" data-target="#productmodal">Add Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body">Clients</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" data-toggle="modal" data-target="#clientmodal">Add Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

<!--Underwriter Model-->
                    <div class="modal fade" id="underwritermodal" tabindex="-1" role="dialog" aria-labelledby="underwriterModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <form action="handle/underwriter.php" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                            <h5 class="modal-title" id="underwriterModalLabel">New Underwriter</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                            
                            <label for="Product" class="control-label">Choose Underwriter</label>
                                <select class="browser-default custom-select" id="underwriter" name="underwriter">
                                                    <?php
                                                            $sql4 = 'select Name from UnderwriterList;';
                                                            $result = mysqli_query($conn, $sql4);
                                                            if(mysqli_num_rows($result) > 0){
                                                                
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'.$row["Name"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            
                                                        ?>
                                                    
                                                    
                                </select><br><br>
                            <input class="form-control form-control-lg" name="legal_entity" type="text" placeholder="Legal entity" required><br>
                            <input class="form-control form-control-lg" name="address" type="text" placeholder="Address" required><br>
                            <input class="form-control form-control-lg" name="email" type="text" placeholder="Email adaress" required><br>
                            <input class="form-control form-control-lg" name="website" type="text" placeholder="Website"><br>
                            <label for="Logo" class="control-label">Upload Logo</label>
                            <input class="form-control form-control-lg" name="logo" type="file" placeholder="Logo" required><br>
                            <input class="form-control form-control-lg" name="location" type="text" placeholder="Location optional" required><br>
                            <input class="form-control form-control-lg" name="account" type="text" placeholder="Bank Account" required><br>
                            
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                        </div>
                        </div>
                    </div>

<!--End of Underwriter Model-->
<!--Agent Model-->
                    <div class="modal fade" id="agentmodal" tabindex="-1" role="dialog" aria-labelledby="agentModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <form action="handle/agent.php" method="post" enctype="multipart/form-data"> 
                            <div class="modal-header">
                            <h5 class="modal-title" id="agentModalLabel">New Agent</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                            <form enctype="multipart/form-data">
                            <input class="form-control form-control-lg" name="contact" type="text" placeholder="Contact Person"><br>
                            <input class="form-control form-control-lg" name="company" type="text" placeholder="Company Name/Individual full names"><br>
                            <input class="form-control form-control-lg" name="reg" type="text" placeholder="ID/PP/REG. Cert. for company/businessr"><br>
                            <input class="form-control form-control-lg" name="kra" type="text" placeholder="KRA Pin number"><br>
                            <label for="profile" class="control-label">Upload KRA COPY</label>
                            <input class="form-control form-control-lg" name="krafile" type="file" placeholder="krafile" required ><br>              
                            <input class="form-control form-control-lg" name="adress" type="text" placeholder="Physical Address"><br>
                            <input class="form-control form-control-lg" name="email" type="text" placeholder="Email Address"><br>
                            <input class="form-control form-control-lg" name="phone" type="text" placeholder="Phone number"><br>
                            <input class="form-control form-control-lg" name="tax" type="text" placeholder="Withholding Tax (%)"><br>

                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                        </div>
                        </div>
                    </div>
<!--End of Agent Model-->

<!--Product Model-->
                        <div class="modal fade" id="productmodal" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <form action="handle/product.php" method="post"  enctype="multipart/form-data"> 

                                <div class="modal-header">
                                <h5 class="modal-title" id="productModalLabel">New Product</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                <form  enctype="multipart/form-data">
                                <label for="Product" class="control-label">Choose a Product</label>
                                <select class="browser-default custom-select" id="prodname" name="prodname">
                                                    <?php
                                                            $sql4 = 'select ProductName from ProductList';
                                                            $result = mysqli_query($conn, $sql4);
                                                            if(mysqli_num_rows($result) > 0){
                                                                
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'.$row["ProductName"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            
                                                        ?>
                                                    
                                                    
                                </select><br><br>
                                <label for="risc" class="control-label">Choose Risk to be covered</label>
                                <select class="browser-default custom-select" id="risk" onchange="myFunction()" name="risk">
                                                    <?php
                                                            $sql1 = 'select * from VehicleClass where type = "MOTORCYCLE"';
                                                            $result = mysqli_query($conn, $sql1);
                                                            
                                                            if(mysqli_num_rows($result) > 0){
                                                                echo '<optgroup label="1. MOTORCYCLE">';
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            $sql2 = 'select * from VehicleClass where type = "TRICYCLE"';
                                                            $result = mysqli_query($conn, $sql2);
                                                            
                                                            if(mysqli_num_rows($result) > 0){
                                                                echo '<optgroup label="2. TRICYCLE">';
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            $sql3 = 'select * from VehicleClass where type = "MOTORVEHICLE"';
                                                            $result = mysqli_query($conn, $sql3);
                                                            if(mysqli_num_rows($result) > 0){
                                                                echo '<optgroup label="3. MOTORVEHICLE">';
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            } 
                                                        ?>
                                </select><br><br>
                                <label for="Product" class="control-label">Choose Underwriter</label>
                                <select class="browser-default custom-select" id="underwriter" name="underwriter">
                                                    <?php
                                                            $sql4 = 'select Name from UnderwriterList;';
                                                            $result = mysqli_query($conn, $sql4);
                                                            if(mysqli_num_rows($result) > 0){
                                                                
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'.$row["Name"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            
                                                        ?>
                                                    
                                                    
                                </select><br><br>
                                <div class="custom-file">
                                <input class="form-control form-control-lg custom-file-input" name="prodlogo" type="file" required><br>
                                <label class="custom-file-label" for="customFile">Upload Underwriter Logo</label>
                                </div>
                                <label for="Product" class="control-label">Choose Coverage</label>
                                <select class="browser-default custom-select" id="coverage" onchange="myFunction()" name="coverage">
                                                    <?php
                                                            $sql4 = 'select cover from Coverage';
                                                            $result = mysqli_query($conn, $sql4);
                                                            if(mysqli_num_rows($result) > 0){
                                                                
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'.$row["cover"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            
                                                        ?>
                                                    
                                                    
                                </select><br><br>
                                                <div id="suminsured"></div>
                                                <div id="suminsured1"></div>
                                                <div id="suminsured2"></div>
                                                <div id="suminsured3"></div>
                                                <br><br>
                                                <div id="tonnage"></div>
                                                <div id="tonnage1"></div>
                                                
                                                <script>
                                                    function myFunction() {
                                                        var x = document.getElementById("coverage");
                                                        var i = x.selectedIndex;
                                                        console.log(i); 
                                                        if (i == 2 ) {
                                                            document.getElementById("suminsured").innerHTML ='<label for="profile" class="control-label">Enter Amount Insured</label>';

                                                            document.getElementById("suminsured1").innerHTML = '<input class="form-control form-control-lg" name="minimum_premium" type="number" placeholder="Minimum Premium Insured" required><br>';
                                                            document.getElementById("suminsured2").innerHTML = '<label for="profile" class="control-label">Enter Year of Manufacture</label>';
                                                            document.getElementById("suminsured3").innerHTML = '<select name="selectyear" class="form-control form-control-lg"> <?php for ($i = date('Y'); $i >= 1950; $i--){echo "<option>$i</option>"; }?></select><br>'
                                                    
                                                        }
                                                        
                                                        if (i == 0 || i == 1) {
                                                            document.getElementById("suminsured").innerHTML = '';
                                                            document.getElementById("suminsured1").innerHTML = '';
                                                            document.getElementById("suminsured2").innerHTML = '';
                                                            document.getElementById("suminsured3").innerHTML = '';
                                                            
                                                            
                                                        }
                                                        if (i == 0 || i == 2){
                                                            document.getElementById("tonnage").innerHTML = '';
                                                            document.getElementById("tonnage1").innerHTML = '';
                                                        }

                                                
                                                
                                                        var z = document.getElementById("risk");
                                                        var y = z.selectedIndex;
                                                        console.log(y);
                                                        if ((y == 5 || y == 6 || y == 2)&&(i == 1)) {
                                                            document.getElementById("tonnage").innerHTML ='<label for="profile" class="control-label">Enter Tonnage</label>';

                                                            document.getElementById("tonnage1").innerHTML = '<input class="form-control form-control-lg" name="tonnage" type="number" placeholder="Tonnage" required><br>';
                                                        }else{
                                                                document.getElementById("tonnage").innerHTML =" ";
                                                                document.getElementById("tonnage1").innerHTML = " ";
                                                        }
                                                    }

                                                </script>


                                <textarea class="form-control form-control-lg" name="shortrates" type="textarea"  placeholder="Policy Limits And Benefits" rows="6"></textarea><br>
                                
                                <label for="Str">Enter Short Term Rates</label>
                                    
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">1 Month</th>
                                        <th scope="col">2 Months</th>
                                        <th scope="col">3 Months</th>
                                        <th scope="col">4 Months</th>
                                        <th scope="col">5 Months</th>
                                        <th scope="col">6 Months</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th> <input class="form-control form-control-lg" name="shorttermrates" type="number" step="any" placeholder="STR"><br></th>
                                        <th> <input class="form-control form-control-lg" name="shorttermrates1" type="number" step="any" placeholder="STR"><br></th>
                                        <th> <input class="form-control form-control-lg" name="shorttermrates2" type="number" step="any" placeholder="STR"><br></th>
                                        <th> <input class="form-control form-control-lg" name="shorttermrates3" type="number" step="any" placeholder="STR"><br></th>
                                        <th> <input class="form-control form-control-lg" name="shorttermrates4" type="number" step="any" placeholder="STR"><br></th>
                                        <th> <input class="form-control form-control-lg" name="shorttermrates5" type="number" step="any" placeholder="STR"><br></th>
                                    </tr>
                                    
                                    </tbody>
                                </table><br>
                                <input class="form-control form-control-lg" name="anualrates" type="number" step="any" placeholder="Annual rates" required><br>
                                <input class="form-control form-control-lg" name="clause" type="text" placeholder="Clauses" required><br>
                                <input class="form-control form-control-lg" name="conditions" type="text" placeholder="Conditions and waranties" required><br>
                                <p>
                                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Optional Benefits
                                    </a>

                                </p>
                                <div class="collapse" id="collapseExample">
                                    <div class="card card-body">
                                <input class="form-control form-control-lg" name="optionalbenefit0" type="text" placeholder="EG Access protector"><br>
                                <input class="form-control form-control-lg" name="optionalbenefit1" type="number" step="any" placeholder="Optional BeneMinimum Premium"><br>
                                <input class="form-control form-control-lg" name="optionalbenefit2" type="number" step="any" placeholder="Optional Benefits Rate(%)"><br>

                                    </div>
                                </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                                </form>

                            </div>
                            </div>
                        </div>
<!--End of Product Model-->
<!--Client Model-->
                        <div class="modal fade" id="clientmodal" tabindex="-1" role="dialog" aria-labelledby="clientModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="handle/client.php" method="post" enctype="multipart/form-data"> 
                                <div class="modal-header">
                                <h5 class="modal-title" id="agentModalLabel">New Client</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                <form enctype="multipart/form-data">
                                <input class="form-control form-control-lg" name="fname" type="text" placeholder="First Name/business name"><br>
                                <input class="form-control form-control-lg" name="mname" type="text" placeholder="Middle Names/company /business name"><br>
                                <input class="form-control form-control-lg" name="lname" type="text" placeholder="Last Name/company /business name"><br>
                                <input class="form-control form-control-lg" name="reg" type="text" placeholder="ID/PP/REG. Cert. for Companies/business"><br>
                                <label for="profile" class="control-label">Upload ID/PP/REG. Cert. for Companies/business</label>
                                <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="ID/PP/REG. Cert. for Companies/business"><br>
                                <input class="form-control form-control-lg" name="kra" type="text" placeholder="KRA PIN"><br>
                                <label for="profile" class="control-label">Uload KRA PIN</label>
                                <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="KRA PIN"><br>
                                <input class="form-control form-control-lg" name="email" type="text" placeholder="EMAIL"><br>
                                <input class="form-control form-control-lg" name="phone" type="text" placeholder="PHONE"><br>
                                <input class="form-control form-control-lg" name="occupation" type="text" placeholder="OCCUPATION"><br>
                                <input class="form-control form-control-lg" name="residence" type="text" placeholder="RESIDENCE"><br>
                                <label for="profile" class="control-label">Upload profile picture(optional)</label>
                                <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="PROFILE PHOTO/LOGO"><br>
                                <label for="logbook" class="control-label">Upload Logbook (optional)</label>
                                <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="VEHICLE DETAILS .LOGBOOK"><br>

                                
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                            </div>
                            </div>
                        </div>
<!--End of Client Model-->

                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-area mr-1"></i>
                                        Area Chart Example
                                    </div>
                                    <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar mr-1"></i>
                                        Bar Chart Example
                                    </div>
                                    <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1" id="underwirters"></i>
                                <a href="#home">Underwriters</a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Legal Entity</th>
                                                <th>Adress</th>
                                                <th>Email</th>
                                                <th>Website</th>
                                                <th>Location</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Legal Entity</th>
                                                <th>Adress</th>
                                                <th>Email</th>
                                                <th>Website</th>
                                                <th>Location</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php 
                                                $product_sql = "SELECT * FROM Underwriter";
                                                    if($result = mysqli_query($conn, $product_sql)){
                                                        if(mysqli_num_rows($result) > 0){
                                                            while($row = mysqli_fetch_array($result)){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $row['NAME']?></td>
                                                                    <td><?php echo $row['LEGAL_ENTITY']?></td>
                                                                    <td><?php echo $row['ADDRESS']?></td>
                                                                    <td><?php echo $row['EMAIL_ADDRESS']?></td>
                                                                    <td><?php echo $row['WEBSITE']?></td>
                                                                    <td><?php echo $row['LOCATION_OPTIONAL']?></td>
                                                                </tr>
                                                            <?php 
                                                        }
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1" id="agents"></i>
                                <a href="#home">Agents</a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Contact Person</th>
                                                <th>Krs Pin</th>
                                                <th>Adress</th>
                                                <th>Phone Number</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Contact Person</th>
                                                <th>Krs Pin</th>
                                                <th>Adress</th>
                                                <th>Phone Number</th>
                                                <th>Email</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                                $product_sql = "SELECT * FROM Agent";
                                                    if($result = mysqli_query($conn, $product_sql)){
                                                        if(mysqli_num_rows($result) > 0){
                                                            while($row = mysqli_fetch_array($result)){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $row['Company_Name']?></td>
                                                                    <td><?php echo $row['Contact_Person']?></td>
                                                                    <td><?php echo $row['KRA_Pin_number']?></td>
                                                                    <td><?php echo $row['Physical_Address']?></td>
                                                                    <td><?php echo $row['Phone_number']?></td>
                                                                    <td><?php echo $row['Email_Address']?></td>
                                                                </tr>
                                                            <?php 
                                                        }
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1" id="products"></i>
                                <a href="#home">Products</a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Risk</th>
                                                <th>Underwriter</th>
                                                <th>Coverage</th>
                                                <th>Rates</th>
                                                <th>Benefits</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Risk</th>
                                                <th>Underwriter</th>
                                                <th>Coverage</th>
                                                <th>Rates</th>
                                                <th>Year</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            
                                            <?php 
                                                $product_sql = "SELECT * FROM Product";
                                                    if($result = mysqli_query($conn, $product_sql)){
                                                        if(mysqli_num_rows($result) > 0){
                                                            while($row = mysqli_fetch_array($result)){
                                            ?>
                                                                <tr>
                                                                    <td><?php echo $row['PRODUCT_NAME']?></td>
                                                                    <td><?php echo $row['RISK_COVERED']?></td>
                                                                    <td><?php echo $row['UNDERWRITER']?></td>
                                                                    <td><?php echo $row['COVERAGE']?></td>
                                                                    <td><?php echo $row['SHORTTERM_RATES']?></td>
                                                                    <td><?php echo $row['YEAR_ACCEPTED']?></td>
                                                                </tr>
                                                            <?php 
                                                        }
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1" id="clients"></i>
                                <a href="#home">Clients</a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>KRA Pin</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Residence</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>KRA Pin</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Residence</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                                $product_sql = "SELECT * FROM Client";
                                                    if($result = mysqli_query($conn, $product_sql)){
                                                        if(mysqli_num_rows($result) > 0){
                                                            while($row = mysqli_fetch_array($result)){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $row['Fname']?></td>
                                                                    <td><?php echo $row['Lname']?></td>
                                                                    <td><?php echo $row['KRA_PIN']?></td>
                                                                    <td><?php echo $row['EMAIL']?></td>
                                                                    <td><?php echo $row['PHONE']?></td>
                                                                    <td><?php echo $row['RESIDENCE']?></td>
                                                                </tr>
                                                            <?php 
                                                        }
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/datatables-demo.js"></script>
    </body>
</html>
