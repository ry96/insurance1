<!DOCTYPE html>
<?php
    session_start();
    if(!isset($_POST["product"])) { 

        header("refresh:0;url=../index.php");
    }
    require 'handle/db.php';
    $prod_identifier =$_SESSION["prodIdentifier"];
?>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Page Title - SB Admin</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Details</h3></div>
                                    <div class="card-body">
                                        <form action="product.php" method="post">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input class="form-control py-1" name = "name" id="name" type="text" placeholder="Your Name" required/>
                                                    </div>
                                                    <div class="form-group">
                                                        <br><input class="form-control py-1" name = "email" id="email" type="text" placeholder="Email" required/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input class="form-control py-1" name = "ph-no" id="ph-no" type="text" placeholder="Phone Number" required/>
                                                    </div>
                                                    <div class="form-group">
                                                    <label id="vehicle_class" class="form-text text-muted "><small>Choose vehicle class</small></label>
                                                        <select  name="vehicleClass" class="form-control py-1" id="cover"  onchange="myFunction()">
                                                            <?php
                                                                    $sql1 = 'select * from VehicleClass where type = "MOTORCYCLE"';
                                                                    $result = mysqli_query($conn, $sql1);
                                                                    
                                                                    if(mysqli_num_rows($result) > 0){
                                                                        echo '<optgroup label="1. MOTORCYCLE">';
                                                                        while($row = mysqli_fetch_array($result)){
                                                                            echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                        }
                                                                        echo "</optgroup>";
                                                                    }
                                                                    $sql2 = 'select * from VehicleClass where type = "TRICYCLE"';
                                                                    $result = mysqli_query($conn, $sql2);
                                                                    
                                                                    if(mysqli_num_rows($result) > 0){
                                                                        echo '<optgroup label="2. TRICYCLE">';
                                                                        while($row = mysqli_fetch_array($result)){
                                                                            echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                        }
                                                                        echo "</optgroup>";
                                                                    }
                                                                    $sql3 = 'select * from VehicleClass where type = "MOTORVEHICLE"';
                                                                    $result = mysqli_query($conn, $sql3);
                                                                    if(mysqli_num_rows($result) > 0){
                                                                        echo '<optgroup label="3. MOTORVEHICLE">';
                                                                        while($row = mysqli_fetch_array($result)){
                                                                            echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                        }
                                                                        echo "</optgroup>";
                                                                    }
                                                                        
                                                                ?>                                                                        
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress"></label>
                                                <input class="form-control py-1" name = "vehicle_reg" id="vehicle_reg" type="text" aria-describedby="vehicle_reg" placeholder="vehicle registration" required/>
                                            </div>
                                            
                                                <input id="prodIdentifier" name="prodIdentifier" type="hidden" value="<?echo $prod_identifier?>">
                                                <input id="cover" name="cover" type="hidden" value="Third Party">
                                                <input type="submit" class="form-control input-lg d-flex justify-content-center btn btn-outline-success" value="Request Product">
                                            
                                        </form>
                                    </div>
                                    
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="../index.php">Cancel</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-1 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
