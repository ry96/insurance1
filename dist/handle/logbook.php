<?php
    if(!isset($_SESSION)){ 
        session_start(); 
    }
    require "db.php"; 
    #print_r($_SESSION);
    if (isset($_POST["Registration"]) && (isset($_FILES['clientFiles']))){ 
        $_SESSION['IDNUMBER'] = $_POST['IdNumber'];
        $_SESSION['KRA'] = $_POST['KRA'];
        $_SESSION['OCCUPATION'] = $_POST['occupation'];
        $_SESSION['PERIOD'] = $_POST['Date'];
        $_SESSION['location'] = $_POST['location'];
        $client_phone = $_SESSION["ph-no"];
        #print_r($_FILES);
        $logbooks = "../assets/logbooks/$client_phone/";
        if (!file_exists($logbooks)) {
            mkdir($logbooks, 0755, true);
            
        }
        $Registration = $_POST["Registration"];
        $Chasis = $_POST["Chasis"];
        $Make = $_POST["Make"];
        $Model = $_POST["Model"];
        $Type = $_POST["Type"];
        $Body = $_POST["Body"];
        $Fuel = $_POST["Fuel"];
        $ManYear = $_POST["ManYear"];
        $EngineNo = $_POST["EngineNo"];
        $Color = $_POST["Color"];
        $RegDate = $_POST["RegDate"];
        $GrossWeight = $_POST["GrossWeight"];
        $Owners = $_POST["Owners"];
        $Passangers = $_POST["Passangers"];
        $Load = $_POST["Load"];
        $Tax = $_POST["Tax"];
        $rating = $_POST["rating"];
        $Pin = $_POST["Pin"];
        $Name = $_POST["Name"];
        $KRA = $_POST["KRA"];
        if (isset($_POST["Email"])){
            $Medium = $_POST["Email"];
        }else{
            $Medium = $_POST["Whatsup"];
        }
        $docs = array();
        foreach($_FILES['clientFiles']['tmp_name'] as $key=>$tmp_name){
            $file_name = $key.$_FILES['clientFiles']['name'][$key];
            $file_tmp =$_FILES['clientFiles']['tmp_name'][$key];
            
            if ($key == 0){
                $file_name = "logbook-_-" . $file_name;
            }elseif($key == 1){
                $file_name = "idnumber-_-" . $file_name;
            }else{
                $file_name = "kra-_-" . $file_name;
            }
            $path = $logbooks . $file_name;
            
            if(move_uploaded_file($file_tmp, $path)){
                array_push($docs, $path);    
            }
            
        }
        $logbook = trim($docs[0], './'); 
        $IdNumber = trim($docs[1], './');
        $KRA_Doc = trim($docs[2], './');

        $sql = "INSERT into logbook (
            Registration,
            Chasis,
            Make,
            Model,
            Type,
            Body,
            Fuel,
            ManYear,
            EngineNo,
            Color,
            RegDate,
            GrossWeight,
            Owners,
            Passangers,
            Loads,
            Tax,
            rating,
            Pin,
            Name,
            KRA,
            Mediums,
            logbook,
            IdNumber,
            KRA_Doc        
        ) values(
            '$Registration',
            '$Chasis',
            '$Make',
            '$Model',
            '$Type',
            '$Body',
            '$Fuel',
            '$ManYear',
            '$EngineNo',
            '$Color',
            '$RegDate',
            '$GrossWeight',
            '$Owners',
            '$Passangers',
            '$Load',
            '$Tax',
            '$rating',
            '$Pin',
            '$Name',
            '$KRA',
            '$Medium',
            '$logbook',
            '$IdNumber',
            '$KRA_Doc'
        )";
        if (mysqli_query($conn, $sql)) {
                $responce = "Logbook uploadded successfully";
            
        }else{
            $responce = mysqli_error($conn);
            if (strpos($responce, 'Duplicate entry') !== false) {
                $responce = "Logbook Already exists";
            } else {
            $responce = "An unknown error has occured";
            } 
        }
        //print_r($_SESSION);
        include "alert.php";
        mysqli_close($conn);


        }

        header( "refresh:2;url=../gateway.php" );