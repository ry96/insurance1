<?php
    require "db.php";
    if (isset($_POST["name"])){
        $underwriterlogopath = "../assets/underwriterlogo/";
        if (!file_exists($underwriterlogopath)) {
            mkdir($underwriterlogopath, 0777, true);
        }
        $name = ucwords($_POST["name"]);
        $legal_entity = ucwords($_POST["legal_entity"]);
        $address = ucwords($_POST["address"]);
        $email = $_POST["email"];
        $website = $_POST["website"];
        #$logo = $_POST["logo"];
        $location = ucwords($_POST["location"]);
        $account = $_POST["account"];

        #print_r($_POST);
        $file_name = $_FILES['logo']['name'];
        $file_tmp =$_FILES['logo']['tmp_name'];
        $path = $underwriterlogopath.time(). $file_name;
        if(move_uploaded_file($file_tmp, $path)){
            $logo = trim($path, './');   
        }

        $sql = "INSERT into Underwriter (
            NAME,
            LEGAL_ENTITY,
            ADDRESS,
            EMAIL_ADDRESS,
            WEBSITE,
            LOGO,
            LOCATION_OPTIONAL,
            BANK_ACCOUNT_OPTIONAL,
            UNIQUE_CODE
        )  VALUES (
            '$name',
            '$legal_entity',
            '$address',
            '$email',
            '$website',
            '$logo',
            '$location',
            '$account',
            '$unique'
        )";
        if (mysqli_query($conn, $sql)) {
            $responce = "Underwriter <b>$name</b> created successfully";
        } else {
            if(strpos(mysqli_error($conn), "Duplicate") !== false){
                $responce = "Underwriter Exisit";
            } else{
                $responce = mysqli_error($conn);
            }
            
        }
        include "alert.php";
        mysqli_close($conn);
        
    }
    
header( "refresh:2;url=../index.php" );
    
