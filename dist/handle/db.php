<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "jendie";

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $database);

    // Check connection
    if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
    }
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
    $unique = substr(str_shuffle($chars), 0, 20);
?>