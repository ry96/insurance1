<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="styles.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
 <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
   
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.html">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Account</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  
  <div class="row">

  
    <div class="col-sm-12">
      <div class="panel panel-primary">
        <div class="panel-heading" style="text-align:center" data-toggle="collapse" data-target="#collapseView" aria-expanded="true" aria-controls="collapseView">View</div>
      </div>
    </div>

    <div class="collapse col-sm-12" id="collapseView">
        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading">Uderwriters</div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading">Users</div>
          </div>
        </div>
        
        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading">Products</div>
          </div>
        </div>


        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading">Agents</div>
          </div>
        </div>
      
        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading">Clients</div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading">Policies</div>
          </div>
        </div>
    </div>

    <div class="col-sm-12">
      <div class="panel panel-primary">
        <div class="panel-heading" style="text-align:center" style="text-align:center" data-toggle="collapse" data-target="#collapseAdd" aria-expanded="true" aria-controls="collapseAdd">Add</div>
      </div>
    </div>

    <div class="collapse col-sm-12" id="collapseAdd" >
      <div class="col-sm-4">
        <div class="panel btn-secondary">
          <div class="panel-heading"  data-toggle="modal" data-target="#exampleModal">Add Uderwriters</div>
        </div>
      </div>


      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
          <form>
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">New Underwriter</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              <input class="form-control form-control-lg" name="name" type="text" placeholder="Name" required><br>
              <input class="form-control form-control-lg" name="legal_entity" type="text" placeholder="Legal entity" required><br>
              <input class="form-control form-control-lg" name="address" type="text" placeholder="Address" required><br>
              <input class="form-control form-control-lg" name="email" type="text" placeholder="Email adaress" required><br>
              <input class="form-control form-control-lg" name="website" type="text" placeholder="Website"><br>
              <label for="Logo" class="control-label">Upload Logo</label>
              <input class="form-control form-control-lg" name="logo" type="file" placeholder="Logo" required><br>
              <input class="form-control form-control-lg" name="location" type="text" placeholder="Location optional" required><br>
              <input class="form-control form-control-lg" name="account" type="text" placeholder="Bank Account" required><br>
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
          </div>
        </div>
      </div>


      <div class="col-sm-4"> 
        <div class="panel btn-secondary">
          <div class="panel-heading" data-toggle="modal" data-target="#usermodal">Add User</div>
        </div>
      </div>


      <div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
          <form> 
            <div class="modal-header">
              <h5 class="modal-title" id="userModalLabel">New User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
              <input class="form-control form-control-lg" name="fname" type="text" placeholder="First name" required><br>
              <input class="form-control form-control-lg" name="lname" type="text" placeholder="Last name" required><br>
              <input class="form-control form-control-lg" name="phone" type="text" placeholder="Phone" required><br>
              <input class="form-control form-control-lg" name="email" type="text" placeholder="Email" required><br>
              <input class="form-control form-control-lg" name="id" type="text" placeholder="id" required><br>
              <label for="profile" class="control-label">Upload id Image</label>
              <input class="form-control form-control-lg" name="idimg" type="file" placeholder="nhifdoc" ><br>
              <input class="form-control form-control-lg" name="kra" type="text" placeholder="kra" required><br>
              <label for="profile" class="control-label">Upload kra Doc</label>
              <input class="form-control form-control-lg" name="kradoc" type="file" placeholder="nhifdoc" ><br>

              <input class="form-control form-control-lg" name="bank" type="text" placeholder="Bank No" required><br>
              <label for="profile" class="control-label">Upload bank Card Image</label>
              <input class="form-control form-control-lg" name="bankimg" type="file" placeholder="nhifdoc" ><br>
              <input class="form-control form-control-lg" name="nssf" type="text" placeholder="nssf" required><br>
              <label for="profile" class="control-label">Upload Nssf Doc</label>
              <input class="form-control form-control-lg" name="nssfdoc" type="file" placeholder="nssfdoc" ><br>
              <input class="form-control form-control-lg" name="nhif" type="text" placeholder="nhif" ><br>
              <label for="profile" class="control-label">Upload nhif Doc</label>
              <input class="form-control form-control-lg" name="nhifdoc" type="file" placeholder="nhifdoc" ><br>
              <label for="profile" class="control-label">Upload Signature file</label>
              <input class="form-control form-control-lg" name="signature" type="file" placeholder="signature" required ><br>
  
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save</button>

            </div>
            </form>
          </div>
        </div>
      </div>


      <div class="col-sm-4"> 
        <div class="panel btn-secondary">
          <div class="panel-heading"  data-toggle="modal" data-target="#productmodal">Add Product</div>
        </div>
      </div>



      <div class="modal fade" id="productmodal" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
          <form> 

            <div class="modal-header">
              <h5 class="modal-title" id="productModalLabel">New Product</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
              <input class="form-control form-control-lg" name="prodname" type="text" placeholder="Product Name"><br>
              <input class="form-control form-control-lg" name="risk" type="text" placeholder="Risk Covered"><br>
              <input class="form-control form-control-lg" name="underwriter" type="text" placeholder="Underwriter"><br>
              <input class="form-control form-control-lg" name="coverage" type="text" placeholder="Coverage"><br>
              <input class="form-control form-control-lg" name="shortrates" type="number" placeholder="Short Term Rates"><br>
              <input class="form-control form-control-lg" name="anualrates" type="number" placeholder="Anual rates"><br>
              <input class="form-control form-control-lg" name="clause" type="text" placeholder="Clauses"><br>
              <input class="form-control form-control-lg" name="conditions" type="text" placeholder="Conditions and waranties"><br>
              <p>
                <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                  Optional Benefits
                </a>

              </p>
              <div class="collapse" id="collapseExample">
                <div class="card card-body">
              <input class="form-control form-control-lg" name="optionalbenefit0" type="text" placeholder="EG Access protector"><br>
              <input class="form-control form-control-lg" name="optionalbenefit1" type="number" placeholder="Optional BeneMinimum Premium"><br>
              <input class="form-control form-control-lg" name="optionalbenefit2" type="number" placeholder="Optional Benefits Rate(%)"><br>

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>

          </div>
        </div>
      </div>


      <div class="col-sm-4"> 
        <div class="panel btn-secondary">
          <div class="panel-heading" data-toggle="modal" data-target="#agentmodal">Add Agent</div>
        </div>
      </div>


      <div class="modal fade" id="agentmodalls" tabindex="-1" role="dialog" aria-labelledby="agentModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
          <form> 
            <div class="modal-header">
              <h5 class="modal-title" id="agentModalLabel">New Agent</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
              <input class="form-control form-control-lg" name="contact" type="text" placeholder="Contact Person"><br>
              <input class="form-control form-control-lg" name="company" type="text" placeholder="Company Name/Individual full names"><br>
              <input class="form-control form-control-lg" name="reg" type="text" placeholder="ID/PP/REG. Cert. for company/businessr"><br>
              <input class="form-control form-control-lg" name="kra" type="text" placeholder="KRA Pin number"><br>
              <input class="form-control form-control-lg" name="kracpy" type="text" placeholder="KRA PIN COPY"><br>
              <input class="form-control form-control-lg" name="adress" type="text" placeholder="Physical Address"><br>
              <input class="form-control form-control-lg" name="email" type="text" placeholder="Email Address"><br>
              <input class="form-control form-control-lg" name="phone" type="text" placeholder="Phone number"><br>
              <input class="form-control form-control-lg" name="tax" type="text" placeholder="Withholding Tax (%)"><br>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
          </div>
        </div>
      </div>




      <div class="col-sm-4"> 
        <div class="panel btn-secondary">
          <div class="panel-heading" data-toggle="modal" data-target="#clientmodal">Add Client</div>
        </div>
      </div>

      <div class="modal fade" id="clientmodal" tabindex="-1" role="dialog" aria-labelledby="clientModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <form> 
            <div class="modal-header">
              <h5 class="modal-title" id="agentModalLabel">New Client</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
              <input class="form-control form-control-lg" name="fname" type="text" placeholder="First Name/business name"><br>
              <input class="form-control form-control-lg" name="mname" type="text" placeholder="Middle Names/company /business name"><br>
              <input class="form-control form-control-lg" name="lname" type="text" placeholder="Last Name/company /business name"><br>
              <input class="form-control form-control-lg" name="reg" type="text" placeholder="ID/PP/REG. Cert. for Companies/business"><br>
              <label for="profile" class="control-label">Upload ID/PP/REG. Cert. for Companies/business</label>
              <input class="form-control form-control-lg" name="regfile" type="file" placeholder="ID/PP/REG. Cert. for Companies/business"><br>
              <input class="form-control form-control-lg" name="kra" type="text" placeholder="KRA PIN"><br>
              <label for="profile" class="control-label">Uload KRA PIN</label>
              <input class="form-control form-control-lg" name="krafile" type="file" placeholder="KRA PIN"><br>
              <input class="form-control form-control-lg" name="email" type="text" placeholder="EMAIL"><br>
              <input class="form-control form-control-lg" name="phone" type="text" placeholder="PHONE"><br>
              <input class="form-control form-control-lg" name="occupation" type="text" placeholder="OCCUPATION"><br>
              <input class="form-control form-control-lg" name="residence" type="text" placeholder="RESIDENCE"><br>
              <label for="profile" class="control-label">Upload profile picture(optional)</label>
              <input class="form-control form-control-lg" name="logo" type="file" placeholder="PROFILE PHOTO/LOGO"><br>
              <label for="logbook" class="control-label">Upload Logbook (optional)</label>
              <input class="form-control form-control-lg" name="logbook" type="file" placeholder="VEHICLE DETAILS .LOGBOOK"><br>

              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
          </div>
        </div>
      </div>




      <div class="col-sm-4"> 
        <div class="panel btn-secondary">
          <div class="panel-heading"><a href="index.html#flexare" style="text-decoration: none; color: white;">Add Policy</a></div>
        </div>
      </div>
  </div>
</div>
 
<?php include '../progresschart.php';?>  
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading">New Business</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=5" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-danger">
        <div class="panel-heading">Renewal</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=8" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-success">
        <div class="panel-heading">Total Sales</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=100" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
  </div>
</div><br>

<div class="container">    
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading">Paid</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=6" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-primary">
        <div class="panel-heading">Out Standing</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=8" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-primary">
        <div class="panel-heading">Pending</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=3" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
  </div>
</div><br><br>

<footer class="container-fluid text-center">
  <p>Copyright</p>  
  <form class="form-inline">Get deals:
    <input type="email" class="form-control" size="50" placeholder="Email Address">
    <button type="button" class="btn btn-danger">Sign Up</button>
  </form>
</footer>
<script>
<?php header( "refresh:2;url=../dashboard.php" );?>
</script>
</body>
</html>