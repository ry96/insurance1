<!DOCTYPE html>
<?php
  require 'handle/db.php';
  session_start();
  if(!isset($_POST["email"])) { 
     
      header("refresh:0;url=../index.php");
    }
?>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Charts - SB Admin</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
                    </div>
                </div>
            </form>
            <!-- Navbar-->
            
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                        </div>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                <?php
                    if(isset($_POST['name'])){
                        $_SESSION["name"]  = $_POST["name"];
                        $_SESSION["email"]  = $_POST["email"];
                        $_SESSION["ph-no"]  = $_POST["ph-no"];
                        $_SESSION["vehicleClass"]  = $_POST["vehicleClass"];
                        $_SESSION["cover"]  = $_POST["cover"];
                        $_SESSION["vehicle_reg"] = $_POST["vehicle_reg"];
                        if(!isset($_SESSION["session_id"])) {
                            $_SESSION["session_id"] = rand(); 
                        }
                        

                        if ($_SESSION["cover"] == "Third Party Only"){
                            $ThirdPrtyOnly = True;
                            $ThirdPrtyAndTheft = False;
                            $Comprehensive = False;
                        }elseif ($_SESSION["cover"] == "Third Party And Theft"){
                            $ThirdPrtyAndTheft = True;
                            $ThirdPrtyOnly = False;
                            $Comprehensive = False;
                        }elseif ($_SESSION["cover"] == "Comprehensive"){
                            $Comprehensive = True;
                            $ThirdPrtyOnly = False;
                            $ThirdPrtyAndTheft = False;
                        }             
                        if(isset($_POST['sum_insured'])){
                            $_SESSION["sum_insured"] = $_POST['sum_insured'];
                        }else  $_SESSION["sum_insured"] = 1000; // Default take from db
                        if(isset($_POST['yom'])){
                            $_SESSION['year_of_manufacture'] = $_POST['yom'];
                        }else{
                            $_SESSION['year_of_manufacture'] = "Default";
                        }
                        if(isset($_POST["coverperiod"])){
                            $_SESSION['coverperiod'] = $_POST["coverperiod"];
                        }else{
                            $_SESSION['coverperiod']  = "Default";//take from db
                        }
                        if(isset($_POST["vehicleMake"])){
                            $_SESSION['vehicleMake'] = $_POST["vehicleMake"];
                        }else{
                            $_SESSION['vehicleMake'] = "Default";//take from db
                        }
                        if(isset($_POST["tonnage"])){
                            $_SESSION['tonnage'] = $_POST["tonnage"];
                        }else{
                            $_SESSION['tonnage'] = "Default";//take from db
                        }

                        // Third Party
                        if ($ThirdPrtyOnly == True){
                            $sql = "SELECT * FROM Product WHERE COVERAGE = 'Third Party Only'";
                        }

                        //Third Party and Theft
                        if ($ThirdPrtyAndTheft == True){
                            $sql = "SELECT * FROM Product WHERE COVERAGE = 'Third Party And Theft'";
                        }
                        
                        //Comprehensive
                        if ($Comprehensive == True){
                            $sql = "SELECT * FROM Product WHERE COVERAGE = 'Comprehensive'";
                        }

                        
                        //$sql = "SELECT * FROM Product";
                        if($result = mysqli_query($conn, $sql)){
                            if(mysqli_num_rows($result) > 0){}}?>
                            <div class="container-fluid">
                                <ol class="breadcrumb mb-4">
                                    <li class="breadcrumb-item active">Quotations for vehicle registration: &nbsp; <b><?echo $_SESSION["vehicle_reg"]?></b> &nbsp; Coverage: &nbsp; <b><?echo $_SESSION["cover"] ?> &nbsp; </b> for a period of &nbsp;<b><?echo $_SESSION['coverperiod']?> </b> &nbsp; Choose your prefered underwriter</li>
                                </ol>
                                <div class="row">  
                                <?while($row = mysqli_fetch_array($result)){
                                       $_SESSION["PRODUCT_IDENTIFIER"] = $row["PRODUCT_IDENTIFIER"];
                                       $_SESSION["UNDERWRITER"] = $row["UNDERWRITER"];
                                    ?>
                                    
                                    <div class="col-lg-4">
                                        <div class="card mb-4">
                                            <div class="card-header">
                                                <i class=""></i>
                                                <small><?echo $row["UNDERWRITER"]?></small>
                                            </div>
                                            <div class="d-flex justify-content-center" >premium</div>
                                            <div class="card-body d-flex justify-content-center h1" ><?echo $_SESSION["sum_insured"]?></div>
                                            <form action="product.php" method="post">
                                                    <input type="hidden" id="prodIdentifier" name="prodIdentifier" value=<?echo $row["PRODUCT_IDENTIFIER"]?>>
                                                    <input type="submit" class="form-control input-lg d-flex justify-content-center btn btn-outline-success" value="Get Product">
                                            </form>
                                        </div>
                                </div>
                            <?}} ?>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto footer">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; jendie 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="assets/demo/chart-pie-demo.js"></script>
    </body>
</html>
