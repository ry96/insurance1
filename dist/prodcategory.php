<!DOCTYPE html>
<?php

    session_start();
    if(!isset($_POST["underwriterid"])) { 
   
        header("refresh:0;url=../index.php");
    }
    require 'handle/db.php';

?>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Charts - SB Admin</title>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
                    </div>
                </div>
            </form>
            <!-- Navbar-->
            
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                        </div>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
               
                    <div class="container-fluid">
                         <?
                             $underwriter_Name = $_POST["underwriterid"];
                             $third_party = "SELECT * From Product WHERE UNDERWRITER = '$underwriter_Name' AND (COVERAGE = 'Third Party And Theft' OR COVERAGE = 'Third Party Only')"; 
                             $comprehensive = "SELECT * FROM Product WHERE UNDERWRITER = '$underwriter_Name' AND (COVERAGE = 'Comprehensive')";
                        ?>  
                        <ol class="breadcrumb col-lg-12 d-flex justify-content-center" >
                            <div class="d-flex justify-content-center"><? echo  $underwriter_Name ?></div>

                        </ol>
                        <div class="row">  
                        <?if($result = mysqli_query($conn, $third_party)){
                                while($row = mysqli_fetch_array($result)){
                                    $_SESSION["prodIdentifier"] = $row["PRODUCT_IDENTIFIER"];
                                    $_SESSION["UNDERWRITER"] = $row["UNDERWRITER"];
                                    $_SESSION["sum_insured"] = 1000;
                               ?>
                            <div class="col-lg-6">
                                <div class="card mb-6">
                                    <div class="card-header">
                                        <i class=""></i>
                                        <div class="d-flex justify-content-center">Third Party</div>
                                    </div>
                                    <div class="card-body"><div id="" width="100%" height="150"></div><?if(isset($row["SHORTTERM_RATES"])){ echo $row["SHORTTERM_RATES"];}else{ echo "Description"; } ?></div>
                                    <form action="thirtparty.php" method="post">
                                        <input id="product" name="product" type="hidden" value="<?echo $underwriter_Name?>">
                                        
                                        <input type="submit" class="form-control input-lg d-flex justify-content-center btn btn-outline-success" value="Get Quote">
                                    </form>
                                </div>
                            </div>
                        <?}}?>
                        <?if($result = mysqli_query($conn, $comprehensive)){
                            while($row = mysqli_fetch_array($result)){
                                $_SESSION["prodIdentifier"] = $row["PRODUCT_IDENTIFIER"];
                                $_SESSION["UNDERWRITER"] = $row["UNDERWRITER"];
                            ?>
                            <div class="col-lg-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class=""></i>
                                        <div class="d-flex justify-content-center">Comprehensive</div>
                                    </div>
                                    <div class="card-body"><div id="" width="100%" height="150"></div><?if(isset($row["SHORTTERM_RATES"])){ echo $row["SHORTTERM_RATES"];}else{ echo "Description"; } ?></div>
                                    <form action="comprehensive.php" method="post">
                                        <input id="product" name="product" type="hidden" value="<?echo $underwriter_Name?>">
                                        <input type="submit" class="form-control input-lg d-flex justify-content-center btn btn-outline-success" value="Get Quote">
                                    </form>
                                </div>
                            </div>
                            <?}}?>
                            
                        </div>
                    </div>
                </main>
                
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="assets/demo/chart-pie-demo.js"></script>
    </body>
</html>
