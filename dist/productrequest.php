<!DOCTYPE html>
<?php
    require 'handle/db.php';
    session_start(); 
    if(!isset($_POST["prodIdentifier"])) { 
        
      header("refresh:0;url=../index.php");
    }
    $_SESSION["grosspremium"] = ($_POST["grosspremium"]);
?>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Page Title - SB Admin</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Request This Product</h3></div>
                                    <div class="card-body">
                                        <form action="handle/logbook.php" method="post" enctype="multipart/form-data">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputFirstName">Particulers</label>
                                                        <input class="form-control py-1"  id="Registration" name="Registration" type="text" placeholder="Registration" required/>
                                                        <input class="form-control py-1"  id="Chasis" name="Chasis" type="text" placeholder="Chasis/Frame" required/>
                                                        <input class="form-control py-1"  id="Make" name="Make" type="text" placeholder="Make" required/>
                                                        <input class="form-control py-1"  id="Model" name="Model" type="text" placeholder="Model" required/>
                                                        <input class="form-control py-1"  id="Type" name="Type" type="text" placeholder="Type" required/>
                                                        <input class="form-control py-1"  id="Body" name="Body" type="text" placeholder="Body" required/>
                                                        <input class="form-control py-1"  id="Fuel" name="Fuel" type="text" placeholder="Fuel" required/>
                                                        <input class="form-control py-1"  id="ManYear" name="ManYear" type="text" placeholder="Man Year" required/>
                                                        

                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="small mb-1" for="inputLastName">Particulers</label>
                                                        <input class="form-control py-1"  id="EngineNo" name="EngineNo" type="text" placeholder="Engine No" required/>
                                                        <input class="form-control py-1"  id="Color" name="Color" type="text" placeholder="Color" required/>
                                                        <input class="form-control py-1"  id="RegDate" name="RegDate" type="text" placeholder="Reg Date" required/>
                                                        <input class="form-control py-1"  id="GrossWeight" name="GrossWeight" type="text" placeholder="Gross Weight" required/>
                                                        <input class="form-control py-1"  id="Owners" name="Owners" type="text" placeholder="No of Previous Owners" required/>
                                                        <input class="form-control py-1"  id="Passangers" name="Passangers" type="text" placeholder="Passangers" required/>
                                                        <input class="form-control py-1"  id="Tax" name="Tax" type="text" placeholder="Tax Class" required/>
                                                        <input class="form-control py-1"  id="Load" name="Load" type="text" placeholder="Load Capacity(KG)" required/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control py-1"  id="Rating" type="text" name="rating" placeholder="Rating" required/><br>
                                                <input class="form-control py-1"  id="occupation" type="text" name="occupation" placeholder="Occupation" required/><br>
                                                <input class="form-control py-1"  id="occupation" type="text" name="location" placeholder="Location" required/>
                                                <label class="small mb-1" for="Registered Owners">Registered Owners</label>
                                                <input class="form-control py-1"  id="Pin" name="Pin" type="text" aria-describedby="emailHelp" pattern="[a-zA-Z0-9 ]+" placeholder="Pin" />
                                                <input class="form-control py-1"  id="Name" name="Name" type="text" aria-describedby="emailHelp" pattern="[a-zA-Z0-9 ]+" placeholder="Name" />
                                                <label class="small mb-1" for="Doccuments">Upload Doccuments</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="logbook" name="clientFiles[]" required>
                                                    <label class="custom-file-label" for="logbook">Upload logbook</label>
                                                    <div class="invalid-feedback">Upload Logbook</div>
                                                  </div>
                                                  <input class="form-control py-1" id="IdNumber"  name="IdNumber" type="text" placeholder="Enter ID number" required/>

                                                  <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="IdNumber" name="clientFiles[]" required>
                                                    <label class="custom-file-label" for="IdNumber">Upload ID</label>
                                                    <div class="invalid-feedback">Upload Logbook</div>
                                                  </div>
                                                  <input class="form-control py-1" id="KRA" type="text"  name="KRA" placeholder="Enter KRA PIN" required/>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="KRA" name="clientFiles[]" required>
                                                    <label class="custom-file-label" for="KRA">Upload Kra PIN</label>
                                                    <div class="invalid-feedback">Example invalid custom file feedback</div>
                                                  </div>
                                                  <div class="form-group">
                                                    <label class="small mb-1" for="start_date">Choose Cover start Date</label>
                                                    <input class="form-control py-4" id="start_date" value = "<?echo date('yy-m-d')?>" name="Date" type="date" placeholder="Enter Phone number of Receiver" min="<?echo date('yy-m-d')?>"/>
                                                </div>
                                        
                                                <div class="form-group">       
                                                    <label class="small mb-1" for="inputPassword">Choose Delivery Mode</label>
    
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" id="Email" name="Email" type="checkbox" />
                                                        <label class="custom-control-label" for="Email">Email</label><br>
                                                        
                                                    </div>
                                                    <div class="custom-control custom-checkbox">                    
                                                        <input class="custom-control-input" id="Whatsup" name="Whatsup" type="checkbox" />
                                                        <label class="custom-control-label" for="Whatsup">Whatsup</label>
                                                    </div>
                                                </div>
                                                <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                                </div>

                                            </div>
                                            <input type="submit" class="form-control input-lg d-flex justify-content-center btn btn-outline-success" name="submit" value="Almost There Proceed">

                                           <!-- <div class="form-group mt-4 mb-0"><a class="btn btn-primary btn-block" href="gateway.php">Almost There Proceed</a></div>-->
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="../../agentjourney/underwriters.php">Cancel</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-1 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>