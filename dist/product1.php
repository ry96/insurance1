<!DOCTYPE html>
<?php
  require 'handle/db.php';

?>
<html lang="en">
    <head>
        <meta charset="utf-8" >
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge" >
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" >
        <meta name="description" content="" >
        <meta name="author" content="" >
        <title>Charts - SB Admin</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                        </div>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>

                    <div class="container-fluid">
                        <div class="col-md-2 mt-5" style="position:fixed">
                            <ul class="list-group" id="additional">
                                <li class="list-group-item active">Optional Benefits</li>
                            </ul>
                        </div>
                        
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                   
                                    <div class="card-header"><h5 class="text-center font-weight-light my-2"><?echo "Test"?></h5></div>
                                    <div class="card-body">
                                        
                                            <form>
                                                <div class="form-row">
                                                    
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="smallLastName">Underwriter</label>
                                                            <small class="form-control small mb-1" id="smallLastName" type="text" placeholder="Enter last name" ><?echo "Test"?></small>
                                                        </div>
                                                    </div>
                                                </div>  
                                                <div class="form-row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="smallFirstName">Scope of coverage</label>
                                                            <small class="form-control py-2" id="smallFirstName" type="text" placeholder="Enter first name" ><?echo "Test"?></small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="smallFirstName">Vehicle Class</label>
                                                            <small class="form-control py-2" id="smallFirstName" type="text" placeholder="Enter first name" ><?echo "Test"?></small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="smallFirstName">Policy Benefits and limit of Cover</label>
                                                            <textarea  class="form-control py-2" id="smallFirstName" type="text" placeholder="Enter first name"  rows="3" readonly><?echo "Test"?></textarea >
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="smallFirstName">Clauses</label>
                                                            <small class="form-control py-2" id="smallFirstName" type="text" placeholder="Enter first name" ><?echo "Test"?></small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="smallLastName">Conditions and Warranties</label>
                                                            <small class="form-control py-2" id="smallLastName" type="text" placeholder="Enter last name" ><?echo "Test"?></small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="smallLastName">Basic Premium</label>
                                                            <small class="form-control py-2" id="smallLastName" type="text" placeholder="Enter last name" ><?echo "Test"?></small>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="form-row">
                                                    

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="smallFirstName">Benefit</label>
                                                            
                                                                        
                                                                        <div class="d-flex bd-highlight mb-3 btn btn-outline-warning" onclick='myFunction("<?php echo "Test"?>")'>
                                                                            <div class="mr-auto p-2 bd-highlight "><?echo "Test"?></div>
                                                                            
                                                                        </div>
                                                                        <script> var hold = [];
                                                                            function myFunction(id) {
                                                                                var data = <?php echo json_encode($js);?>;
                                                                                
                                                                                for (x of data) {
                                                                                    if(x[1] == id){
                                                                                        if (hold.includes(id)){
                                                                                            alert("benefit already added");
                                                                                        }else{
                                                                                            var node = document.createElement("LI");
                                                                                            node.className = "list-group-item";
                                                                                            var benefit = parseInt(x['OP_BR']); //(parseInt(x['OP_BMP']) + 
                                                                                            var textnode = document.createTextNode(x[1] + "\n" + benefit);
                                                                                            node.appendChild(textnode);
                                                                                            document.getElementById("additional").appendChild(node);
                                                                                            var doc = document.getElementById("additional").innerText;
                                                                                            doc = doc.split("Optional Benefits").pop();
                                                                                            doc = doc.trim();
                                                                                            hold.push(id);
                                                                                            console.log(x)
                                                                                            
                                                                                        }
                                                                                        

                                                                                    }                                                            
                                                                                } 
                                                                                console.log(hold);
                                                                                //console.log(hold[0].split("\u21b5").join()); 
                                                                                                                                                              
                                                                            }
                                                                        </script>                                                            
                                                                   
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                                
                                                <div class="form-group mt-4 mb-0"><a class="btn btn-primary btn-block" href="share.php">Share</a></div>

                                                <div class="form-group mt-4 mb-0"><a class="btn btn-primary btn-block" href="productrequest.php">Buy</a></div>
                                            </form>
                                        
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="../index.php">give up</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; jendie 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="assets/demo/chart-pie-demo.js"></script>
    </body>
</html>
