<?php
//============================================================+
// File name   : example_061.php
// Begin       : 2010-05-24
// Last Update : 2014-01-25
//
// Description : Example 061 for TCPDF class
//               XHTML + CSS
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: XHTML + CSS
 * @author Nicola Asuni
 * @since 2010-05-25
 */

// Include the main TCPDF library (search for installation path).
include "tcpdf.php";
//require_once('examples/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information

// set default header data

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font

// set margins

// set auto page breaks

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

/* NOTE:
 * *********************************************************
 * You can load external XHTML using :
 *
 * $html = file_get_contents('/path/to/your/file.html');
 *
 * External CSS files will be automatically loaded.
 * Sometimes you need to fix the path of the external CSS.
 * *********************************************************
 */

// define some HTML content with style
$world = ("World");
$html = <<<EOF
<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<style>
    h1 {
        color: black;
        font-family: times;
        font-size: 8pt;
        text-decoration: underline;
    }
    p.first {
        color: black;
        font-family: helvetica;
        font-size: 8pt;
    }
    p.first span {
        color: black;
        font-style: italic;
    }
    p#second {
        color: rgb(00,63,127);
        font-family: times;
        font-size: 8pt;
        text-align: justify;
    }
    p#second > span {
        background-color: black;
    }
    table.first {
        color: black;
        font-family: helvetica;
        font-size: 8pt;
        
        
    }
    table.second {
        color: black;
        font-family: helvetica;
        font-size: 8pt;
        
        
    }
    td {
        /*border: 2px solid black;
        background-color: #ffffee;*/
    }
    td.second {
        margin:0px;
        border-style: solid solid solid solid;
        font-size: 8pt;
    }
    div.test {
        color: #CC0000;
        background-color: #FFFF66;
        font-family: helvetica;
        font-size: 8pt;
        border-style: solid solid solid solid;
        border-width: 2px 2px 2px 2px;
        border-color: black black black black;
        text-align: center;
    }
    .lowercase {
        text-transform: lowercase;
    }
    .uppercase {
        text-transform: uppercase;
    }
    .capitalize {
        text-transform: capitalize;
    }
</style>
</head>
$world
<div></div>
<table class="first" cellpadding="4" cellspacing="18">
    <tr>
        <td width="90" align="center"><b><img src="logo.png" alt="logo" ></b></td>
        <td width="230" align="center"><b></b></td>
        
        <td width="300" align="right"><b>Nyadwe Insurance Brokers Ltd (Head Office)<br>
            P.O. Box 18238, 00500 Nairobi/Kenya<br>
            Phone: +254206558224,0718692393,0732692393<br>
            Email: info@nyadwe.com

            </b></td>
    </tr>
    <hr>
    <tr>
        <td width="30" align="center"></td>
        <td width="140" rowspan="6" class=""></td>
        <td width="20"><br /></td>
        <td width="200">RISK NOTE #993143717<br /></td>
        <td width="80"></td>
        <td align="center" width="45"><br /></td>
    </tr>
    <tr>
        <td width="300" align="left" rowspan="3">
            Policy No: <br>   
            Class of Insurance: <br> 
            Underwriting company: <br>   
            Name of insured: <br> 
            Scheme:  <br>   
            PIN No: <br> 
            ID No:  <br>   
            OCCUPATION: <br> 
            Period of insurance From: <br> 
            Note:Terms and Conditions as per policy.
        </td>
        
        <td width="200" rowspan="3">
            --- not issued --- <br>  
            All Risks Insurance <br>  
            Pioneer General Insurance Ltd <br>  
            Kennedy Nyagah <br>  
            Flex Pay <br>  
            A0046687V <br>  
            27023120 <br>  
            Insurance Agent <br>  
            14/08/2019 To 13/08/2020 <br>  
            <br>  
        </td>
        <hr><hr>
    </tr>
</table>

<table class="second" cellpadding="4" cellspacing="18">
<tr>
    <td width="350" class="second" rowspan="">All Risks Insurance </td>
    <td width="120" class="second" rowspan="">Sum Insured </td>
    <td width="120" class="second" rowspan="">Premium</td>
</tr>
<tr>
    <td width="350" class="second" rowspan="">World Wide</td>
    <td width="120" class="second" rowspan="">90,000</td>
    <td width="120" class="second" rowspan="">7,200</td>
</tr>
<tr>
    <td width="350" class="second" rowspan="">Total Declared Value </td>
    <td width="120" class="second" rowspan="">90,000</td>
    <td width="120" class="second" rowspan="">7,200</td>
</tr>
<tr>
    <td width="350" class="" rowspan=""></td>
    <td width="120" class="second" rowspan="">Basic Premium</td>
    <td width="120" class="second" rowspan="">7,200</td>
</tr>
<tr>
    <td width="350" class="" rowspan=""></td>
    <td width="120" class="second" rowspan="">PCF (0.25)% </td>
    <td width="120" class="second" rowspan="">18</td>
</tr>
<tr>
    <td width="350" class="" rowspan=""></td>
    <td width="120" class="second" rowspan="">I.T.L (0.2)%: </td>
    <td width="120" class="second" rowspan="">14</td>
</tr>
<tr>
    <td width="350" class="" rowspan=""></td>
    <td width="120" class="second" rowspan="">Stamp duty:</td>
    <td width="120" class="second" rowspan="">40</td>
</tr>
<tr>
    <td width="350" class="" rowspan=""></td>
    <td width="120" class="second" rowspan="">Premium</td>
    <td width="120" class="second" rowspan="">7,272</td>
</tr>
</table>
    
<table class="" cellpadding="4" cellspacing="18">
<tr>
    <td width="150" align="left" class="" rowspan="">Property on cover: </td>
    <td width="500" align="left" class="" rowspan="">SAMSUNG FRIDGE S/N 12000412</td>
</tr>
<hr>
<tr>
    <td width="150" align="left" class="" rowspan="">Location</td>
    <td width="500" align="left" class="" rowspan="">EMBAKASI, PIPELINE ESTATE, EASY LIFE APARTMENT, HSE NO. A2
    </td>
</tr>
<hr>
<tr>
    <td width="150" align="left" class="" rowspan="">Excess</td>
    <td width="500" align="left" class="" rowspan="">10% each and every loss Minimum Kshs. 10, 000
    Mobile phones Excess: 15% each and every loss Minimum Kshs. 5,000</td>
</tr>
<hr>
<tr>
    <td width="150" align="left" class="" rowspan="">Cover Summary:</td>
    <td width="500" align="left" class="" rowspan="">This covers loss or damage to the whole or part of the property described in the schedule while anywhere in the
    world by an accident or misfortune not otherwise excluded provided that the insured shall be responsible for the first
    amount payable stated in the schedule in respect of each and every event occurring during the period of insurance.
    We may at our discretion, repair, reinstate or replace such property lost or damaged or may pay in cash the amount
    of loss or Damage.</td>
</tr>
<hr>
<tr>
    <td width="150" align="left" class="" rowspan="">Limits of Liability:</td>
    <td width="500" align="left" class="" rowspan="">The following clauses are applicable ;
    Reinstatement of Loss ,Reinstatement of Value Clause, Locked boot clause,Pair & Set Clause,
    Riot, Strike and Civil Commotion Clause,PVT(Political violence and terrorism),Premium payment warranty</td>
</tr>
<hr>
<tr>
    <td width="500" align="left" class="" rowspan="">
    Payment Options<br>
    1) Mpesa Paybill No 692393<br>
    2) Cheque to Nyadwe Insurance Brokers Ltd<br>
    3) Bank Transfer (Instructions from Nyadwe Accounts)<br>
    </td>
   
</tr>
<tr>
    <td width="300" align="left" class="" rowspan="">
    Prepared by naomi chege:
    </td>
    <td width="100" align="right" class="" rowspan="">
     Signature: 
    </td>
    <td width="100" align="right" class="" rowspan="">
     <img src="signature.jpeg" height = "50" width = "100" alt="logo">
    </td>

</tr>
<tr>
    <td width="300" align="left" class="" rowspan="">
    14/08/2019 03:19pm
    </td>

</tr>
</table>
EOF;

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// add a page



// output the HTML content


// reset pointer to the last page

// ---------------------------------------------------------
$to = "ericksoi3709@gmail.com"; 
$from = "ericksoi3709@gmail.com"; 
$subject = "send email with pdf attachment"; 
$message = "<p>Please see the attachment.</p>";
$separator = md5(time());
//Close and output PDF document
$pdf->Output('example_061.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+