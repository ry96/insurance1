<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="styles.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
 <style>
    /* Remove the navbar's default rounded borders and increase the bottom margin */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    
    /* Remove the jumbotron's default bottom margin */ 
     .jumbotron {
      margin-bottom: 0;
    }
    @media screen and (min-width: 992px) {
        .lgh {
          max-width: 1250px; /* New width for large modal */
          margin-left: 5px;
          min-height:850px;
        }
    }
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>
<?php
  require 'handle/db.php';
	
?>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Account</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
  
  <div class="row">

  
    <div class="col-sm-12">
      <div class="panel panel-primary">
        <div class="panel-heading" style="text-align:center" data-toggle="collapse" data-target="#collapseView" aria-expanded="true" aria-controls="collapseView">View</div>
      </div>
    </div>

    <div class="collapse col-sm-12" id="collapseView">
        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading" data-toggle="modal" data-target="#staticBackdrop">Uderwriters</div>
          </div>
        </div>
        
        <!-- Modal -->
      <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog lgh">
          <div class="modal-content lgh">
            <div class="modal-header">
              <h5 class="modal-title" id="staticBackdropLabel">Under Writers</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                  <div class="modal-body">
                      
                      <div class="container-fluid" style="max-width: 970px;">
                        <div class="row no-gutters">
                          
                        <?php
                          $sql = "SELECT * FROM Underwriter";
                          if($result = mysqli_query($conn, $sql)){
                            if(mysqli_num_rows($result) > 0){
                                
                              while($row = mysqli_fetch_array($result)){
                                echo '<div class="col-md-4 card btn-secondary" style="padding:6px; border:none;" data-toggle="modal" data-target="#'.$row["UNIQUE_CODE"].'">',
                                 '<img class="card-img-top" src="'.$row['LOGO'].'" height="200" width="100%" alt="Card image cap">',
                                 '<div class="card-body">',
                                 '<p class="card-text h1 text-center">'.$row["NAME"].'</p>',
                                '</div>',
                                '</div>',
                                        '<!-- Modal -->',
                                  '<div class="modal fade" id="'.$row["UNIQUE_CODE"].'" tabindex="-1" role="dialog" aria-labelledby="underwriterModalLabel" aria-hidden="true">',
                                   '<div class="modal-dialog" role="document">',
                                      '<div class="modal-content lgh1">',
                                        '<div class="modal-header">',
                                          '<h5 class="modal-title" id="exampleModalLabel">Under Writer</h5>',
                                         '<button type="button" class="close" data-dismiss="modal" aria-label="Close">',
                                            '<span aria-hidden="true">&times;</span>',
                                          '</button>',
                                        '</div>',
                                        '<div class="modal-body">',
                                          '<!-- Page Container -->',
                                          '<div class="w3-content w3-margin-top" style="max-width:1400px;">',

                                            '<!-- The Grid -->',
                                            '<div class="w3-row-padding">',
                                            
                                              '<!-- Left Column -->',
                                              '<div class="w3-third">',
                                              
                                                '<div class="w3-white w3-text-grey w3-card-4">',
                                                  '<div class="w3-display-container">',
                                                    '<img src="'.$row['LOGO'].'" style="width:100%" alt="Avatar">',
                                                    '<div class="w3-display-bottomleft w3-container w3-text-black">',
                                                      
                                                    '</div>',
                                                  '</div>',
                                                  '<div class="w3-container">',
                                                    
                                                    '<div class="w3-twothird">',
                                          '</div>',
                            
                                                
                                                  '</div>',
                                                '</div><br>',

                                              '<!-- End Left Column -->',
                                              '</div>',

                                              '<!-- Right Column -->',
                                              '<div class="w3-twothird">',
                                              
                                                '<div class="w3-container w3-card w3-white w3-margin-bottom">',
                                                  '<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Legal Entity</h2>',
                                                  '<div class="w3-container">',
                                                    '<h5 class="w3-opacity"><b>'.$row["LEGAL_ENTITY"].'</b></h5>',
                                                    '<hr>',
                                                  '</div>',

                                                  '<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Address</h2>',
                                                  '<div class="w3-container">',
                                                    '<h5 class="w3-opacity"><b>'.$row["ADDRESS"].'</b></h5>',
                                                    '<hr>',
                                                  '</div>',

                                                  '<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Email Address</h2>',
                                                  '<div class="w3-container">',
                                                    '<h5 class="w3-opacity"><b>'.$row["EMAIL_ADDRESS"].'</b></h5>',
                                                    '<hr>',
                                                  '</div>',
                                                  
                                                  '<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Website</h2>',
                                                  '<div class="w3-container">',
                                                    '<h5 class="w3-opacity"><b>'.$row["WEBSITE"].'</b></h5>',
                                                    '<hr>',
                                                  '</div>',
                                                  
                                                  '<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Location</h2>',
                                                  '<div class="w3-container">',
                                                    '<h5 class="w3-opacity"><b>'.$row["LOCATION_OPTIONAL"].'</b></h5>',
                                                    '<hr>',
                                                  '</div>',
                                                  
                                                  '<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>Bank Account</h2>',
                                                  '<div class="w3-container">',
                                                   '<h5 class="w3-opacity"><b>'.$row["BANK_ACCOUNT_OPTIONAL"].'</b></h5>',
                                                    '<hr>',
                                                  '</div>',
                                                '</div>',
                                              '<!-- End Right Column -->',
                                              '</div>',
                                              
                                            '<!-- End Grid -->',
                                            '</div>',
                                            
                                            '<!-- End Page Container -->',
                                          '</div>',
                                        '</div>',
                                        '<div class="modal-footer">',
                                          '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>',
                                        '</div>',
                                      '</div>',
                                    '</div>',
                                  '</div>';
                              }
                            }
                        }
                        ?>
                        </div>
                      </div>
                  </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading" data-toggle="modal" data-target="#userstaticBackdrop">Users</div>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="userstaticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog lgh">
            <div class="modal-content lgh">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Users</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
                    <div class="modal-body">
                        
                        <div class="container-fluid" style="max-width: 970px;">
                          <div class="row no-gutters">
                            
                          <?php
                            $sql1 = "SELECT * FROM User";
                            if($result = mysqli_query($conn, $sql1)){
                              if(mysqli_num_rows($result) > 0){
                                  
                                while($row = mysqli_fetch_array($result)){
                                  echo '<div class="col-md-4 card btn-secondary" style="padding:6px; border:none;" data-toggle="modal" data-target="#'.$row["Phone"].'">',
                                  '<img class="card-img-top" src="https://via.placeholder.com/150/000000/FFFFFF/?text='.$row['Fname'] . ' ' .$row['Lname'].'" height="200" width="100%" alt="Card image cap">',
                                  '<div class="card-body">',
                                  '<p class="card-text h1 text-center">'.$row["Phone"].'</p>',
                                  '</div>',
                                  '</div>',
                                          '<!-- Modal -->',
                                    '<div class="modal fade" id="'.$row["Phone"].'" tabindex="-1" role="dialog" aria-labelledby="underwriterModalLabel" aria-hidden="true">',
                                    '<div class="modal-dialog" role="document">',
                                        '<div class="modal-content lgh1">',
                                          '<div class="modal-header">',
                                            '<h5 class="modal-title" id="exampleModalLabel">'.$row['Fname'] . ' ' .$row['Lname'].'</h5>',
                                          '<button type="button" class="close" data-dismiss="modal" aria-label="Close">',
                                              '<span aria-hidden="true">&times;</span>',
                                            '</button>',
                                          '</div>',
                                          '<div class="modal-body">',
                                            '<!-- Page Container -->',
                                            '<div class="w3-content w3-margin-top" style="max-width:1400px;">',

                                              '<!-- The Grid -->',
                                              '<div class="w3-row-padding">',
                                              
                                                '<!-- Left Column -->',
                                                '<div class="w3-third">',
                                                
                                                  '<div class="w3-white w3-text-grey w3-card-4">',
                                                    '<div class="w3-display-container">',
                                                      '<img src="https://via.placeholder.com/150/000000/FFFFFF/?text='.$row['Fname'] . ' ' .$row['Lname'].'" style="width:100%" alt="Avatar">',
                                                      '<div class="w3-display-bottomleft w3-container w3-text-black">',
                                                        
                                                      '</div>',
                                                    '</div>',
                                                    '<div class="w3-container">',
                                                      
                                                      '<div class="w3-twothird">',
                                                    '</div>',
                              
                                                  
                                                    '</div>',
                                                  '</div><br>',

                                                '<!-- End Left Column -->',
                                                '</div>',

                                                '<!-- Right Column -->',
                                                '<div class="w3-twothird">',
                                                
                                                  '<div class="w3-container w3-card w3-white w3-margin-bottom">',
                                                    '<div class="w3-container">',
                                                      '<br><br>',
                                                      '<h5 class="w3-opacity"><b>Phone</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["Phone"].' </h6>',
                                                      '<hr>',
                                                     
                                                      '<h5 class="w3-opacity"><b>Email</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["Email"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>ID Number</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["ID_no"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>KRA Pin</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["KRA"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Bank Account Number</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["Bank_No"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>NSSF Number</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["NSSF"].' </h6>',                                      
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>NHIF Number</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["NHIF"].' </h6>',                                      
                                                      '<hr>',

                                                    '</div>',

                                                  '</div>',
                                                '<!-- End Right Column -->',
                                                '</div>',
                                                
                                              '<!-- End Grid -->',
                                              '</div>',
                                              
                                              '<!-- End Page Container -->',
                                            '</div>',
                                          '</div>',
                                          '<div class="modal-footer">',
                                            '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>',
                                          '</div>',
                                        '</div>',
                                      '</div>',
                                    '</div>';
                                }
                              }
                          }
                          ?>
                          </div>
                        </div>
                    </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

 
        
        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading"  data-toggle="modal" data-target="#productBackdrop">Products</div>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="productBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog lgh">
            <div class="modal-content lgh">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Products</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
                    <div class="modal-body">
                        
                        <div class="container-fluid" style="max-width: 970px;">
                          <div class="row no-gutters">
                            
                          <?php
                            $sql2 = "SELECT * FROM Product";
                            if($result = mysqli_query($conn, $sql2)){
                              if(mysqli_num_rows($result) > 0){
                               
                                while($row = mysqli_fetch_array($result)){
                                  #print_r($row);   
                                  if ($row["PRODUCT_NAME"] == "Bimaplus(MOTORINSURANCE)"){
                                    $product_logo = "images/productlogo/bimaplus.png";
                                    echo $row['UNDERWRITER'];
                                    $emailsql = 'SELECT EMAIL_ADDRESS FROM Underwriter WHERE LEGAL_ENTITY = "'.$row['UNDERWRITER'].'"'; 
                                    if($result4 = mysqli_query($conn, $emailsql)){
                                    while($row4 = mysqli_fetch_array($result4)){
                                      print_r($row4);
                                    }}
                                  }; 
                                  echo '<div class="col-md-4 card secondary" style="padding:6px; padding:2px;" data-toggle="modal" data-target="#'.$row["PRODUCT_IDENTIFIER"].'">',
                                  '<img class="card-img-top" src="'.$row['PRODUCT_LOGO'].'" height="200" width="100%" alt="Product Logo">',
                                  '<div class="card-body">',
                                  '<p class="card-text h1 text-center">'.$row["UNDERWRITER"].'</p>',
                                  '</div>',
                                  '</div>',
                                          '<!-- Modal -->',
                                    '<div class="modal fade" id="'.$row["PRODUCT_IDENTIFIER"].'" tabindex="-1" role="dialog" aria-labelledby="underwriterModalLabel" aria-hidden="true">',
                                      '<div class="modal-dialog" role="document">',
                                        '<div class="modal-content lgh1">',
                                          '<div class="modal-header">',
                                            '<h5 class="modal-title" id="exampleModalLabel">'.$row['PRODUCT_NAME'].'</h5>',
                                          '<button type="button" class="close" data-dismiss="modal" aria-label="Close">',
                                              '<span aria-hidden="true">&times;</span>',
                                            '</button>',
                                          '</div>',
                                          '<div class="modal-body">',
                                            '<!-- Page Container -->',
                                            '<div class="w3-content w3-margin-top" style="max-width:1400px;">',

                                              '<!-- The Grid -->',
                                              '<div class="w3-row-padding">',
                                              
                                                '<!-- Left Column -->',
                                                '<div class="w3-third">',
                                                
                                                  '<div class="w3-white w3-text-grey w3-card-4">',
                                                    '<div class="w3-display-container">',
                                                      '<img src="'.$product_logo.'" style="width:100%" alt="Avatar">',
                                                      '<div class="w3-display-bottomleft w3-container w3-text-black">',
                                                        
                                                      '</div>',
                                                    '</div>',
                                                    '<div class="w3-container">',
                                                      
                                                      '<div class="w3-twothird">',
                                                    '</div>',
                              
                                                  
                                                    '</div>',
                                                  '</div><br>',

                                                '<!-- End Left Column -->',
                                                '</div>',

                                                '<!-- Right Column -->',
                                                '<div class="w3-twothird">',
                                                
                                                  '<div class="w3-container w3-card w3-white w3-margin-bottom">',
                                                    '<div class="w3-container">',
                                                      '<br><br>',
                                                      '<h5 class="w3-opacity"><b>Risk Covered</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["VEHICLE_TYPE"].' </h6>',
                                                      '<h6 class="w3-text-teal"> '.$row["RISK_COVERED"].' </h6>',
                                                      '<hr>',
                                                     
                                                      '<h5 class="w3-opacity"><b>Under Writer</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["UNDERWRITER"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Coverrage</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["COVERAGE"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Policy Liits And Benefits</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["SHORTTERM_RATES"].' </h6>',
                                                      '<hr>',
                                                  
                                                      '<h5 class="w3-opacity"><b>Annual Rates</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["ANNUAL_RATES"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Clauses</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["CLAUSES"].' </h6>',                                      
                                                      '<hr>',
                                                      '<h5 class="w3-opacity"><b>Conditions and Waranties</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["CONDITIONS_AND_WARRANTIES"].' </h6>',                                      
                                                      '<hr>',
                                                      
                                                      '<h5 class="w3-opacity"><b>Year Accepted</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["YEAR_ACCEPTED"].' </h6>',                                      
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Tonnage</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["TONNAGE"].' </h6>',                                      
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Minimum Premium</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["MINIMUM_PREMIUM"].' </h6>',                                      
                                                      '<hr>',
                                                      '<h5 class="w3-opacity"><b>Email</b></h5>',
                                                      '<table class="table">',
                                                        '</tbody>';
                                                        $underwriter =  $row["UNDERWRITER"];                                                      
                                                        $email_sql = "SELECT EMAIL_ADDRESS FROM UnderwriterList WHERE Name LIKE '%$underwriter%'";
                                                        $email_result = mysqli_query($conn, $email_sql);
                                                        $email_row = mysqli_fetch_array($email_result, MYSQLI_NUM);
                                                        $email_address = $email_row[0];
                                                         echo '<td class="w3-opacity">'.$email_address.'</td>',
                                                         '<td class="nav-item dropdown">',
                                                          '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">',
                                                            'Edit',
                                                          '</a>',
                                                          '<div class="dropdown-menu" aria-labelledby="navbarDropdown">',
                                                        '<form action="handle/product_benefits.php" method="post" >',
                                                          '<fieldset style="width:200px">',
                                                            '<div class="form-group">',
                                                              '<input type="text" id=""  name="prodopt1" class="form-control" placeholder="New email value">',
                                                            '</div>',
                          
                                                         '</td>',                                                         
                                                        '</tbody>',
                                                        '</table>',
                                                      '<hr>',
                                                    '<div class="nav-item dropdown" >',
                                                      '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">',
                                                        'Add Optional Benefits',
                                                      '</a>',
                                                      '<div class="dropdown-menu" aria-labelledby="navbarDropdown">',
                                                    '<form action="handle/product_benefits.php" method="post" >',
                                                      '<fieldset style="width:200px">',
                                                        '<div class="form-group">',
                                                          '<input type="text" id=""  name="prodopt1" class="form-control" placeholder="EG Access protector">',
                                                        '</div>',
                                                        '<div class="form-group">',
                                                          '<input type="number" step="any" id=""  name="prodopt2" class="form-control" placeholder="Optional BeneMinimum Premium">',
                                                        '</div>',
                                                        '<div class="form-group">',
                                                          '<input type="number" step="any" id="" name="prodopt3"  class="form-control" placeholder="Optional Benefits Rate(%)">',
                                                        '</div>',
                                                        '<div class="form-group">',
                                                          '<input readonly type="text"  id="" name="prodopt4" value="'.$row["PRODUCT_IDENTIFIER"].'" class="form-control">',
                                                        '</div>',
                                                        '<button type="submit" class="btn btn-primary">Submit</button>',
                                                      '</fieldset>',
                                                    '</form>',
                                                      '</div>',
                                                    '</div>',
                                                    '<hr>';
                                                                                                             
                                                      $additionalsql = 'SELECT * FROM Product_additional_benefits WHERE PRODUCT_IDENTIFIER = "'.$row['PRODUCT_IDENTIFIER'].'"'; 
                                                      $OPTIONAL_ADDITIONAL_BENEFITS = [];
                                                      $OP_BR = [];
                                                      $OP_BMP = []; 
                                                      if($result1 = mysqli_query($conn, $additionalsql)){
                                                        if(mysqli_num_rows($result1) > 0){
                                                         
                                                          
                                                        while($row1 = mysqli_fetch_array($result1)){
                                                          array_push($OPTIONAL_ADDITIONAL_BENEFITS, $row1["OPTIONAL_ADDITIONAL_BENEFITS"]);
                                                          array_push($OP_BR, $row1["OP_BR"]);
                                                          array_push($OP_BMP, $row1["OP_BMP"]);   
                                                          } 
                                                          
                                                        #else{echo $row['PRODUCT_IDENTIFIER'];}  
                                                        $combined = array_map(null, $OPTIONAL_ADDITIONAL_BENEFITS, $OP_BR, $OP_BMP);
                                                        echo '<h5 class="w3-opacity"><b>Benefit</b></h5>',
                                                        '<table class="table table-dark">',
                                                          '<thead>',
                                                            '<tr>',
                                                              '<th scope="col">Name</th>',
                                                              '<th scope="col">Benefits Rate</th>',
                                                              '<th scope="col">Min Premium</th>',
                                                            '</tr>',
                                                            '</thead>';
                                                          
                                                          foreach ($combined as $val){
                                                            echo '<table class="table table-dark">',
                                                            '</tbody>',
                                                             '<td>'.$val[0].'</td>',
                                                             '<td>'.$val[1].'</td>',
                                                             '<td>'.$val[2].'</td>',
                                                             
                                                            '</tbody>',
                                                            '</table>';
                                                         }
                                                        }
                                                      }
                                                        /*   
                                                         #print_r($OP_BR); 
                                                        echo '<hr>',
                                                      '<h5 class="w3-opacity"><b>Minimum Premium</b></h5>';
                                                     
                                                      foreach ($OP_BMP as $value2){
                                                            
                                                            echo'<h6 class="w3-text-teal"> '.$value2.' </h6>'; 
                                                         }
                                                      echo '<hr>',
                                                      '<h5 class="w3-opacity"><b>Rate</b></h5>';                                     
                                                       foreach ($OP_BR as $value3){
                                                            
                                                            echo'<h6 class="w3-text-teal"> '.$value3.' </h6>'; 
                                                         } 
                                                         */ 
                                                        echo '<hr>',                                                     
                                                    '</div>',

                                                  '</div>',
                                                '<!-- End Right Column -->',
                                                '</div>',
                                                
                                              '<!-- End Grid -->',
                                              '</div>',
                                              
                                              '<!-- End Page Container -->',
                                            '</div>',
                                          '</div>',
                                          '<div class="modal-footer">',
                                            '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>',
                                          '</div>',
                                        '</div>',
                                      '</div>',
                                    '</div>';
                                }
                              }
                          }
                          ?>
                          </div>
                        </div>
                    </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading"  data-toggle="modal" data-target="#agentsBackdrop">Agents</div>
          </div>
        </div>
        <!-- Modal -->
      <div class="modal fade" id="agentsBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog lgh">
          <div class="modal-content lgh">
            <div class="modal-header">
              <h5 class="modal-title" id="staticBackdropLabel">Agents</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                  <div class="modal-body">
                      
                      <div class="container-fluid" style="max-width: 970px;">
                        <div class="row no-gutters">
                          
                        <?php
                          $sql3 = "SELECT * FROM Agent";
                          if($result = mysqli_query($conn, $sql3)){
                            if(mysqli_num_rows($result) > 0){
                                
                              while($row = mysqli_fetch_array($result)){
                                echo '<div class="col-md-4 card btn-secondary" style="padding:6px; border:none;" data-toggle="modal" data-target="#'.$row["KRA_Pin_number"].'">',
                                 '<img class="card-img-top" src="https://via.placeholder.com/150/000000/FFFFFF/?text='.$row['Contact_Person'] .'" height="200" width="100%" alt="Card image cap">',
                                 '<div class="card-body">',
                                 '<p class="card-text h1 text-center">'.$row["Company_Name"].'</p>',
                                '</div>',
                                '</div>',
                                        '<!-- Modal -->',
                                  '<div class="modal fade" id="'.$row["KRA_Pin_number"].'" tabindex="-1" role="dialog" aria-labelledby="underwriterModalLabel" aria-hidden="true">',
                                   '<div class="modal-dialog" role="document">',
                                      '<div class="modal-content lgh1">',
                                        '<div class="modal-header">',
                                          '<h5 class="modal-title" id="exampleModalLabel">Under Writer</h5>',
                                         '<button type="button" class="close" data-dismiss="modal" aria-label="Close">',
                                            '<span aria-hidden="true">&times;</span>',
                                          '</button>',
                                        '</div>',
                                        '<div class="modal-body">',
                                          '<!-- Page Container -->',
                                          '<div class="w3-content w3-margin-top" style="max-width:1400px;">',

                                            '<!-- The Grid -->',
                                            '<div class="w3-row-padding">',
                                            
                                              '<!-- Left Column -->',
                                              '<div class="w3-third">',
                                              
                                                '<div class="w3-white w3-text-grey w3-card-4">',
                                                  '<div class="w3-display-container">',
                                                    '<img src="https://via.placeholder.com/150/000000/FFFFFF/?text='.$row['Contact_Person'] .'" style="width:100%" alt="Avatar">',
                                                    '<div class="w3-display-bottomleft w3-container w3-text-black">',
                                                      
                                                    '</div>',
                                                  '</div>',
                                                  '<div class="w3-container">',
                                                    
                                                    '<div class="w3-twothird">',
                                          '</div>',
                            
                                                
                                                  '</div>',
                                                '</div><br>',

                                              '<!-- End Left Column -->',
                                              '</div>',

                                                '<!-- Right Column -->',
                                                '<div class="w3-twothird">',
                                                
                                                  '<div class="w3-container w3-card w3-white w3-margin-bottom">',
                                                    '<div class="w3-container">',
                                                      '<br><br>',
                                                      '<h5 class="w3-opacity"><b>Company Name</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["Company_Name"].' </h6>',
                                                      '<hr>',
                                                     
                                                      '<h5 class="w3-opacity"><b>Registration Certificate</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["REG_Cert"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>KRA Pin</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["KRA_Pin_number"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Physical Address</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["Physical_Address"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Email Address</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["Email_Address"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Phone Number</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["Phone_number"].' </h6>',                                      
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Witholding Tax</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["Withholding_Tax"].' </h6>',                                                                        
                                                      '<hr>',                                                      
                                                    '</div>',

                                                  '</div>',
                                                '<!-- End Right Column -->',
                                              '</div>',
                                              
                                            '<!-- End Grid -->',
                                            '</div>',
                                            
                                            '<!-- End Page Container -->',
                                          '</div>',
                                        '</div>',
                                        '<div class="modal-footer">',
                                          '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>',
                                        '</div>',
                                      '</div>',
                                    '</div>',
                                  '</div>';
                              }
                            }
                        }
                        ?>
                       
                        </div>
                      </div>
                  </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>





        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading" data-toggle="modal" data-target="#agentsBackdrop">Clients</div>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="userstaticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog lgh">
            <div class="modal-content lgh">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Clients</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
                    <div class="modal-body">
                        
                        <div class="container-fluid" style="max-width: 970px;">
                          <div class="row no-gutters">
                            
                          <?php
                            $sql4 = "SELECT * FROM Clients";
                            if($result = mysqli_query($conn, $sql4)){
                              if(mysqli_num_rows($result) > 0){
                                  
                                while($row = mysqli_fetch_array($result)){
                                  echo '<div class="col-md-4 card btn-secondary" style="padding:6px; border:none;" data-toggle="modal" data-target="#'.$row["KRA_PIN"].'">',
                                  '<img class="card-img-top" src="'.$row['PROFILE_PHOTO'] .'" height="200" width="100%" alt="Card image cap">',
                                  '<div class="card-body">',
                                  '<p class="card-text h1 text-center">'.$row["PHONE"].'</p>',
                                  '</div>',
                                  '</div>',
                                          '<!-- Modal -->',
                                    '<div class="modal fade" id="'.$row["KRA_PIN"].'" tabindex="-1" role="dialog" aria-labelledby="underwriterModalLabel" aria-hidden="true">',
                                    '<div class="modal-dialog" role="document">',
                                        '<div class="modal-content lgh1">',
                                          '<div class="modal-header">',
                                            '<h5 class="modal-title" id="exampleModalLabel">'.$row['Fname'] . ' ' .$row['Lname'].'</h5>',
                                          '<button type="button" class="close" data-dismiss="modal" aria-label="Close">',
                                              '<span aria-hidden="true">&times;</span>',
                                            '</button>',
                                          '</div>',
                                          '<div class="modal-body">',
                                            '<!-- Page Container -->',
                                            '<div class="w3-content w3-margin-top" style="max-width:1400px;">',

                                              '<!-- The Grid -->',
                                              '<div class="w3-row-padding">',
                                              
                                                '<!-- Left Column -->',
                                                '<div class="w3-third">',
                                                
                                                  '<div class="w3-white w3-text-grey w3-card-4">',
                                                    '<div class="w3-display-container">',
                                                      '<img src="'.$row['PROFILE_PHOTO'] .'" style="width:100%" alt="Avatar">',
                                                      '<div class="w3-display-bottomleft w3-container w3-text-black">',
                                                        
                                                      '</div>',
                                                    '</div>',
                                                    '<div class="w3-container">',
                                                      
                                                      '<div class="w3-twothird">',
                                                    '</div>',
                              
                                                  
                                                    '</div>',
                                                  '</div><br>',

                                                '<!-- End Left Column -->',
                                                '</div>',

                                                '<!-- Right Column -->',
                                                '<div class="w3-twothird">',
                                                
                                                  '<div class="w3-container w3-card w3-white w3-margin-bottom">',
                                                    '<div class="w3-container">',
                                                      '<br><br>',
                                                      '<h5 class="w3-opacity"><b>Phone</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["PHONE"].' </h6>',
                                                      '<hr>',
                                                     
                                                      '<h5 class="w3-opacity"><b>Email</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["EMAIL"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>KRA PIN</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["KRA_PIN"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Occupation Pin</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["OCCUPATION"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Residence</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["RESIDENCE"].' </h6>',
                                                      '<hr>',

                                                      '<h5 class="w3-opacity"><b>Business Certificate</b></h5>',
                                                      '<h6 class="w3-text-teal"> '.$row["Cert_for"].' </h6>',                                      
                                                      '<hr>',


                                                    '</div>',

                                                  '</div>',
                                                '<!-- End Right Column -->',
                                                '</div>',
                                                
                                              '<!-- End Grid -->',
                                              '</div>',
                                              
                                              '<!-- End Page Container -->',
                                            '</div>',
                                          '</div>',
                                          '<div class="modal-footer">',
                                            '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>',
                                          '</div>',
                                        '</div>',
                                      '</div>',
                                    '</div>';
                                }
                              }
                          }
                          ?>
                          </div>
                        </div>
                    </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>






        <div class="col-sm-4">
          <div class="panel btn-secondary">
            <div class="panel-heading">Policies</div>
          </div>
        </div>
    </div>

    <div class="col-sm-12">
      <div class="panel panel-primary">
        <div class="panel-heading" style="text-align:center" style="text-align:center" data-toggle="collapse" data-target="#collapseAdd" aria-expanded="true" aria-controls="collapseAdd">Add</div>
      </div>
    </div>

    <div class="collapse col-sm-12" id="collapseAdd" >
      <div class="col-sm-4">
        <div class="panel btn-secondary">
          <div class="panel-heading"  data-toggle="modal" data-target="#exampleModal">Add Uderwriters</div>
        </div>
      </div>


      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
          <form action="handle/underwriter.php" method="post" enctype="multipart/form-data">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">New Underwriter</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              <input class="form-control form-control-lg" name="name" type="text" placeholder="Name" required><br>
              <input class="form-control form-control-lg" name="legal_entity" type="text" placeholder="Legal entity" required><br>
              <input class="form-control form-control-lg" name="address" type="text" placeholder="Address" required><br>
              <input class="form-control form-control-lg" name="email" type="text" placeholder="Email adaress" required><br>
              <input class="form-control form-control-lg" name="website" type="text" placeholder="Website"><br>
              <label for="Logo" class="control-label">Upload Logo</label>
              <input class="form-control form-control-lg" name="logo" type="file" placeholder="Logo" required><br>
              <input class="form-control form-control-lg" name="location" type="text" placeholder="Location optional" required><br>
              <input class="form-control form-control-lg" name="account" type="text" placeholder="Bank Account" required><br>
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
          </div>
        </div>
      </div>


      <div class="col-sm-4"> 
        <div class="panel btn-secondary">
          <div class="panel-heading" data-toggle="modal" data-target="#usermodal">Add User</div>
        </div>
      </div>


      <div class="modal fade" id="usermodal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
          <form action="handle/user.php" method="post" enctype="multipart/form-data"> 
            <div class="modal-header">
              <h5 class="modal-title" id="userModalLabel">New User</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
              <input class="form-control form-control-lg" name="fname" type="text" placeholder="First name" required><br>
              <input class="form-control form-control-lg" name="lname" type="text" placeholder="Last name" required><br>
              <input class="form-control form-control-lg" name="phone" type="text" placeholder="Phone" required><br>
              <input class="form-control form-control-lg" name="email" type="text" placeholder="Email" required><br>
              <input class="form-control form-control-lg" name="id" type="text" placeholder="id" required><br>
              <label for="profile" class="control-label">Upload id Image</label>
              <input class="form-control form-control-lg" name="userFiles[]" type="file" placeholder="nhifdoc" ><br>
              <input class="form-control form-control-lg" name="kra" type="text" placeholder="kra" required><br>
              <label for="profile" class="control-label">Upload kra Doc</label>
              <input class="form-control form-control-lg" name="userFiles[]" type="file" placeholder="nhifdoc" ><br>

              <input class="form-control form-control-lg" name="bank" type="text" placeholder="Bank No" required><br>
              <label for="profile" class="control-label">Upload bank Card Image</label>
              <input class="form-control form-control-lg" name="userFiles[]" type="file" placeholder="nhifdoc" ><br>
              <input class="form-control form-control-lg" name="nssf" type="text" placeholder="nssf" required><br>
              <label for="profile" class="control-label">Upload Nssf Doc</label>
              <input class="form-control form-control-lg" name="userFiles[]" type="file" placeholder="nssfdoc" ><br>
              <input class="form-control form-control-lg" name="nhif" type="text" placeholder="nhif" ><br>
              <label for="profile" class="control-label">Upload nhif Doc</label>
              <input class="form-control form-control-lg" name="userFiles[]" type="file" placeholder="nhifdoc" ><br>
              <label for="profile" class="control-label">Upload Signature file</label>
              <input class="form-control form-control-lg" name="userFiles[]" type="file" placeholder="signature" required ><br>
  
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save</button>

            </div>
            </form>
          </div>
        </div>
      </div>


      <div class="col-sm-4"> 
        <div class="panel btn-secondary">
          <div class="panel-heading"  data-toggle="modal" data-target="#productmodal">Add Product</div>
        </div>
      </div>



      <div class="modal fade" id="productmodal" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
          <form action="handle/product.php" method="post"  enctype="multipart/form-data"> 

            <div class="modal-header">
              <h5 class="modal-title" id="productModalLabel">New Product</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form  enctype="multipart/form-data">
              <label for="Product" class="control-label">Choose a Product</label>
              <select class="browser-default custom-select" id="prodname" name="prodname">
                                <?php
                                        $sql4 = 'select ProductName from ProductList';
                                        $result = mysqli_query($conn, $sql4);
                                        if(mysqli_num_rows($result) > 0){
                                            
                                            while($row = mysqli_fetch_array($result)){
                                                echo '<option>'.$row["ProductName"].'</option>';
                                            }
                                            echo "</optgroup>";
                                        }
                                         
                                    ?>
                                
                                   
              </select><br><br>
              <label for="risc" class="control-label">Choose Risk to be covered</label>
              <select class="browser-default custom-select" id="risk" onchange="myFunction()" name="risk">
                                 <?php
                                        $sql1 = 'select * from VehicleClass where type = "MOTORCYCLE"';
                                        $result = mysqli_query($conn, $sql1);
                                        
                                        if(mysqli_num_rows($result) > 0){
                                            echo '<optgroup label="1. MOTORCYCLE">';
                                            while($row = mysqli_fetch_array($result)){
                                                echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                            }
                                            echo "</optgroup>";
                                        }
                                        $sql2 = 'select * from VehicleClass where type = "TRICYCLE"';
                                        $result = mysqli_query($conn, $sql2);
                                        
                                        if(mysqli_num_rows($result) > 0){
                                            echo '<optgroup label="2. TRICYCLE">';
                                            while($row = mysqli_fetch_array($result)){
                                                echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                            }
                                            echo "</optgroup>";
                                        }
                                        $sql3 = 'select * from VehicleClass where type = "MOTORVEHICLE"';
                                        $result = mysqli_query($conn, $sql3);
                                        if(mysqli_num_rows($result) > 0){
                                            echo '<optgroup label="3. MOTORVEHICLE">';
                                            while($row = mysqli_fetch_array($result)){
                                                echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                            }
                                            echo "</optgroup>";
                                        } 
                                    ?>
              </select><br><br>
              <label for="Product" class="control-label">Choose Underwriter</label>
              <select class="browser-default custom-select" id="underwriter" name="underwriter">
                                <?php
                                        $sql4 = 'select Name from UnderwriterList;';
                                        $result = mysqli_query($conn, $sql4);
                                        if(mysqli_num_rows($result) > 0){
                                            
                                            while($row = mysqli_fetch_array($result)){
                                                echo '<option>'.$row["Name"].'</option>';
                                            }
                                            echo "</optgroup>";
                                        }
                                         
                                    ?>
                                
                                   
              </select><br><br>
              <div class="custom-file">
              <input class="form-control form-control-lg custom-file-input" name="prodlogo" type="file" required><br>
              <label class="custom-file-label" for="customFile">Upload Underwriter Logo</label>
              </div>
              <label for="Product" class="control-label">Choose Coverage</label>
              <select class="browser-default custom-select" id="coverage" onchange="myFunction()" name="coverage">
                                <?php
                                        $sql4 = 'select cover from Coverage';
                                        $result = mysqli_query($conn, $sql4);
                                        if(mysqli_num_rows($result) > 0){
                                            
                                            while($row = mysqli_fetch_array($result)){
                                                echo '<option>'.$row["cover"].'</option>';
                                            }
                                            echo "</optgroup>";
                                        }
                                         
                                    ?>
                                
                                   
              </select><br><br>
                            <div id="suminsured"></div>
                            <div id="suminsured1"></div>
                            <div id="suminsured2"></div>
                            <div id="suminsured3"></div>
                            <br><br>
                            <div id="tonnage"></div>
                            <div id="tonnage1"></div>
                            
                            <script>
                                function myFunction() {
                                    var x = document.getElementById("coverage");
                                    var i = x.selectedIndex;
                                    console.log(i); 
                                    if (i == 2 ) {
                                        document.getElementById("suminsured").innerHTML ='<label for="profile" class="control-label">Enter Amount Insured</label>';

                                        document.getElementById("suminsured1").innerHTML = '<input class="form-control form-control-lg" name="minimum_premium" type="number" placeholder="Minimum Premium Insured" required><br>';
                                        document.getElementById("suminsured2").innerHTML = '<label for="profile" class="control-label">Enter Year of Manufacture</label>';
                                        document.getElementById("suminsured3").innerHTML = '<select name="selectyear" class="form-control form-control-lg"> <?php for ($i = date('Y'); $i >= 1950; $i--){echo "<option>$i</option>"; }?></select><br>'
                                   
                                    }
                                    
                                    if (i == 0 || i == 1) {
                                        document.getElementById("suminsured").innerHTML = '';
                                        document.getElementById("suminsured1").innerHTML = '';
                                        document.getElementById("suminsured2").innerHTML = '';
                                        document.getElementById("suminsured3").innerHTML = '';
                                        
                                        
                                    }
                                    if (i == 0 || i == 2){
                                        document.getElementById("tonnage").innerHTML = '';
                                        document.getElementById("tonnage1").innerHTML = '';
                                    }

                            
                              
                                    var z = document.getElementById("risk");
                                    var y = z.selectedIndex;
                                    console.log(y);
                                    if ((y == 5 || y == 6 || y == 2)&&(i == 1)) {
                                          document.getElementById("tonnage").innerHTML ='<label for="profile" class="control-label">Enter Tonnage</label>';

                                          document.getElementById("tonnage1").innerHTML = '<input class="form-control form-control-lg" name="tonnage" type="number" placeholder="Tonnage" required><br>';
                                    }else{
                                            document.getElementById("tonnage").innerHTML =" ";
                                            document.getElementById("tonnage1").innerHTML = " ";
                                    }
                                }

                            </script>


              <textarea class="form-control form-control-lg" name="shortrates" type="textarea"  placeholder="Policy Limits And Benefits" rows="6"></textarea><br>
              
              <label for="Str">Enter Short Term Rates</label>
                 
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">1 Month</th>
                    <th scope="col">2 Months</th>
                    <th scope="col">3 Months</th>
                    <th scope="col">4 Months</th>
                    <th scope="col">5 Months</th>
                    <th scope="col">6 Months</th>

                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th> <input class="form-control form-control-lg" name="shorttermrates" type="number" step="any" placeholder="STR"><br></th>
                    <th> <input class="form-control form-control-lg" name="shorttermrates1" type="number" step="any" placeholder="STR"><br></th>
                    <th> <input class="form-control form-control-lg" name="shorttermrates2" type="number" step="any" placeholder="STR"><br></th>
                    <th> <input class="form-control form-control-lg" name="shorttermrates3" type="number" step="any" placeholder="STR"><br></th>
                    <th> <input class="form-control form-control-lg" name="shorttermrates4" type="number" step="any" placeholder="STR"><br></th>
                    <th> <input class="form-control form-control-lg" name="shorttermrates5" type="number" step="any" placeholder="STR"><br></th>
                  </tr>
                 
                </tbody>
              </table><br>
              <input class="form-control form-control-lg" name="anualrates" type="number" step="any" placeholder="Annual rates" required><br>
              <input class="form-control form-control-lg" name="clause" type="text" placeholder="Clauses" required><br>
              <input class="form-control form-control-lg" name="conditions" type="text" placeholder="Conditions and waranties" required><br>
              <p>
                <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                  Optional Benefits
                </a>

              </p>
              <div class="collapse" id="collapseExample">
                <div class="card card-body">
              <input class="form-control form-control-lg" name="optionalbenefit0" type="text" placeholder="EG Access protector"><br>
              <input class="form-control form-control-lg" name="optionalbenefit1" type="number" step="any" placeholder="Optional BeneMinimum Premium"><br>
              <input class="form-control form-control-lg" name="optionalbenefit2" type="number" step="any" placeholder="Optional Benefits Rate(%)"><br>

                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>

          </div>
        </div>
      </div>


      <div class="col-sm-4"> 
        <div class="panel btn-secondary">
          <div class="panel-heading" data-toggle="modal" data-target="#agentmodal">Add Agent</div>
        </div>
      </div>


      <div class="modal fade" id="agentmodal" tabindex="-1" role="dialog" aria-labelledby="agentModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
          <form action="handle/agent.php" method="post" enctype="multipart/form-data"> 
            <div class="modal-header">
              <h5 class="modal-title" id="agentModalLabel">New Agent</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form enctype="multipart/form-data">
              <input class="form-control form-control-lg" name="contact" type="text" placeholder="Contact Person"><br>
              <input class="form-control form-control-lg" name="company" type="text" placeholder="Company Name/Individual full names"><br>
              <input class="form-control form-control-lg" name="reg" type="text" placeholder="ID/PP/REG. Cert. for company/businessr"><br>
              <input class="form-control form-control-lg" name="kra" type="text" placeholder="KRA Pin number"><br>
              <label for="profile" class="control-label">Upload KRA COPY</label>
              <input class="form-control form-control-lg" name="krafile" type="file" placeholder="krafile" required ><br>              
              <input class="form-control form-control-lg" name="adress" type="text" placeholder="Physical Address"><br>
              <input class="form-control form-control-lg" name="email" type="text" placeholder="Email Address"><br>
              <input class="form-control form-control-lg" name="phone" type="text" placeholder="Phone number"><br>
              <input class="form-control form-control-lg" name="tax" type="text" placeholder="Withholding Tax (%)"><br>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
          </div>
        </div>
      </div>




      <div class="col-sm-4"> 
        <div class="panel btn-secondary">
          <div class="panel-heading" data-toggle="modal" data-target="#clientmodal">Add Client</div>
        </div>
      </div>

      <div class="modal fade" id="clientmodal" tabindex="-1" role="dialog" aria-labelledby="clientModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <form action="handle/client.php" method="post" enctype="multipart/form-data"> 
            <div class="modal-header">
              <h5 class="modal-title" id="agentModalLabel">New Client</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form enctype="multipart/form-data">
              <input class="form-control form-control-lg" name="fname" type="text" placeholder="First Name/business name"><br>
              <input class="form-control form-control-lg" name="mname" type="text" placeholder="Middle Names/company /business name"><br>
              <input class="form-control form-control-lg" name="lname" type="text" placeholder="Last Name/company /business name"><br>
              <input class="form-control form-control-lg" name="reg" type="text" placeholder="ID/PP/REG. Cert. for Companies/business"><br>
              <label for="profile" class="control-label">Upload ID/PP/REG. Cert. for Companies/business</label>
              <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="ID/PP/REG. Cert. for Companies/business"><br>
              <input class="form-control form-control-lg" name="kra" type="text" placeholder="KRA PIN"><br>
              <label for="profile" class="control-label">Uload KRA PIN</label>
              <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="KRA PIN"><br>
              <input class="form-control form-control-lg" name="email" type="text" placeholder="EMAIL"><br>
              <input class="form-control form-control-lg" name="phone" type="text" placeholder="PHONE"><br>
              <input class="form-control form-control-lg" name="occupation" type="text" placeholder="OCCUPATION"><br>
              <input class="form-control form-control-lg" name="residence" type="text" placeholder="RESIDENCE"><br>
              <label for="profile" class="control-label">Upload profile picture(optional)</label>
              <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="PROFILE PHOTO/LOGO"><br>
              <label for="logbook" class="control-label">Upload Logbook (optional)</label>
              <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="VEHICLE DETAILS .LOGBOOK"><br>

              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
          </div>
        </div>
      </div>




      <div class="col-sm-4"> 
        <div class="panel btn-secondary">
          <div class="panel-heading"><a href="index.php#flexare" style="text-decoration: none; color: white;">Add Policy</a></div>
        </div>
      </div>
  </div>
</div>
 
<?php include 'progresschart.php';?>  
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading">New Business</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=5" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-danger">
        <div class="panel-heading">Renewal</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=8" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-success">
        <div class="panel-heading">Total Sales</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=100" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
  </div>
</div><br>

<div class="container">    
  <div class="row">
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading">Paid</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=6" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-primary">
        <div class="panel-heading">Out Standing</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=8" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-primary">
        <div class="panel-heading">Pending</div>
        <div class="panel-body"><img src="https://placehold.it/150x80/FFFFFF?text=3" class="img-responsive" style="width:100%" alt="Image"></div>
        <div class="panel-footer"></div>
      </div>
    </div>
  </div>
</div><br><br>


<footer class="container-fluid text-center">
  <p>Copyright</p>  
  <form class="form-inline">Get deals:
    <input type="email" class="form-control" size="50" placeholder="Email Address">
    <button type="button" class="btn btn-danger">Sign Up</button>
  </form>
</footer>

</body>
</html>