<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Card Hover Effects</title>
    <link rel="stylesheet" href="assets/css/procuct.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        max-width: 300px;
        text-align: center;
        font-family: arial;
        }

        .center {
            margin: auto;
            width: 40%;
            padding: 10px;
            }
        .title {
        color: grey;
        font-size: 18px;
        }

        .submit {
            padding: 34px;
        }
        button {
        border: none;
        outline: 0;
        display: inline-block;
        padding: 8px;
        color: white;
        background-color: #000;
        text-align: center;
        cursor: pointer;
        width: 100%;
        font-size: 18px;
        }

        a {
        text-decoration: none;
        font-size: 22px;
        color: black;
        }

        button:hover, a:hover {
        opacity: 0.7;
        
    }
    </style>
    
</head>
<body>
<?php
  require 'handle/db.php';
  $underwriter_Name = $_POST["underwriterid"];
  echo $underwriter_Name;    
  $sql = "SELECT count(*) as total FROM Product WHERE UNDERWRITER = '$underwriter_Name' AND (COVERAGE = 'Third Party And Theft' OR COVERAGE = 'Third Party Only')";
  $sql2 = "SELECT count(*) as total FROM Product WHERE UNDERWRITER = '$underwriter_Name' AND (COVERAGE = 'Comprehensive')";

  if($result = mysqli_query($conn, $sql)){
        $data=mysqli_fetch_assoc($result);
        $third_party = $data['total'];   
    }
    if($result2 = mysqli_query($conn, $sql2)){
        $data2=mysqli_fetch_assoc($result2);
        $comprehensive = $data2['total'];
    }
?>   
<!--- Available products for <? echo $underwriter_Name?>--->
<h2 style="text-align:center">Product Categories</h2>
<div class="center">
    <div class="card" style="float:left">
        
        <h1>Third Party</h1>
        <p class="title"><?echo $third_party ?> Products</p>
        <p>Description</p>
        <div style="margin: 24px 0;">
            <a href="#"><i class="fa fa-dribbble"></i></a> 
            <a href="#"><i class="fa fa-twitter"></i></a>  
            <a href="#"><i class="fa fa-linkedin"></i></a>  
            <a href="#"><i class="fa fa-facebook"></i></a> 
        </div>
        <form action="thirtparty.php" method="post">
            <input id="underwriterid" name="underwriterid" type="hidden" value="<?echo $underwriter_Name?>">
            <h3><button type="submit" class="submit" value="View Products">Select</button></h3>
        </form>
    </div>

    <div class="card" style="float:right">
        
        <h1>Comprehensive</h1>
        <p class="title"><?echo $comprehensive?> Products</p>
        <p>Description</p>
        <div style="margin: 24px 0;">
            <a href="#"><i class="fa fa-dribbble"></i></a> 
            <a href="#"><i class="fa fa-twitter"></i></a>  
            <a href="#"><i class="fa fa-linkedin"></i></a>  
            <a href="#"><i class="fa fa-facebook"></i></a> 
        </div>
        <form action="comprehensive.php" method="post">
            <input id="underwriterid" name="underwriterid" type="hidden" value="<?echo $underwriter_Name?>">
            <h3><button type="submit" class="submit" value="View Products">Select</button></h3>
        </form>
    </div>
</div>

</body>
</html>