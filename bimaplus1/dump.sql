-- MariaDB dump 10.17  Distrib 10.4.14-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: jendie
-- ------------------------------------------------------
-- Server version	10.4.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Agent`
--

DROP TABLE IF EXISTS `Agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) NOT NULL,
  `INFORMAION` varchar(255) DEFAULT NULL,
  `ID_PASSPORT` varchar(255) DEFAULT NULL,
  `ID_PASSPORT_COPY` varchar(255) DEFAULT NULL,
  `KRA_PIN` varchar(255) DEFAULT NULL,
  `KRA_PIN_COPY` varchar(255) DEFAULT NULL,
  `IRA_LICENSE` varchar(255) DEFAULT NULL,
  `IRA_LICENSE_COPY` varchar(255) DEFAULT NULL,
  `PHYSICAL_ADDRESS` varchar(255) DEFAULT NULL,
  `PHONE_NUMBER` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `USERNAME` (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Agent`
--

LOCK TABLES `Agent` WRITE;
/*!40000 ALTER TABLE `Agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `Agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Client`
--

DROP TABLE IF EXISTS `Client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Client` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Fname` varchar(255) DEFAULT NULL,
  `Lname` varchar(255) DEFAULT NULL,
  `Cert_for` varchar(255) DEFAULT NULL,
  `KRA_PIN` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `PHONE` varchar(255) DEFAULT NULL,
  `OCCUPATION` varchar(255) DEFAULT NULL,
  `RESIDENCE` varchar(255) DEFAULT NULL,
  `PROFILE_PHOTO` varchar(255) DEFAULT NULL,
  `VEHICLE_DETAILS` varchar(255) DEFAULT NULL,
  `MNAME` varchar(255) DEFAULT NULL,
  `CertFile` varchar(255) DEFAULT NULL,
  `KRAFILE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uc_KRA_PIN` (`KRA_PIN`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Client`
--

LOCK TABLES `Client` WRITE;
/*!40000 ALTER TABLE `Client` DISABLE KEYS */;
INSERT INTO `Client` VALUES (1,'Erick','Ngumbau','325422','5656565656','erick.soi@hotmail.com','0712962787','Programmer','Embakasi','clientfiles/15927934262bhic_nl (2).csv','clientfiles/15927934263data_spider.py','Soi','clientfiles/15927934260action_doc.odt','clientfiles/15927934261bhic_nl.csv');
/*!40000 ALTER TABLE `Client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContactUs`
--

DROP TABLE IF EXISTS `ContactUs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContactUs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContactUs`
--

LOCK TABLES `ContactUs` WRITE;
/*!40000 ALTER TABLE `ContactUs` DISABLE KEYS */;
INSERT INTO `ContactUs` VALUES (1,'+254 722 301 062');
/*!40000 ALTER TABLE `ContactUs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Coverage`
--

DROP TABLE IF EXISTS `Coverage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Coverage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `cover` varchar(255) NOT NULL,
  `Value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Coverage`
--

LOCK TABLES `Coverage` WRITE;
/*!40000 ALTER TABLE `Coverage` DISABLE KEYS */;
INSERT INTO `Coverage` VALUES (1,'Third Party Only','third'),(2,'Third Party And Theft','thirdAndT'),(3,'Comprehensive','comprehensive');
/*!40000 ALTER TABLE `Coverage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Persons`
--

DROP TABLE IF EXISTS `Persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Persons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `recog` varchar(255) NOT NULL,
  `kra` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Persons`
--

LOCK TABLES `Persons` WRITE;
/*!40000 ALTER TABLE `Persons` DISABLE KEYS */;
/*!40000 ALTER TABLE `Persons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_NAME` varchar(255) DEFAULT NULL,
  `RISK_COVERED` varchar(255) DEFAULT NULL,
  `UNDERWRITER` varchar(255) DEFAULT NULL,
  `COVERAGE` varchar(255) DEFAULT NULL,
  `SHORTTERM_RATES` varchar(255) DEFAULT NULL,
  `ANNUAL_RATES` varchar(255) DEFAULT NULL,
  `CLAUSES` varchar(255) DEFAULT NULL,
  `CONDITIONS_AND_WARRANTIES` varchar(255) DEFAULT NULL,
  `OPTIONAL_ADDITIONAL_BENEFITS` varchar(255) DEFAULT NULL,
  `OP_BMP` varchar(255) DEFAULT NULL,
  `OP_BR` varchar(255) DEFAULT NULL,
  `VEHICLE_TYPE` varchar(255) DEFAULT NULL,
  `PRODUCT_LOGO` varchar(255) DEFAULT NULL,
  `PRODUCT_IDENTIFIER` varchar(255) DEFAULT NULL,
  `YEAR_ACCEPTED` varchar(255) DEFAULT NULL,
  `UNIQUEID` varchar(255) DEFAULT NULL,
  `TONNAGE` varchar(255) DEFAULT NULL,
  `MINIMUM_PREMIUM` varchar(255) DEFAULT NULL,
  `UNDERWRITER_EMAIL` varchar(255) DEFAULT NULL,
  `POLICY_BENEFITS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUEID` (`UNIQUEID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product`
--

LOCK TABLES `Product` WRITE;
/*!40000 ALTER TABLE `Product` DISABLE KEYS */;
INSERT INTO `Product` VALUES (1,'Bimaplus(MOTORINSURANCE)','PSV (bodaboda)','The Monarch Insurance Company Limited','Third Party Only','THIRD PARTY','3500','TBA','TBA','EXCESS PROTECTOR','2500','0.25','MOTORCYCLE','images/productunderwriter/H43itOQziq_DIRECT LINE.png','RPhCv9sQg4','NA','Bimaplus(MOTORINSURANCE)PSV (bodaboda)The Monarch Insurance Company LimitedThird Party Only','NA','NA',NULL,NULL),(2,'Bimaplus(MOTORINSURANCE)','PSV (bodaboda)','The Monarch Insurance Company Limited','Comprehensive','COMPREHENSIVE','3','TBA','TBA','PERSONAL ACCIDENT','2500','2500','MOTORCYCLE','images/productunderwriter/he03H9W2GK_Monarch.png','LZzRqgi4c6','2005','Bimaplus(MOTORINSURANCE)PSV (bodaboda)The Monarch Insurance Company LimitedComprehensive','NA','7100',NULL,NULL),(3,'Bimaplus(MOTORINSURANCE)','Private','AIG Kenya Insurance Company Limited','Third Party Only','THIRD PARTY ONLY','7500','TBA','TBA','','','','MOTORVEHICLE','images/productunderwriter/rltNjiwCcc_AIG.png','pSxg3XIyPP','NA','Bimaplus(MOTORINSURANCE)PrivateAIG Kenya Insurance Company LimitedThird Party Only','NA','NA',NULL,NULL),(4,'Bimaplus(MOTORINSURANCE)','Comercial Own goods','AIG Kenya Insurance Company Limited','Comprehensive','COMPREHENSIVE','5','TBA','TBA','Excess Protector','2500','0.25','MOTORVEHICLE','images/productunderwriter/d9ue9UrnAW_AIG.png','W0UIkZ6bZT','2005','Bimaplus(MOTORINSURANCE)Comercial Own goodsAIG Kenya Insurance Company LimitedComprehensive','NA','30000',NULL,NULL),(5,'Bimaplus(MOTORINSURANCE)','Private','Trident Insurance Company Limited','Third Party Only','Any cost arising from any legal proceedings and the costs of repairing the other parties vehicle or any compensation granted to third party persons due to injury or death','7500','TBA','TBA','','','','MOTORVEHICLE','assets/productunderwriter/pAaZFN32Cj_TRIDENT INSURANCE.jfif','ggjEphcAuU','NA','Bimaplus(MOTORINSURANCE)PrivateTrident Insurance Company LimitedThird Party Only','NA','NA',NULL,NULL);
/*!40000 ALTER TABLE `Product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductList`
--

DROP TABLE IF EXISTS `ProductList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductList` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductList`
--

LOCK TABLES `ProductList` WRITE;
/*!40000 ALTER TABLE `ProductList` DISABLE KEYS */;
INSERT INTO `ProductList` VALUES (1,'Bimaplus(MOTORINSURANCE)');
/*!40000 ALTER TABLE `ProductList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product_additional_benefits`
--

DROP TABLE IF EXISTS `Product_additional_benefits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product_additional_benefits` (
  `PRODUCT_IDENTIFIER` varchar(255) DEFAULT NULL,
  `OPTIONAL_ADDITIONAL_BENEFITS` varchar(255) DEFAULT NULL,
  `OP_BMP` varchar(255) DEFAULT NULL,
  `OP_BR` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product_additional_benefits`
--

LOCK TABLES `Product_additional_benefits` WRITE;
/*!40000 ALTER TABLE `Product_additional_benefits` DISABLE KEYS */;
INSERT INTO `Product_additional_benefits` VALUES ('RPhCv9sQg4','EXCESS PROTECTOR','2500','0.25'),('RPhCv9sQg4','PASSENGER LEGAL LIABILITY','500','500'),('RPhCv9sQg4','WINDSCREEN','1000','10'),('LZzRqgi4c6','PERSONAL ACCIDENT','2500','2500'),('pSxg3XIyPP','PERSONAL ACCIDENT','2500','2500'),('W0UIkZ6bZT','Excess Protector','2500','0.25'),('W0UIkZ6bZT','PASSENGER LEGAL LIABILITY','500','500'),('W0UIkZ6bZT','POLITICAL VIOLENCE AND TERRORISM','2500','0.25'),('W0UIkZ6bZT','Personal Accident','1500','1500'),('W0UIkZ6bZT','WINDSCREEN','1000','10'),('I5HxM4XBcH','EXCESS PROTECTOR','5000','0.5'),('KrnHIPlD4L','EXCESS PROTECTOR','5000','0.5'),('JwZ3ybiYlN','EXCESS PROTECTOR','5000','0.5');
/*!40000 ALTER TABLE `Product_additional_benefits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Restricted_Vehicles`
--

DROP TABLE IF EXISTS `Restricted_Vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Restricted_Vehicles` (
  `PRODUCT_IDENTIFIER` varchar(255) DEFAULT NULL,
  `Vehicle` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Restricted_Vehicles`
--

LOCK TABLES `Restricted_Vehicles` WRITE;
/*!40000 ALTER TABLE `Restricted_Vehicles` DISABLE KEYS */;
INSERT INTO `Restricted_Vehicles` VALUES ('RPhCv9sQg4','Isuzu'),('RPhCv9sQg4','Isuzu');
/*!40000 ALTER TABLE `Restricted_Vehicles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Underwriter`
--

DROP TABLE IF EXISTS `Underwriter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Underwriter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `LEGAL_ENTITY` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `WEBSITE` varchar(255) DEFAULT NULL,
  `LOGO` varchar(255) DEFAULT NULL,
  `LOCATION_OPTIONAL` varchar(255) DEFAULT NULL,
  `BANK_ACCOUNT_OPTIONAL` varchar(255) DEFAULT NULL,
  `UNIQUE_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uc_name` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Underwriter`
--

LOCK TABLES `Underwriter` WRITE;
/*!40000 ALTER TABLE `Underwriter` DISABLE KEYS */;
INSERT INTO `Underwriter` VALUES (1,'AIG KENYA INSURANCE COMPANY LIMITED','AIG Is A World Leading Property-casualty And General Insurance Organization.','P.O. Box 49460 -00100.Nairobi-Kenya','aigkenya@aig.com','www.aig.co.ke','','Eden Square ComplexChiromo Road. ','TBA','It%_VJ*kCbglMjif9h#n');
/*!40000 ALTER TABLE `Underwriter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UnderwriterList`
--

DROP TABLE IF EXISTS `UnderwriterList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UnderwriterList` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UnderwriterList`
--

LOCK TABLES `UnderwriterList` WRITE;
/*!40000 ALTER TABLE `UnderwriterList` DISABLE KEYS */;
INSERT INTO `UnderwriterList` VALUES (1,'AAR Insurance Company Limited','img/insurance/aar-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(2,'Africa Merchant Assurance Company Limited','img/insurance/amaco-logo.jpeg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(3,'AIG Kenya Insurance Company Limited','img/insurance/aig-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(4,'Allianz Insurance Company of Kenya Limited','img/insurance/alianz-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(5,'APA Insurance Limited','img/insurance/apa-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(6,'APA Life Assurance Company Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(7,'Barclays Life Assurance Kenya Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(8,'Britam General Insurance Company (K) Limited','img/insurance/britam-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(9,'Britam Life Assurance Company (K) Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(10,'Metropolitan Cannon General Insurance Company Limited','img/insurance/metropolitan-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(11,'Capex Life Assurance Company Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(12,'CIC General Insurance Company Limited','img/insurance/cic-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(13,'CIC Life Assurance Company Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(14,'Corporate Insurance Company Limited','img/insurance/coporate-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(15,'Directline Assurance Company Limited','img/insurance/direct_line-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(16,'Fidelity Shield Insurance Company Limited','img/insurance/fidelity-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(17,'First Assurance Company Limited','img/insurance/first_assurance-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(18,'GA Insurance Limited','img/insurance/ga-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(19,'GA Life Assurance Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(20,'Geminia Insurance Company Limited','img/insurance/geminia-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(21,'ICEA LION General Insurance Company Limited','img/insurance/icea-logo.svg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(22,'ICEA LION Life Assurance Company Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(23,'Intra Africa Assurance Company Limited','img/insurance/intra-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(24,'Invesco Assurance Company Limited','img/insurance/invesco-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(25,'Jubilee General Insurance Limited','img/insurance/jubilee-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(26,'Jubilee Health Insurance Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(27,'Kenindia Assurance Company Limited','img/insurance/kenindia-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(28,'Kenya Orient Insurance Limited','img/insurance/kenya_orient-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(29,'Kenya Orient Life Assurance Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(30,'KUSCCO Mutual Assurance Limited','img/insurance/kuscco-logo.jpg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(31,'Liberty Life Assurance Kenya Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(32,'Madison Insurance Company Kenya Limited','img/insurance/madison-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(33,'Madison General Insurance Kenya Limited','img/insurance/madison-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(34,'Mayfair Insurance Company Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(35,'Metropolitan Cannon Life Assurance Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(36,'Occidental Insurance Company Limited','img/insurance/occidental-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(37,'Old Mutual Assurance Company Limited','img/insurance/old-mutual-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(38,'Pacis Insurance Company Limited','img/insurance/Pacis-logo.jpeg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(39,'MUA Insurance ( Kenya) Limited 01','img/insurance/mua-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(40,'Pioneer General Insurance Company Limited','img/insurance/pioneer-logo.jpeg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(41,'Pioneer Assurance Company Limited','img/insurance/pioneer-logo.jpeg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(42,'Prudential Life Assurance Company Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(43,'Resolution Insurance Company Limited','img/insurance/resolution-logo.jpeg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(44,'Saham Assurance Company Kenya Limited','img/insurance/saham.jpeg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(45,'Sanlam General Insurance Company Limited','img/insurance/sanlam-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(46,'Sanlam Life Insurance Company Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(47,'Takaful Insurance of Africa Limited','img/insurance/takaful-logo.jpeg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(48,'Tausi Assurance Company Limited','img/insurance/takaful-logo.jpeg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(49,'The Heritage Insurance Company Limited','img/insurance/herritage-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(50,'The Jubilee Insurance Company of Kenya Limited','img/insurance/jubilee-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(51,'The Kenyan Alliance Insurance Company Limited','img/insurance/kenya-alliance-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(52,'The Monarch Insurance Company Limited','img/insurance/monarch-logo.png','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(53,'Trident Insurance Company Limited','img/insurance/trident-logo.jpeg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(54,'UAP Insurance Company Limited','img/insurance/uap-logo.svg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(55,'UAP Life Assurance Limited',NULL,'knyaga@iplus.co.ke','Small description about the Insurance company goes here.'),(56,'Xplico Insurance Company Limited','img/insurance/xplico-logo.jpeg','knyaga@iplus.co.ke','Small description about the Insurance company goes here.');
/*!40000 ALTER TABLE `UnderwriterList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Fname` varchar(255) DEFAULT NULL,
  `Lname` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `ID_no` varchar(255) DEFAULT NULL,
  `KRA` varchar(255) DEFAULT NULL,
  `Bank_No` varchar(255) DEFAULT NULL,
  `NSSF` varchar(255) DEFAULT NULL,
  `NHIF` varchar(255) DEFAULT NULL,
  `Signature` varchar(255) DEFAULT NULL,
  `ACCESS` varchar(255) DEFAULT NULL,
  `IDIMG` varchar(255) DEFAULT NULL,
  `KRADOC` varchar(255) DEFAULT NULL,
  `BANKIMG` varchar(255) DEFAULT NULL,
  `NSSFDOC` varchar(255) DEFAULT NULL,
  `NHIFDOC` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uc_ID_no` (`ID_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ValuationCenters`
--

DROP TABLE IF EXISTS `ValuationCenters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ValuationCenters` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Town` varchar(255) DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `Tel` varchar(255) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `ContactPerson` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ValuationCenters`
--

LOCK TABLES `ValuationCenters` WRITE;
/*!40000 ALTER TABLE `ValuationCenters` DISABLE KEYS */;
INSERT INTO `ValuationCenters` VALUES (1,'NAIROBI','2nd floor, rm 2B Muthithi Rd,Westlands','0202632578','info@regentautovaluers.co.ke','Imelda'),(2,'NAIROBI','Luther Plaza, University Way Roundabout, Nyerere Rd P.O Box 34365 – 00100 Nairobi, Kenya','0202603906,0722608210,0700394000,0700393777','lutheran@regentautovaluers.co.ke','Mr. Festus Kaleli'),(3,'NAIROBI','Muthaiga Square, 3rd Floor','0612303476,0728292912','pangani@regentautovaluers.co.ke','Mr. Alex'),(4,'NAIROBI','Westlands, Muthithi Rd, Opposite avocado Towers','0202603916,0711599665','westlands@regentautovaluers.co.ke','Mr. Robert Mwangi.'),(5,'ELDORET','WATERGATE PLAZA','0202134075,0700077009','eldoret@regentautovaluers.co.ke','Mr. Khaemba Andrew'),(6,'KISUMU','AWORI HOUSE','0202134077,0724228228','kisumu@regentautovaluers.co.ke','Mr. Enock Ochieng.'),(7,'MALINDI','Royal Complex Rm no G15,','0712840088','regentmalindi@gmail.com','Mr. Mwangi'),(8,'NAROK','NENKAI PLAZA','07184333330779338531,','narok@regentautovaluers.co.ke','Mr. Muli'),(9,'NAIROBI','Upperhill, Matumbato Rd','0202605723,0704879000','upperhill@regentautovaluers.co.ke','Mr. Munene'),(10,'NAITOBI','Utawala, Grey Park Heights, 1st Flr, Room A17','0770626463,0790884101','utawala@regentautovaluers.co.ke','Mr. Tabale'),(11,'NAIROBI','Buruburu Complex – Nairobi, Room B3','0202603915,0702501050','buruburu@regentautovaluers.co.ke','Mr. Elias Mwangi.'),(12,'NGONG','Along Ngong Road (from town) 50 Meters from China Center, At Jameson Court Block B Office No. 10','0713092158,0772733808','ngongroad@regentautovaluers.co.ke','Mr. Maina'),(13,'KITALE','AMBWERE PLAZA','0702512727','regentbungoma@gmail.com,kitale@regentautovaluers.co.ke','Mr. Kepha.'),(14,'MACHAKOS','PEMA HOUSE','0771569035,0728969509,0719723650,0720274150','machakos@regentautovaluers.co.ke','Mr. Muli'),(15,'THIKA','EQUITY PLAZA','0202693664,0712636368,0705198131','thika@regentautovaluers.co.ke','Mr. Nelson Kiirinya'),(16,'MERU','TWIN PLAZA','0202603909,0718094777','meru@regentautovaluers.co.ke','Mr. Njeru'),(17,'NAKURU','MASTERS PLAZA 4TH FLOOR :WING 2 :ROOM 1','0202134076,0727707017','nakuru@regentautovaluers.co.ke','Mr. Lawrence Macharia'),(18,'MOMBASA','YUNIS BUILDING 3RD FLOOR ROOM NO.4','0412220430,0727707016','mombasa@regentautovaluers.co.ke','Mr. Waweru'),(19,'NYERI','GATHII HSE','0612034027,0723697400','nyeri@regentautovaluers.co.ke','Mr. Mukundi.'),(20,'EMBU','NEEMA PLAZA','0202603918,0712839944','embu@regentautovaluers.co.ke','Mr. Mukangu'),(21,'KISII','UPENDO PLAZA','0715710257,0701714652','kisii@regentautovaluers.co.ke','Mr. Caleb Oyugi'),(22,'RUIRU','SANFRED HOUSE, SECOND FLOOR, RM 205','0772684059,0708156785','ruiru@regentautovaluers.co.ke','Mr. Kuko'),(23,'KITUI','KCB BUILDING 2ND FLOOR','0711599775','regentkitui@gmail.com','Mr. Sylvester Mulei'),(24,'KERUGOYA','','0704262478','kerugoya@regentautovaluers.co.ke','Mr. Munene'),(25,'NANYUKI','Nanyuki rd, Chadwick house ground floor','0704262613','nanyuki@regentautovaluers.co.ke','Mr. Murugami'),(26,'NAIVASHA','DOVE COMPLEX 1ST FLOOR ROOM 17, KARIUKI CHOTARA ROAD','0704262501,0772733809','naivasha@regentautovaluers.co.ke','Mr. Mwangi'),(27,'NAIROBI','Tymes Arcade 2nd Flr Rm 212, ONGATA RONGAI','254774086657','rongai@regentautovaluers.co.ke','Mr. Thiba.'),(28,'KAJIADO','Betty Plaza, 3rd floor, rm 108 ?, Kitengela','0772684060,0700158152','kitengela@regentautovaluers.co.ke','Mr. Adan Galgalo'),(29,'NYALI','','0700158124,0772762883','nyali@regentautovaluers.co.ke','Mr. Murangiri'),(30,'KAKAMEGA','Emmisioma Building 2nd Flr behind Post Bank, Along Cannon Awori Street','254790122193','kakamega@regentautovaluers.co.ke','Mr. Dennis Buyema.'),(31,'NYAHURURU','','0724276242','regentnyahururu@gmail.com','Mr. Nahashon Kariuki'),(32,'KERICHO','JEMI PLAZA ROOM NO.BA9, Next to Bank of Africa Opposite Garden Hotel','0712840202,0772762874','kericho@regentautovaluers.co.ke','Mr. Zablon'),(33,'MOMBASA ROAD','Nextgen Mall','0799396534,0778005571','nextgen@regentautovaluers.co.ke','Mr. Stanley Kioko.'),(34,'KIAMBU','Quick Matt, Komrades Business Center Room 7.','0775550802,0716601759','kiambu@regentautovaluers.co.ke','Mr. Maina'),(35,'KIKUYU','Ivory Towers, Room B4','0793276932,0771099216','kikuyu@regentautovaluers.co.ke','Mr. Daniel Kinyua');
/*!40000 ALTER TABLE `ValuationCenters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VehicleClass`
--

DROP TABLE IF EXISTS `VehicleClass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VehicleClass` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VehicleClass`
--

LOCK TABLES `VehicleClass` WRITE;
/*!40000 ALTER TABLE `VehicleClass` DISABLE KEYS */;
INSERT INTO `VehicleClass` VALUES (1,'MOTORCYCLE','Private'),(2,'MOTORCYCLE','PSV (bodaboda)'),(3,'TRICYCLE','Comercial Own goods'),(4,'TRICYCLE','PSV (tuktuk)'),(5,'MOTORVEHICLE','Private'),(6,'MOTORVEHICLE','Comercial Own goods'),(7,'MOTORVEHICLE','General Cartage Lorries,Trucks and Tankers'),(8,'MOTORVEHICLE','Agricultural and Forestry vehicles'),(9,'MOTORVEHICLE','Chauffeur driven'),(10,'MOTORVEHICLE','Motor trade'),(11,'MOTORVEHICLE','Institutional Vehicles'),(12,'MOTORVEHICLE','Driving school Vehicle'),(13,'MOTORVEHICLE','Tour Service Vehicles'),(14,'MOTORVEHICLE','PSV - Matatu'),(15,'MOTORVEHICLE','PSV - Taxi'),(16,'MOTORVEHICLE','Ambulance and fire fighters'),(17,'MOTORVEHICLE','Forklift,Crane, Rollers and Excavators');
/*!40000 ALTER TABLE `VehicleClass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurance`
--

DROP TABLE IF EXISTS `insurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plate` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `delivery` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurance`
--

LOCK TABLES `insurance` WRITE;
/*!40000 ALTER TABLE `insurance` DISABLE KEYS */;
/*!40000 ALTER TABLE `insurance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logbook`
--

DROP TABLE IF EXISTS `logbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration` varchar(255) DEFAULT NULL,
  `chasis` varchar(255) DEFAULT NULL,
  `make` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `body` varchar(255) DEFAULT NULL,
  `fuel` varchar(255) DEFAULT NULL,
  `man_year` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `engine_number` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `reg_date` varchar(255) DEFAULT NULL,
  `gross_weight` varchar(255) DEFAULT NULL,
  `duty` varchar(255) DEFAULT NULL,
  `previous_owners` varchar(255) DEFAULT NULL,
  `passengers` varchar(255) DEFAULT NULL,
  `tare_weight` varchar(255) DEFAULT NULL,
  `tax_class` varchar(255) DEFAULT NULL,
  `axels` varchar(255) DEFAULT NULL,
  `load_capacity` varchar(255) DEFAULT NULL,
  `reg_country` varchar(255) DEFAULT NULL,
  `previous_reg` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `id_file` varchar(255) DEFAULT NULL,
  `kra_number` varchar(255) DEFAULT NULL,
  `kra_file` varchar(255) DEFAULT NULL,
  `logbook_file` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logbook`
--

LOCK TABLES `logbook` WRITE;
/*!40000 ALTER TABLE `logbook` DISABLE KEYS */;
INSERT INTO `logbook` VALUES (1,'dd','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll'),(2,'dd','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll'),(3,'dd','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll'),(4,'dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd'),(5,'tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','tt','t','tt','tt'),(6,'fff','fff','fff','fff','fff','ffff','fff','fff','fff','fff','fff','fff','fff','fff','fff','fff','fff','fff','fff','fff','fff','fff','fff','fff','fff','ffff','fff','ffff'),(7,'4444','4444','dd','dd','dd','dd','dd','ddd','ddd','ddd','ddd','ddd','ddd','dd','dd','ddd','dd','ddd','ddd','ddd','ddd','dd','dd','dd','ddd','dd','ddd','ddd'),(8,'sasasa','trtrt','gfgfgf','gfgfg','gfgfg','gfgfg','gfgfg','gfgf','gfg','gfgfg','','','','','gfgfg','ggfgfg','gfgfg','gfgfgf','','gfgfg','gfgfg','gfgfg','gfgfg','gfgfg','fgfgf','gfgfg','gfgfg','gfgfg'),(9,'dd','ff','gg','gg','gg','gg','g','gg','g','gg','gg','g','gg','ggg','ggg','ggg','ggg','gg','gg','gg','gg','gg','gg','gg','gg','gg','',''),(10,'sasa','sasa','sasas','sasas','sasas','sasas','sasasa','sasas','sasas','','','','','sasas','sasa','sasas','sasas','sasas','sasas','sasas','sasas','sasas','sasas','sasas','sasas','','',''),(11,'sasa','sasa','dsdsd','dsds','','','','','','','','','','sasas','sasa','sasas','sasas','sasas','sasas','sasas','sasas','sasas','sasas','sasas','sasas','','',''),(12,'sasa','sasa','ddd','','','','','','','','','','','sasas','sasa','sasas','sasas','sasas','sasas','sasas','sasas','sasas','sasas','sasas','sasas','','',''),(13,'dsdsd','dsdsd','gghgh','hghgh','ghgh','hghg','hghgh','ghhgh','hghgh','hghgh','hghgh','hghgh','hghgh','hghgh','hghgh','hgh','ghgh','hghgh','hghgh','hghgh','hghgh','hghgh','hghgh','hgh','hghgh','hhgh','hghgh','hghhg'),(14,'ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','w','ww','ww','ww','ww','ww','ww','ww'),(15,'www','ww','ww','ww','ww','ww','ww','www','ww','ww','ww','www','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww','ww'),(16,'sasa','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','ll','lll','ll','ll'),(17,'ddd','ddd','ddd','ddd','ddd','ddd','dddd','dddd','ddd','ddd','ddd','ddd','ddd','ddd','ddd','ddd','ddd','ddd','ddd','ddd','ddd','dd','ddd','ddd','ddd','ddd','ddd','ddd'),(18,'ddDD','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd','dd'),(19,'d','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j'),(20,'sas','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo','oo'),(21,'sasasa','sasas','sss','ss','sss','sss','sss','ssss','ssss','ssss','ssss','ssss','sss','sss','ss','ss','ss','sss','ss','ss','ss','ss','ss','ss','ss','ss','ss','ss'),(22,'ssas','jhhh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh'),(23,'dd','kk','kk','kkk','k','kk','k','kk','k','','kkk','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k'),(24,'sa','dd','hh','hh','hh','hh','hh','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','hh','h','h'),(25,'d','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h'),(26,'xxa','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h'),(27,'d','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','h','','h','h','h','h','h','h','h','h','h'),(28,'k','k','k','k','k','k','kk','','kk','','k','k','k','k','k','k','k','k','k','k','k','k','k','kk','','kk','k','k'),(29,'dd','jj','jj','jj','jj','jj','jj','2021','jj','jj','jj','jj','jj','jj','jj','jjj','jj','jj','jj','jj','jj','jj','jj','jj','jj','jj','j','jj'),(30,'sasa','lll','ll','ll','ll','ll','ll','2021','ll','ll','ll','2021-01-01','6','6','6','6','6','6','6','6','6','6','','545','example_061.pdf','55','example_061.pdf','example_061.pdf'),(31,'sasa','lll','ll','ll','ll','ll','ll','2021','ll','ll','ll','2021-01-01','6','6','6','6','6','6','6','6','6','6','','545','example_061.pdf','55','example_061.pdf','example_061.pdf'),(32,'dd','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hh','hhh','hh','hhh','hh','hh','hh','hh','hh','hh','hh','hh','h','hh','hh','hh'),(33,'dd','dd','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','','l','l','l','l','l'),(34,'d','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j','j'),(35,'d','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','k','2021-01-01','k','k','k','k','k'),(36,'d','k','k','k','k','k','k','k','s','s','s','2021-01-01','3','ss','ss','4','4','s','4','4','s','s','2021-01-01','4545','example_061.pdf','dd','example_061.pdf','example_061.pdf'),(37,'a','l','l','l','l','l','l','44','l','l','l','2021-01-01','5','56','5','56','5','5','56','5','65','5','0005-05-05','45','example_061.pdf','sa','example_061.pdf','example_061.pdf'),(38,'kkkk','kkkk','kk','k','k','kk','k','2','ll','ll','ll','2021-01-01','2','ss','2','2','2','kk','1','2','l','l','2021-01-01','555','example_061.pdf','ssss','example_061.pdf','example_061.pdf'),(39,'DD','dd','dd','dd','dd','dd','dd','55','dd','d','sss','2021-01-01','1','sss','1','1','1','ss','1','1','ss','ss','2021-01-01','ssss','example_061.pdf','ss','example_061.pdf','example_061.pdf'),(40,'ss','k','k','k','k','k','k','55','k','55','55','2021-01-01','1','55','55','55','55','55','55','55','55','55','2021-01-01','55','example_061.pdf','dsdsd','example_061.pdf','example_061.pdf'),(41,'o','o','o','o','o','o','o','5','o','o','5','2021-01-01','5','5','5','5','5','5','5','5','5','5','0005-05-05','5','example_061.pdf','5','example_061.pdf','example_061.pdf'),(42,'ss','k','k','k','k','k','k','8','5','5','5','0005-05-05','5','5','5','5','5','5','5','5','5','5','0005-05-05','5','example_061.pdf','5','example_061.pdf','example_061.pdf'),(43,'dd','ll','l','l','l','l','55','55','55','55','55','0005-05-05','55','55','55','55','555','555','555','555','555','555','0005-05-05','555','example_061.pdf','555','example_061.pdf','example_061.pdf'),(44,'ll','ll','555','555','555','555','555','555','555','555','555','0005-05-05','555','555','555','555','555','555','555','555','555','555','0005-05-05','555','example_061.pdf','555','example_061.pdf','example_061.pdf'),(45,'555','555','555','55','555','555','555','555','555','555','555','0005-05-05','555','555','555','555','555','555','555','555','555','555','0005-05-05','555','example_061.pdf','555','example_061.pdf','example_061.pdf'),(46,'555','555','555','555','555','555','555','555','555','555','555','0005-05-05','555','555','555','555','555','555','55','555','555','555','0005-05-05','555','example_061.pdf','555','example_061.pdf','example_061.pdf'),(47,'yy','yy','yy','yy','yy','yy','yy','','yy','yy','yy','0005-05-05','55','55','55','55','55','55','55','55','55','55','0005-05-05','55','example_061.pdf','55','example_061.pdf','example_061.pdf'),(48,'tt','tt','tt','tt','555','555','555','555','555','555','555','0005-05-05','555','555','555','555','555','555','55','5555','555','555','0055-05-05','55','example_061.pdf','666','example_061.pdf','example_061.pdf'),(49,'44','tt','tt','tt','tt','tt','tt','5555','5555','5555','5555','0055-05-05','5555','5555','5555','5555','5555','5555','5555','5555','5555','5555','0055-05-05','5555','example_061.pdf','5555','example_061.pdf','example_061.pdf');
/*!40000 ALTER TABLE `logbook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `role` varchar(255) DEFAULT NULL,
  `companyname` varchar(255) DEFAULT NULL,
  `contactperson` varchar(255) DEFAULT NULL,
  `krapin` varchar(255) DEFAULT NULL,
  `krapincopy` varchar(255) DEFAULT NULL,
  `emailaddress` varchar(255) DEFAULT NULL,
  `phonenumber` varchar(255) DEFAULT NULL,
  `physicaladdress` varchar(255) DEFAULT NULL,
  `idnumber` varchar(255) DEFAULT NULL,
  `idcopy` varchar(255) DEFAULT NULL,
  `iralicense` varchar(255) DEFAULT NULL,
  `iracopy` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Erick','$2y$10$9hEq0DiBL1Wo9rkP4/M.gemrj7ClDmstJGtWPM8ue4F/Qt5S.zVqy','2021-01-11 10:50:34','default','DDD','Erick Soi','30018214','test.py','ericksoi3709@gmail.com','0712962787','Embakasi','30018214','85987458','','six.py','Erick','Soi');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-15 10:08:50
