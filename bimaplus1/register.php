<?php
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$username = $password = $confirm_password = $firstname = $lastname = $companyname = $contactperson = $krapin = $krapincopy = $emailaddress = $phonenumber = $physicaladdress= $idnumber = $idcopy = $iralicense = $iracopy = "";
$username_err = $password_err = $confirm_password_err = $firstname_err = $lastname_err = $companyname_err = $contactperson_err = $krapin_err = $krapincopy_err = $emailaddress_err = $phonenumber_err = $physicaladdress_err = $idnumber_err = $idcopy_err = $iralicense_err = $iracopy_err = "";
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    print_r($_POST);
    // Validate first name
    if(empty(trim($_POST["firstname"]))){
        $firstname_err = "Please enter first name.";
    } else{
        $firstname = trim($_POST["firstname"]);
    }

    // Validate first name
    if(empty(trim($_POST["lastname"]))){
        $lastname_err = "Please enter last name.";
    } else{
        $lastname = trim($_POST["lastname"]);
    }

    // Validate first name
    if(empty(trim($_POST["companyname"]))){
        $companyname_err = "Please enter company name.";
    } else{
        $companyname = trim($_POST["companyname"]);
    }

    // Validate first name
    if(empty(trim($_POST["contactperson"]))){
        $contactperson_err = "Please enter conterct person name.";
    } else{
        $contactperson = trim($_POST["contactperson"]);
    }

    // Validate first name
    if(empty(trim($_POST["krapin"]))){
        $krapin_err = "Please enter kra pin.";
    } else{
        $krapin = trim($_POST["krapin"]);
    }

    // Validate first name
    if(empty(trim($_POST["krapincopy"]))){
        $krapincopy_err = "Please upload kra copy.";
    } else{
        $krapincopy = trim($_POST["krapincopy"]);
    }

    // Validate first name
    if(empty(trim($_POST["emailaddress"]))){
        $emailaddress_err = "Please enter email address.";
    } else{
        $emailaddress = trim($_POST["emailaddress"]);
    }
    
    // Validate first name
    if(empty(trim($_POST["phonenumber"]))){
        $phonenumber_err = "Please enter phone number.";
    } else{
        $phonenumber = trim($_POST["phonenumber"]);
    }

    // Validate first name
    if(empty(trim($_POST["physicaladdress"]))){
        $physicaladdress_err = "Please enter place of residence.";
    } else{
        $physicaladdress = trim($_POST["physicaladdress"]);
    }
    
    // Validate first name
    if(empty(trim($_POST["idnumber"]))){
        $idnumber_err = "Please enter id number.";
    } else{
        $idnumber = trim($_POST["idnumber"]);
    }

    // Validate first name
    if(empty(trim($_POST["idcopy"]))){
        $idcopy_err = "Please upload id copy.";
    } else{
        $idcopy = trim($_POST["idcopy"]);
    }

    // Validate first name
    if(empty(trim($_POST["iralicense"]))){
        $iralicense_err = "Please enter ira license.";
    } else{
        $idcopy = trim($_POST["iralicense"]);
    }
    
    // Validate first name
    if(empty(trim($_POST["iracopy"]))){
        $iracopy_err = "Please upload ira license copy.";
    } else{
        $iracopy = trim($_POST["iracopy"]);
    }
    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter a username.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "This username is already taken.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
    
    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        
        // Prepare an insert statement
        $sql = "INSERT INTO users (username, firstname, lastname, companyname, contactperson, krapin, krapincopy, emailaddress, phonenumber, physicaladdress, idnumber, idcopy, iralicense, iracopy, password, role) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ssssssssssssssss", $param_username, $param_firstname, $param_lastname, $param_companyname, $param_contactperson, $param_krapin, $param_krapincopy, $param_emailaddress, $param_phonenumber, $param_physicaladdress, $param_idnumber, $param_idcopy, $param_iralicense, $param_iracopy, $param_password, $param_role);
            
            // Set parameters
            $param_firstname = $firstname;
            $param_lastname = $lastname;
            $param_username = $username;
            $param_companyname = $companyname;
            $param_contactperson = $contactperson;
            $param_krapin = $krapin;
            $param_krapincopy = $krapincopy;
            $param_emailaddress = $emailaddress;
            $param_phonenumber = $phonenumber;
            $param_physicaladdress = $physicaladdress;
            $param_idnumber = $idnumber;
            $param_idcopy = $idcopy;
            $param_iralicense = $iralicense;
            $param_iracopy = $iracopy;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            $param_role = "default";
            #mysqli_stmt_execute($stmt); 
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                header("location: login.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Page Title - SB Admin</title>
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        <style>
            .help-block{
                color:red;
            }
        </style>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Create Account</h3></div>
                                    <div class="card-body">
                                    <p>Please fill this form to create an account.</p>
                                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group <?php echo (!empty($firstname_err)) ? 'has-error' : ''; ?>">
                                                        <label>First Name</label>
                                                        <input type="text" name="firstname" id="firstname" class="form-control" value="<?php echo $firstname; ?>"placeholder="Enter first name">
                                                        <span class="help-block"><?php echo $firstname_err; ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group <?php echo (!empty($lastname_err)) ? 'has-error' : ''; ?>">
                                                        <label>Last  Name</label>
                                                        <input type="text" name="lastname" class="form-control" value="<?php echo $lastname; ?>"placeholder="Enter last name">
                                                        <span class="help-block"><?php echo $lastname_err; ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                                                        <label>User Name</label>
                                                        <input type="text" name="username" class="form-control" value="<?php echo $username; ?>"placeholder="Enter User name">
                                                        <span class="help-block"><?php echo $username_err; ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group <?php echo (!empty($emailaddress_err)) ? 'has-error' : ''; ?>">
                                                        <label>Email Address</label>
                                                        <input type="text" name="emailaddress" class="form-control" value="<?php echo $emailaddress ; ?>"placeholder="Enter Email address">
                                                        <span class="help-block invalid-tooltip"><?php echo $emailaddress_err; ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group <?php echo (!empty($companyname_err)) ? 'has-error' : ''; ?>">
                                                        <label>Company Name</label>
                                                        <input type="text" name="companyname" class="form-control" value="<?php echo $companyname; ?>"placeholder="Enter company name">
                                                        <span class="help-block"><?php echo $companyname_err; ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group <?php echo (!empty($contactperson_err)) ? 'has-error' : ''; ?>">
                                                        <label>Contact Person</label>
                                                        <input type="text" name="contactperson" class="form-control" value="<?php echo $contactperson; ?>"placeholder="Enter contact person">
                                                        <span class="help-block"><?php echo $contactperson_err; ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group <?php echo (!empty($phonenumber_err)) ? 'has-error' : ''; ?>">
                                                        <label>Phone Number</label>
                                                        <input type="text" name="phonenumber" class="form-control" value="<?php echo $phonenumber; ?>"placeholder="Enter phone number">
                                                        <span class="help-block"><?php echo $phonenumber_err; ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group <?php echo (!empty($physicaladdress_err)) ? 'has-error' : ''; ?>">
                                                        <label>Residence</label>
                                                        <input type="text" name="physicaladdress" class="form-control" value="<?php echo $physicaladdress; ?>"placeholder="Enter place of residence">
                                                        <span class="help-block"><?php echo $physicaladdress_err; ?></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="small mb-1" for="information">Information</label>
                                                <input type="text" name="idnumber" class="form-control" value="<?php echo $idnumber; ?>"placeholder="Enter id number">
                                                <span class="help-block"><?php echo $idnumber_err; ?></span>
                                                
                                                <input class="form-control py-1" name="idcopy" id="idcopy" type="file" value="<?php echo $idcopy; ?>" placeholder="id copy" />
                                                <span class="help-block"><?php echo $idcopy_err; ?></span>

                                                <input type="text" name="krapin" class="form-control" value="<?php echo $krapin; ?>"placeholder="Enter kra pin">
                                                <span class="help-block"><?php echo $krapin_err; ?></span>
                                                
                                                <input class="form-control py-1" name="krapincopy" id="krapincopy" type="file" value="<?php echo $krapincopy; ?>" placeholder="kra copy" />
                                                <span class="help-block"><?php echo $krapincopy_err; ?></span>

                                                <input type="text" name="iralicense" class="form-control" value="<?php echo $iralicense; ?>"placeholder="Enter ira licence number">
                                                <span class="help-block"><?php echo $iralicense_err; ?></span>
                                                
                                                <input class="form-control py-1" name="iracopy" id="iracopy" type="file" value="<?php echo $iracopy; ?>" placeholder="ira copy" />
                                                <span class="help-block"><?php echo $iracopy_err; ?></span>

                                                
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                                                        <label class="small mb-1" for="password">Password</label>
                                                        <input class="form-control py-4" id="password" name="password" type="password" placeholder="Enter password" value="<?php echo $password; ?>" />
                                                        <span class="help-block"><?php echo $password_err; ?></span>

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                                                        <label class="small mb-1" for="confirm_password">Confirm Password</label>
                                                        <input class="form-control py-4 " id="confirm_password"  name="confirm_password"  type="password" placeholder="Confirm password" value="<?php echo $confirm_password; ?>" />
                                                        <span class="help-block"><?php echo $confirm_password_err; ?></span>
                                                    </div>
                                                </div>                                                                                               
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary btn-block" value="Submit">
                                                <input type="reset" class="btn btn-default btn-block" value="Reset">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="login.php">Have an account? Go to login</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
  
</body>
</html>