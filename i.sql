-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: jendie
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Agent`
--

DROP TABLE IF EXISTS `Agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Agent` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Contact_Person` varchar(255) DEFAULT NULL,
  `Company_Name` varchar(255) DEFAULT NULL,
  `REG_Cert` varchar(255) DEFAULT NULL,
  `KRA_Pin_number` varchar(255) DEFAULT NULL,
  `KRA_PIN_COPY` varchar(255) DEFAULT NULL,
  `Physical_Address` varchar(255) DEFAULT NULL,
  `Email_Address` varchar(255) DEFAULT NULL,
  `Phone_number` varchar(255) DEFAULT NULL,
  `Withholding_Tax` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uc_KRA_Pin_number` (`KRA_Pin_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Agent`
--

LOCK TABLES `Agent` WRITE;
/*!40000 ALTER TABLE `Agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `Agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Client`
--

DROP TABLE IF EXISTS `Client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Client` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Fname` varchar(255) DEFAULT NULL,
  `Lname` varchar(255) DEFAULT NULL,
  `Cert_for` varchar(255) DEFAULT NULL,
  `KRA_PIN` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `PHONE` varchar(255) DEFAULT NULL,
  `OCCUPATION` varchar(255) DEFAULT NULL,
  `RESIDENCE` varchar(255) DEFAULT NULL,
  `PROFILE_PHOTO` varchar(255) DEFAULT NULL,
  `VEHICLE_DETAILS` varchar(255) DEFAULT NULL,
  `MNAME` varchar(255) DEFAULT NULL,
  `CertFile` varchar(255) DEFAULT NULL,
  `KRAFILE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uc_KRA_PIN` (`KRA_PIN`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Client`
--

LOCK TABLES `Client` WRITE;
/*!40000 ALTER TABLE `Client` DISABLE KEYS */;
INSERT INTO `Client` VALUES (1,'Erick','Ngumbau','325422','5656565656','erick.soi@hotmail.com','0712962787','Programmer','Embakasi','clientfiles/15927934262bhic_nl (2).csv','clientfiles/15927934263data_spider.py','Soi','clientfiles/15927934260action_doc.odt','clientfiles/15927934261bhic_nl.csv');
/*!40000 ALTER TABLE `Client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContactUs`
--

DROP TABLE IF EXISTS `ContactUs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContactUs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContactUs`
--

LOCK TABLES `ContactUs` WRITE;
/*!40000 ALTER TABLE `ContactUs` DISABLE KEYS */;
INSERT INTO `ContactUs` VALUES (1,'+254 722 301 062');
/*!40000 ALTER TABLE `ContactUs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Coverage`
--

DROP TABLE IF EXISTS `Coverage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Coverage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `cover` varchar(255) NOT NULL,
  `Value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Coverage`
--

LOCK TABLES `Coverage` WRITE;
/*!40000 ALTER TABLE `Coverage` DISABLE KEYS */;
INSERT INTO `Coverage` VALUES (1,'Third Party Only','third'),(2,'Third Party And Theft','thirdAndT'),(3,'Comprehensive','comprehensive');
/*!40000 ALTER TABLE `Coverage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Persons`
--

DROP TABLE IF EXISTS `Persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Persons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `recog` varchar(255) NOT NULL,
  `kra` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Persons`
--

LOCK TABLES `Persons` WRITE;
/*!40000 ALTER TABLE `Persons` DISABLE KEYS */;
/*!40000 ALTER TABLE `Persons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_NAME` varchar(255) DEFAULT NULL,
  `RISK_COVERED` varchar(255) DEFAULT NULL,
  `UNDERWRITER` varchar(255) DEFAULT NULL,
  `COVERAGE` varchar(255) DEFAULT NULL,
  `SHORTTERM_RATES` varchar(255) DEFAULT NULL,
  `ANNUAL_RATES` varchar(255) DEFAULT NULL,
  `CLAUSES` varchar(255) DEFAULT NULL,
  `CONDITIONS_AND_WARRANTIES` varchar(255) DEFAULT NULL,
  `OPTIONAL_ADDITIONAL_BENEFITS` varchar(255) DEFAULT NULL,
  `OP_BMP` varchar(255) DEFAULT NULL,
  `OP_BR` varchar(255) DEFAULT NULL,
  `VEHICLE_TYPE` varchar(255) DEFAULT NULL,
  `PRODUCT_LOGO` varchar(255) DEFAULT NULL,
  `PRODUCT_IDENTIFIER` varchar(255) DEFAULT NULL,
  `YEAR_ACCEPTED` varchar(255) DEFAULT NULL,
  `UNIQUEID` varchar(255) DEFAULT NULL,
  `TONNAGE` varchar(255) DEFAULT NULL,
  `MINIMUM_PREMIUM` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUEID` (`UNIQUEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product`
--

LOCK TABLES `Product` WRITE;
/*!40000 ALTER TABLE `Product` DISABLE KEYS */;
/*!40000 ALTER TABLE `Product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductList`
--

DROP TABLE IF EXISTS `ProductList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductList` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductList`
--

LOCK TABLES `ProductList` WRITE;
/*!40000 ALTER TABLE `ProductList` DISABLE KEYS */;
INSERT INTO `ProductList` VALUES (1,'Bimaplus(MOTORINSURANCE)');
/*!40000 ALTER TABLE `ProductList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product_additional_benefits`
--

DROP TABLE IF EXISTS `Product_additional_benefits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product_additional_benefits` (
  `PRODUCT_IDENTIFIER` varchar(255) DEFAULT NULL,
  `OPTIONAL_ADDITIONAL_BENEFITS` varchar(255) DEFAULT NULL,
  `OP_BMP` varchar(255) DEFAULT NULL,
  `OP_BR` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product_additional_benefits`
--

LOCK TABLES `Product_additional_benefits` WRITE;
/*!40000 ALTER TABLE `Product_additional_benefits` DISABLE KEYS */;
INSERT INTO `Product_additional_benefits` VALUES ('35R8CXLvCg','Sdnsjdbhsjd','500','5'),('35R8CXLvCg','hgfdfds','400','5'),('xuKOY3wpWH','Hhshshsh','1','1'),('rVZnmEdV9u','Ewewewe','3','2'),('2XxvCVxBW2','1','0','-3'),('I7SCnUYzYV','1','2','2'),('9CmWYlacrZ','2','2','2'),('yx5MS8lllJ','2','2','2'),('CukdvAANEI','2','2','2'),('CukdvAANEI','1','1','1'),('wTkT9BP3m0','1','3','0'),('DKubJVOKTQ','1','1','1'),('iZAUTiutEU','1','1','1'),('0d3pLkfgQr','1','1','1'),('8IToZGbOP6','1','1','-4'),('8IToZGbOP6','hgfdfds','12','211'),('8IToZGbOP6','ttt','222','222'),('TIijyAVJUU','Rrrrrrrrrr','1','1'),('TIijyAVJUU','fffff','1111','11111'),('2v4NqIwosz','2','2','-1'),('xc9IljaqDi','2','2','2'),('sTuecWWRsH','1','1','-2'),('TWp4P1hzu9','1','1','-2'),('npyca2tcqm','1','1','-2'),('Puo0SdO4p6','1','1','-2'),('28hk71Ng2T','1','1','-2'),('1sfEg3zBoR','1','1','-2'),('zA1h4QY8Bk','1','1','-2'),('YZFuwD6RAK','1','1','-2'),('h2xI1CM0FA','1','1','-2'),('d3wNVn32NE','1','1','-2'),('vi4Eilk1u0','1','1','-2'),('y7GeiPboPt','1','1','-2'),('FAAF8xo0wr','1','1','1'),('iwbldFgLXS','1','1','1'),('RDNEwe1ohV','1','1','1'),('b6Cv5CA1TV','1','1','1'),('AMRrNZzwCR','1','1','1'),('do9BEuxSn0','1','1','1'),('VX9aoOGPx3','2','2','2'),('FP3QwTulkD','2','2','2'),('dIYk6DeGk1','2','2','2'),('kvdVHAbHlz','2','2','2'),('AepiuRUKhX','2','2','2'),('S3CRC94B2b','2','2','2'),('llVBoM91bI','2','2','2'),('lpMCyHRXXO','2','2','2'),('I4rISVbfkD','11','2','2'),('kRhYwSU2VV','2','2','2'),('jiRIGU0sxv','2','2','1'),('We9JNPZRRW','3','3','3'),('Ad3v4CzJJk','22','22','22'),('DzFV4krpBV','3','3','3'),('490uAFye5C','22','22','22');
/*!40000 ALTER TABLE `Product_additional_benefits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Underwriter`
--

DROP TABLE IF EXISTS `Underwriter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Underwriter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `LEGAL_ENTITY` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `WEBSITE` varchar(255) DEFAULT NULL,
  `LOGO` varchar(255) DEFAULT NULL,
  `LOCATION_OPTIONAL` varchar(255) DEFAULT NULL,
  `BANK_ACCOUNT_OPTIONAL` varchar(255) DEFAULT NULL,
  `UNIQUE_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uc_name` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Underwriter`
--

LOCK TABLES `Underwriter` WRITE;
/*!40000 ALTER TABLE `Underwriter` DISABLE KEYS */;
/*!40000 ALTER TABLE `Underwriter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UnderwriterList`
--

DROP TABLE IF EXISTS `UnderwriterList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UnderwriterList` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UnderwriterList`
--

LOCK TABLES `UnderwriterList` WRITE;
/*!40000 ALTER TABLE `UnderwriterList` DISABLE KEYS */;
INSERT INTO `UnderwriterList` VALUES (1,'AAR Insurance Company Limited'),(2,'Africa Merchant Assurance Company Limited'),(3,'AIG Kenya Insurance Company Limited'),(4,'Allianz Insurance Company of Kenya Limited'),(5,'APA Insurance Limited'),(6,'APA Life Assurance Company Limited'),(7,'Barclays Life Assurance Kenya Limited'),(8,'Britam General Insurance Company (K) Limited'),(9,'Britam Life Assurance Company (K) Limited'),(10,'Metropolitan Cannon General Insurance Company Limited'),(11,'Capex Life Assurance Company Limited'),(12,'CIC General Insurance Company Limited'),(13,'CIC Life Assurance Company Limited'),(14,'Corporate Insurance Company Limited'),(15,'Directline Assurance Company Limited'),(16,'Fidelity Shield Insurance Company Limited'),(17,'First Assurance Company Limited'),(18,'GA Insurance Limited'),(19,'GA Life Assurance Limited'),(20,'Geminia Insurance Company Limited'),(21,'ICEA LION General Insurance Company Limited'),(22,'ICEA LION Life Assurance Company Limited'),(23,'Intra Africa Assurance Company Limited'),(24,'Invesco Assurance Company Limited'),(25,'Jubilee General Insurance Limited'),(26,'Jubilee Health Insurance Limited'),(27,'Kenindia Assurance Company Limited'),(28,'Kenya Orient Insurance Limited'),(29,'Kenya Orient Life Assurance Limited'),(30,'KUSCCO Mutual Assurance Limited'),(31,'Liberty Life Assurance Kenya Limited'),(32,'Madison Insurance Company Kenya Limited'),(33,'Madison General Insurance Kenya Limited'),(34,'Mayfair Insurance Company Limited'),(35,'Metropolitan Cannon Life Assurance Limited'),(36,'Occidental Insurance Company Limited'),(37,'Old Mutual Assurance Company Limited'),(38,'Pacis Insurance Company Limited'),(39,'MUA Insurance ( Kenya) Limited 01'),(40,'Pioneer General Insurance Company Limited'),(41,'Pioneer Assurance Company Limited'),(42,'Prudential Life Assurance Company Limited'),(43,'Resolution Insurance Company Limited'),(44,'Saham Assurance Company Kenya Limited'),(45,'Sanlam General Insurance Company Limited'),(46,'Sanlam Life Insurance Company Limited'),(47,'Takaful Insurance of Africa Limited'),(48,'Tausi Assurance Company Limited'),(49,'The Heritage Insurance Company Limited'),(50,'The Jubilee Insurance Company of Kenya Limited'),(51,'The Kenyan Alliance Insurance Company Limited'),(52,'The Monarch Insurance Company Limited'),(53,'Trident Insurance Company Limited'),(54,'UAP Insurance Company Limited'),(55,'UAP Life Assurance Limited'),(56,'Xplico Insurance Company Limited');
/*!40000 ALTER TABLE `UnderwriterList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Fname` varchar(255) DEFAULT NULL,
  `Lname` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `ID_no` varchar(255) DEFAULT NULL,
  `KRA` varchar(255) DEFAULT NULL,
  `Bank_No` varchar(255) DEFAULT NULL,
  `NSSF` varchar(255) DEFAULT NULL,
  `NHIF` varchar(255) DEFAULT NULL,
  `Signature` varchar(255) DEFAULT NULL,
  `ACCESS` varchar(255) DEFAULT NULL,
  `IDIMG` varchar(255) DEFAULT NULL,
  `KRADOC` varchar(255) DEFAULT NULL,
  `BANKIMG` varchar(255) DEFAULT NULL,
  `NSSFDOC` varchar(255) DEFAULT NULL,
  `NHIFDOC` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uc_ID_no` (`ID_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ValuationCenters`
--

DROP TABLE IF EXISTS `ValuationCenters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ValuationCenters` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Town` varchar(255) DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `Tel` varchar(255) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `ContactPerson` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ValuationCenters`
--

LOCK TABLES `ValuationCenters` WRITE;
/*!40000 ALTER TABLE `ValuationCenters` DISABLE KEYS */;
INSERT INTO `ValuationCenters` VALUES (1,'NAIROBI','2nd floor, rm 2B Muthithi Rd,Westlands','0202632578','info@regentautovaluers.co.ke','Imelda'),(2,'NAIROBI','Luther Plaza, University Way Roundabout, Nyerere Rd P.O Box 34365 – 00100 Nairobi, Kenya','0202603906,0722608210,0700394000,0700393777','lutheran@regentautovaluers.co.ke','Mr. Festus Kaleli'),(3,'NAIROBI','Muthaiga Square, 3rd Floor','0612303476,0728292912','pangani@regentautovaluers.co.ke','Mr. Alex'),(4,'NAIROBI','Westlands, Muthithi Rd, Opposite avocado Towers','0202603916,0711599665','westlands@regentautovaluers.co.ke','Mr. Robert Mwangi.'),(5,'ELDORET','WATERGATE PLAZA','0202134075,0700077009','eldoret@regentautovaluers.co.ke','Mr. Khaemba Andrew'),(6,'KISUMU','AWORI HOUSE','0202134077,0724228228','kisumu@regentautovaluers.co.ke','Mr. Enock Ochieng.'),(7,'MALINDI','Royal Complex Rm no G15,','0712840088','regentmalindi@gmail.com','Mr. Mwangi'),(8,'NAROK','NENKAI PLAZA','07184333330779338531,','narok@regentautovaluers.co.ke','Mr. Muli'),(9,'NAIROBI','Upperhill, Matumbato Rd','0202605723,0704879000','upperhill@regentautovaluers.co.ke','Mr. Munene'),(10,'NAITOBI','Utawala, Grey Park Heights, 1st Flr, Room A17','0770626463,0790884101','utawala@regentautovaluers.co.ke','Mr. Tabale'),(11,'NAIROBI','Buruburu Complex – Nairobi, Room B3','0202603915,0702501050','buruburu@regentautovaluers.co.ke','Mr. Elias Mwangi.'),(12,'NGONG','Along Ngong Road (from town) 50 Meters from China Center, At Jameson Court Block B Office No. 10','0713092158,0772733808','ngongroad@regentautovaluers.co.ke','Mr. Maina'),(13,'KITALE','AMBWERE PLAZA','0702512727','regentbungoma@gmail.com,kitale@regentautovaluers.co.ke','Mr. Kepha.'),(14,'MACHAKOS','PEMA HOUSE','0771569035,0728969509,0719723650,0720274150','machakos@regentautovaluers.co.ke','Mr. Muli'),(15,'THIKA','EQUITY PLAZA','0202693664,0712636368,0705198131','thika@regentautovaluers.co.ke','Mr. Nelson Kiirinya'),(16,'MERU','TWIN PLAZA','0202603909,0718094777','meru@regentautovaluers.co.ke','Mr. Njeru'),(17,'NAKURU','MASTERS PLAZA 4TH FLOOR :WING 2 :ROOM 1','0202134076,0727707017','nakuru@regentautovaluers.co.ke','Mr. Lawrence Macharia'),(18,'MOMBASA','YUNIS BUILDING 3RD FLOOR ROOM NO.4','0412220430,0727707016','mombasa@regentautovaluers.co.ke','Mr. Waweru'),(19,'NYERI','GATHII HSE','0612034027,0723697400','nyeri@regentautovaluers.co.ke','Mr. Mukundi.'),(20,'EMBU','NEEMA PLAZA','0202603918,0712839944','embu@regentautovaluers.co.ke','Mr. Mukangu'),(21,'KISII','UPENDO PLAZA','0715710257,0701714652','kisii@regentautovaluers.co.ke','Mr. Caleb Oyugi'),(22,'RUIRU','SANFRED HOUSE, SECOND FLOOR, RM 205','0772684059,0708156785','ruiru@regentautovaluers.co.ke','Mr. Kuko'),(23,'KITUI','KCB BUILDING 2ND FLOOR','0711599775','regentkitui@gmail.com','Mr. Sylvester Mulei'),(24,'KERUGOYA','','0704262478','kerugoya@regentautovaluers.co.ke','Mr. Munene'),(25,'NANYUKI','Nanyuki rd, Chadwick house ground floor','0704262613','nanyuki@regentautovaluers.co.ke','Mr. Murugami'),(26,'NAIVASHA','DOVE COMPLEX 1ST FLOOR ROOM 17, KARIUKI CHOTARA ROAD','0704262501,0772733809','naivasha@regentautovaluers.co.ke','Mr. Mwangi'),(27,'NAIROBI','Tymes Arcade 2nd Flr Rm 212, ONGATA RONGAI','254774086657','rongai@regentautovaluers.co.ke','Mr. Thiba.'),(28,'KAJIADO','Betty Plaza, 3rd floor, rm 108 ?, Kitengela','0772684060,0700158152','kitengela@regentautovaluers.co.ke','Mr. Adan Galgalo'),(29,'NYALI','','0700158124,0772762883','nyali@regentautovaluers.co.ke','Mr. Murangiri'),(30,'KAKAMEGA','Emmisioma Building 2nd Flr behind Post Bank, Along Cannon Awori Street','254790122193','kakamega@regentautovaluers.co.ke','Mr. Dennis Buyema.'),(31,'NYAHURURU','','0724276242','regentnyahururu@gmail.com','Mr. Nahashon Kariuki'),(32,'KERICHO','JEMI PLAZA ROOM NO.BA9, Next to Bank of Africa Opposite Garden Hotel','0712840202,0772762874','kericho@regentautovaluers.co.ke','Mr. Zablon'),(33,'MOMBASA ROAD','Nextgen Mall','0799396534,0778005571','nextgen@regentautovaluers.co.ke','Mr. Stanley Kioko.'),(34,'KIAMBU','Quick Matt, Komrades Business Center Room 7.','0775550802,0716601759','kiambu@regentautovaluers.co.ke','Mr. Maina'),(35,'KIKUYU','Ivory Towers, Room B4','0793276932,0771099216','kikuyu@regentautovaluers.co.ke','Mr. Daniel Kinyua');
/*!40000 ALTER TABLE `ValuationCenters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VehicleClass`
--

DROP TABLE IF EXISTS `VehicleClass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VehicleClass` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VehicleClass`
--

LOCK TABLES `VehicleClass` WRITE;
/*!40000 ALTER TABLE `VehicleClass` DISABLE KEYS */;
INSERT INTO `VehicleClass` VALUES (1,'MOTORCYCLE','Private'),(2,'MOTORCYCLE','PSV (bodaboda)'),(3,'TRICYCLE','Comercial Own goods'),(4,'TRICYCLE','PSV (tuktuk)'),(5,'MOTORVEHICLE','Private'),(6,'MOTORVEHICLE','Comercial Own goods'),(7,'MOTORVEHICLE','General Cartage Lorries,Trucks and Tankers'),(8,'MOTORVEHICLE','Agricultural and Forestry vehicles'),(9,'MOTORVEHICLE','Chauffeur driven'),(10,'MOTORVEHICLE','Motor trade'),(11,'MOTORVEHICLE','Institutional Vehicles'),(12,'MOTORVEHICLE','Driving school Vehicle'),(13,'MOTORVEHICLE','Tour Service Vehicles'),(14,'MOTORVEHICLE','PSV - Matatu'),(15,'MOTORVEHICLE','PSV - Taxi'),(16,'MOTORVEHICLE','Ambulance and fire fighters'),(17,'MOTORVEHICLE','Forklift,Crane, Rollers and Excavators');
/*!40000 ALTER TABLE `VehicleClass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurance`
--

DROP TABLE IF EXISTS `insurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plate` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `delivery` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurance`
--

LOCK TABLES `insurance` WRITE;
/*!40000 ALTER TABLE `insurance` DISABLE KEYS */;
/*!40000 ALTER TABLE `insurance` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-21  7:19:18