<!DOCTYPE html>
<?php
  require 'handle/db.php';
?>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Page Title - SB Admin</title>
        <link href="dashboard/dist/css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Request This Product</h3></div>
                                    <div class="card-body">
                                        <form>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <input class="form-control py-1" id="inputFirstName" type="text" placeholder="Your Name" />
                                                        </div>
                                                        <div class="form-group">
                                                            <input class="form-control py-1" id="inputFirstName" type="text" placeholder="Email" />
                                                        </div>
                                                        <div class="form-group">                                                    
                                                            <input class="form-control py-1" id="inputFirstName" type="text" placeholder="Phone no" />
                                                        </div>
                                                        <div class="form-group"> 
                                                        <label id="emailHelp" class="text-white">Choose vehicle class</label>                                               
                                                            <input class="form-control py-1" id="inputFirstName" type="text" placeholder="Enter Vehicle Make" />
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input class="form-control py-1" id="inputFirstName" type="number" placeholder="Sum Insured" />
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="yom" class="form-control py-1"> <<?php for ($i = date('Y'); $i >= 1900; $i--){echo "<option>$i</option>"; }?></select>
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="coverperiod" class="form-control py-1"><?php for ($i = 1; $i <= 6; $i++){
                                                            if($i == 1){
                                                                echo "<option>6 Months to 1 year</option>"; 
                                                            } elseif($i == 2){
                                                                echo "<option>5 Months</option>"; 
                                                            }elseif($i == 3){
                                                                echo "<option>4 Months</option>"; 
                                                            }elseif($i == 4){
                                                                echo "<option>3 Months</option>"; 
                                                            }elseif($i == 5){
                                                                echo "<option>2 Months</option>"; 
                                                            }elseif($i == 6){
                                                                echo "<option>1 Month</option>"; 
                                                            }
                                                            }?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group"> 
                                                        <label id="emailHelp" class="form-text text-muted">Choose vehicle class</label>
                                                        <select  name="vehicleClass" class="form-control py-1" id="risk"  onchange="myFunction()">
                                                            <?php
                                                                    $sql1 = 'select * from VehicleClass where type = "MOTORCYCLE"';
                                                                    $result = mysqli_query($conn, $sql1);
                                                                    
                                                                    if(mysqli_num_rows($result) > 0){
                                                                        echo '<optgroup label="1. MOTORCYCLE">';
                                                                        while($row = mysqli_fetch_array($result)){
                                                                            echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                        }
                                                                        echo "</optgroup>";
                                                                    }
                                                                    $sql2 = 'select * from VehicleClass where type = "TRICYCLE"';
                                                                    $result = mysqli_query($conn, $sql2);
                                                                    
                                                                    if(mysqli_num_rows($result) > 0){
                                                                        echo '<optgroup label="2. TRICYCLE">';
                                                                        while($row = mysqli_fetch_array($result)){
                                                                            echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                        }
                                                                        echo "</optgroup>";
                                                                    }
                                                                    $sql3 = 'select * from VehicleClass where type = "MOTORVEHICLE"';
                                                                    $result = mysqli_query($conn, $sql3);
                                                                    if(mysqli_num_rows($result) > 0){
                                                                        echo '<optgroup label="3. MOTORVEHICLE">';
                                                                        while($row = mysqli_fetch_array($result)){
                                                                            echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                        }
                                                                        echo "</optgroup>";
                                                                    }
                                                                        
                                                                ?>                                                                        
                                                        </select>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputEmailAddress">Enter Vehicle Registration</label>
                                                <input class="form-control py-1" id="inputEmailAddress" type="email" aria-describedby="emailHelp" placeholder="Vehicle Registration" />
                                            </div>
                                            <div class="form-group mt-4 mb-0"><a class="btn btn-primary btn-block" href="dashboard/dist/productrequest.html">Request Product</a></div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="../../agentjourney/underwriters.php">Cancel</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-1 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
