<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Bima + Plus</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

	<!-- CSS here -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/slicknav.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/progressbar_barfiller.css">
    <link rel="stylesheet" href="assets/css/gijgo.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/animated-headline.css">
	<link rel="stylesheet" href="assets/css/magnific-popup.css">
	<link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="assets/css/themify-icons.css">
	<link rel="stylesheet" href="assets/css/slick.css">
	<link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    
    
</head>
<?php
    include "handle/db.php";
?>
<body>
    <!-- ? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="assets/img/logo/loder.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header ">
                
                <div class="header-bottom  header-sticky">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-2 col-lg-2">
                                <div class="logo">
                                    <a href="index.php"><img src="assets/img/logo/logo.png" alt=""></a>
                                </div>
                            </div>
                            <div class="col-xl-10 col-lg-10">
                                <div class="menu-wrapper  d-flex align-items-center justify-content-end">
                                    <!-- Main-menu -->
                                    <div class="main-menu d-none d-lg-block">
                                        <nav>
                                            <ul id="navigation">                                                                                          
                                                <li><a href="index.php">Home</a></li>
                                                <li><a href="index.php">About</a></li>
                                                <li><a href="index.php">Blog</a></li>
                                                <li><a href="index.php">Contact</a></li>
                                                <li><a href="dashboard/dist/login.php">Login</a></li>
                                                <li><a href="dashboard/dist/login.php">Admin Login</a></li>
                                                <button type="button" class="btn btn-success"><a href="dashboard/dist/register.php">Register As an Agent </a></button>
                                     
                                            </ul>
                                        </nav>
                                    </div>
                                
                                </div>
                            </div> 
                            <!-- Mobile Menu -->
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>
    <!-- header end -->
    <main>
    <!-- slider Area Start-->
    <div class="slider-area">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-8 col-sm-10">
                            <div class="hero__caption">
                                <h1 data-animation="fadeInUp" data-delay=".6s">Smart  <br> Insurance.</h1>
                                <P data-animation="fadeInUp" data-delay=".8s" >Changing how insurance works</P>
                                <!-- Hero-btn -->
                                <div class="hero__btn">
                                    <div class="btn hero-btn mb-10" data-animation="fadeInLeft" data-delay=".8s" data-toggle="modal" data-target="#staticBackdrop">Get Quote</div>
                                    <div class="cal-btn ml-15" data-animation="fadeInRight" data-delay="1.0s">
                                        <i class="flaticon-null"></i>
                                        <p>+254 733 566 464</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Slider -->
            <div class="single-slider slider-height d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-8 col-sm-10">
                            <div class="hero__caption">
                                <h1 data-animation="fadeInUp" data-delay=".6s">Smart  <br> Insurance.</h1>
                                <P data-animation="fadeInUp" data-delay=".8s" >Changing how insurance works</P>
                                <!-- Hero-btn -->
                                <div class="hero__btn">
                                    <div class="btn hero-btn mb-10"  data-animation="fadeInLeft" data-delay=".8s" data-toggle="modal" data-target="#staticBackdrop">Get Quote</div>
                                    <div class="cal-btn ml-15" data-animation="fadeInRight" data-delay="1.0s">
                                        <i class="flaticon-null"></i>
                                        <p>+254 733 566 464</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Generte your quote</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
           <form id="get-quote" action="dist/quote.php" method="POST">
                <div class="form-group">
                    <input type="text" name="name" placeholder="Your Name" class="form-control form-control-lg" required/>
                </div>
                <div class="form-group">
                    <input type="text" name="email" placeholder="Email" class="form-control form-control-lg" required/>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <input type="text" name="ph-no" placeholder="Phone no" class="form-control form-control-lg" required/>
                </div>  
                 <small id="emailHelp" class="form-text text-muted">Choose vehicle class</small>
                <select  name="vehicleClass" class="form-s form-control-lg" id="risk"  onchange="myFunction()">
                      
                        <?php
                            $sql1 = 'select * from VehicleClass where type = "MOTORCYCLE"';
                            $result = mysqli_query($conn, $sql1);
                            
                            if(mysqli_num_rows($result) > 0){
                                //print_r($result);
                                echo "llll";
                                echo '<optgroup label="1. MOTORCYCLE">';
                                while($row = mysqli_fetch_array($result)){
                                    echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                }
                                echo "</optgroup>";
                            }
                            $sql2 = 'select * from VehicleClass where type = "TRICYCLE"';
                            $result = mysqli_query($conn, $sql2);
                            
                            if(mysqli_num_rows($result) > 0){
                                echo '<optgroup label="2. TRICYCLE">';
                                while($row = mysqli_fetch_array($result)){
                                    echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                }
                                echo "</optgroup>";
                            }
                            $sql3 = 'select * from VehicleClass where type = "MOTORVEHICLE"';
                            $result = mysqli_query($conn, $sql3);
                            if(mysqli_num_rows($result) > 0){
                                echo '<optgroup label="3. MOTORVEHICLE">';
                                while($row = mysqli_fetch_array($result)){
                                    echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                }
                                echo "</optgroup>";
                            }
                                
                        ?>                   
                </select>
                <div><br><br>
                <small id="emailHelp" class="form-text text-muted"> Choose limit of cover</small> 
                <select id="mySelect1" class="form-control form-control-sm"  onchange="myFunction()" name="cover">
                <?php
                            $sql4 = 'select cover from Coverage';
                            $result = mysqli_query($conn, $sql4);
                            if(mysqli_num_rows($result) > 0){
                                
                                while($row = mysqli_fetch_array($result)){
                                    echo '<option>'.$row["cover"].'</option>';
                                }
                                echo $row["value"];
                            }
                                
                        ?>
                    
                        
                    </select>
                </div>
                
                <div id="suminsured"></div>
                <div id="suminsured1"></div>
                <div id="suminsured2"></div>
                <div id="suminsured3"></div>
                <div id="coverperiod1"></div>
                <div id="coverperiod"></div>

                <div id="vehiclemake"></div>
                <div id="tonnage"></div>
                <div id="tonnage1"></div>            
                
                <script>
                    function myFunction() {
                        var x = document.getElementById("mySelect1");
                        var i = x.selectedIndex;
                        console.log(i);
                        if (i == 2 || i == 1) {
                            
                            document.getElementById("suminsured").innerHTML ='<small id="emailHelp1" class="form-text text-muted">Enter Amount Insured</small>';

                            document.getElementById("suminsured1").innerHTML = '<input class="form-control form-control-lg" name="sum_insured" type="number" placeholder="Sum Insured" required>';
                            document.getElementById("suminsured2").innerHTML = '<small id="emailHelp" class="form-text text-muted">Enter Year of Manufacture</small>';
                            document.getElementById("suminsured3").innerHTML = '<select name="yom" class="form-control form-control-lg"> <<?php for ($i = date('Y'); $i >= 1900; $i--){echo "<option>$i</option>"; }?></select><br>'
                            document.getElementById("vehiclemake").innerHTML = '<input type="text" name="vehicleMake" placeholder="Enter Vehicle Make" class="form-control form-control-lg" id="quotemake" required/>';
                            document.getElementById("coverperiod1").innerHTML = '<small id="emailHelp" class="form-text text-muted">Choose Cover Period</small>';

                            document.getElementById("coverperiod").innerHTML = '<select name="coverperiod" class="form-control form-control-lg"> <<?php for ($i = 1; $i <= 6; $i++){
                                if($i == 1){
                                    echo "<option>6 Months to 1 year</option>"; 
                                } elseif($i == 2){
                                    echo "<option>5 Months</option>"; 
                                }elseif($i == 3){
                                    echo "<option>4 Months</option>"; 
                                }elseif($i == 4){
                                    echo "<option>3 Months</option>"; 
                                }elseif($i == 5){
                                    echo "<option>2 Months</option>"; 
                                }elseif($i == 6){
                                    echo "<option>1 Month</option>"; 
                                }
                                }?></select><br>'


                        }
                        
                        if (i == 0) {
                            document.getElementById("suminsured").innerHTML = '';
                            document.getElementById("suminsured1").innerHTML = '';
                            document.getElementById("suminsured2").innerHTML = '';
                            document.getElementById("suminsured3").innerHTML = '';
                            document.getElementById("vehiclemake").innerHTML = '';
                            document.getElementById("coverperiod").innerHTML = '';                            
                        }

                        if (i == 0 || i == 2){
                            document.getElementById("tonnage").innerHTML = '';
                            document.getElementById("tonnage1").innerHTML = '';
                        }
                        
                        var z = document.getElementById("risk");
                        var y = z.selectedIndex;
                        //console.log(y);
                        if ((y == 5 || y == 6 || y == 2)&&(i == 1)) {
                                document.getElementById("tonnage").innerHTML ='<label for="profile" class="control-label">Enter Tonnage</label>';

                                document.getElementById("tonnage1").innerHTML = '<input class="form-control form-control-lg" name="tonnage" type="number" placeholder="Tonnage" required><br>';
                        }else{
                                document.getElementById("tonnage").innerHTML =" ";
                                document.getElementById("tonnage1").innerHTML = " ";
                        }

                    }

                </script>
                    <input type="text" name="vehicle_reg" placeholder="Vehicle registration eg KMCE 000K" class="form-control form-control-lg"  id="emailHelp2" required/>
                </div>
                <div class="text-center">
                    <input type="submit" class="btn-default" value="Submit" />
                </div>
            </form>
        </div>

        </div>
    </div>
    </div> 
    <!-- slider Area End-->
    <!--? Services Area Start -->
<!--
    <div class="service-area section-padding30">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-7 col-md-10 col-sm-10">
                   
                    <div class="section-tittle text-center mb-80">
                        <span>What we are doing</span>
                        
                        <h2>We Are In A Mission To Help The Helpless</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-cat text-center mb-50">
                        <div class="cat-icon">
                            <span class="flaticon-null-1"></span>
                        </div>
                        <div class="cat-cap">
                            <h5><a href="services.html">Clean Water</a></h5>
                            <p>The sea freight service has grown conside rably in recent years. We spend timetting to know your processes to.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-cat active text-center mb-50">
                        <div class="cat-icon">
                            <span class="flaticon-think"></span>
                        </div>
                        <div class="cat-cap">
                            <h5><a href="services.html">Clean Water</a></h5>
                            <p>The sea freight service has grown conside rably in recent years. We spend timetting to know your processes to.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-cat text-center mb-50">
                        <div class="cat-icon">
                            <span class="flaticon-gear"></span>
                        </div>
                        <div class="cat-cap">
                            <h5><a href="services.html">Clean Water</a></h5>
                            <p>The sea freight service has grown conside rably in recent years. We spend timetting to know your processes to.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <section class="about-low-area section-padding2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-10">
                    <div class="about-caption mb-50">
                      
                        <div class="section-tittle mb-35">
                            <span>About our foundetion</span>
                            <h2>We Are In A Mission To  Help Helpless</h2>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,mod tempor incididunt ut labore et dolore magna aliqua. Utnixm, quis nostrud exercitation ullamc.</p>
                        <p>Lorem ipvsum dolor sit amext, consectetur adipisicing elit, smod tempor incididunt ut labore et dolore.</p>
                    </div>
                    <a href="about.html" class="btn">About US</a>
                </div>
                <div class="col-lg-6 col-md-12">
                   
                    <div class="about-img ">
                        <div class="about-font-img d-none d-lg-block">
                            <img src="assets/img/gallery/about2.png" alt="">
                        </div>
                        <div class="about-back-img ">
                            <img src="assets/img/gallery/about1.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <div class="our-cases-area section-padding30">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-7 col-md-10 col-sm-10">
                   
                    <div class="section-tittle text-center mb-80">
                        <span>Our Cases you can see</span>
                        <h2>Explore our latest causes that we works </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-cases mb-40">
                        <div class="cases-img">
                            <img src="assets/img/gallery/case1.png" alt="">
                        </div>
                        <div class="cases-caption">
                            <h3><a href="#">Ensure Education For Every Poor Children</a></h3>
                           
                            <div class="single-skill mb-15">
                                <div class="bar-progress">
                                    <div id="bar1" class="barfiller">
                                        <div class="tipWrap">
                                            <span class="tip"></span>
                                        </div>
                                        <span class="fill" data-percentage="70"></span>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="prices d-flex justify-content-between">
                                <p>Raised:<span> $20,000</span></p>
                                <p>Goal:<span> $35,000</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-cases mb-40">
                        <div class="cases-img">
                            <img src="assets/img/gallery/case2.png" alt="">
                        </div>
                        <div class="cases-caption">
                            <h3><a href="#">Providing Healthy Food For The Children</a></h3>
                           
                            <div class="single-skill mb-15">
                                <div class="bar-progress">
                                    <div id="bar2" class="barfiller">
                                        <div class="tipWrap">
                                            <span class="tip"></span>
                                        </div>
                                        <span class="fill" data-percentage="25"></span>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="prices d-flex justify-content-between">
                                <p>Raised:<span> $20,000</span></p>
                                <p>Goal:<span> $35,000</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-cases mb-40">
                        <div class="cases-img">
                            <img src="assets/img/gallery/case3.png" alt="">
                        </div>
                        <div class="cases-caption">
                            <h3><a href="#">Supply Drinking Water For  The People</a></h3>
                           
                            <div class="single-skill mb-15">
                                <div class="bar-progress">
                                    <div id="bar3" class="barfiller">
                                        <div class="tipWrap">
                                            <span class="tip"></span>
                                        </div>
                                        <span class="fill" data-percentage="50"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="prices d-flex justify-content-between">
                                <p>Raised:<span> $20,000</span></p>
                                <p>Goal:<span> $35,000</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
    <section class="featured-job-area section-padding30 section-bg2" data-background="assets/img/gallery/section_bg03.png">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-9 col-md-10 col-sm-12">
                   
                    <div class="section-tittle text-center mb-80">
                        <span>What we are boing</span>
                        <h2>We arrange many social events for charity donations</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-9 col-md-12">
                  
                    <div class="single-job-items mb-30">
                        <div class="job-items">
                            <div class="company-img">
                                <a href="#"><img src="assets/img/gallery/socialEvents1.png" alt=""></a>
                            </div>
                            <div class="job-tittle">
                                <a href="#"><h4>Donation is Hope</h4></a>
                                <ul>
                                    <li><i class="far fa-clock"></i>8:30 - 9:30am</li>
                                    <li><i class="fas fa-sort-amount-down"></i>18.01.2021</li>
                                    <li><i class="fas fa-map-marker-alt"></i>Athens, Greece</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12">
                   
                    <div class="single-job-items mb-30">
                        <div class="job-items">
                            <div class="company-img">
                                <a href="#"><img src="assets/img/gallery/socialEvents2.png" alt=""></a>
                            </div>
                            <div class="job-tittle">
                                <a href="#"><h4>A hand for Children</h4></a>
                                <ul>
                                    <li><i class="far fa-clock"></i>8:30 - 9:30am</li>
                                    <li><i class="fas fa-sort-amount-down"></i>18.01.2021</li>
                                    <li><i class="fas fa-map-marker-alt"></i>Athens, Greece</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12">
                   
                    <div class="single-job-items mb-30">
                        <div class="job-items">
                            <div class="company-img">
                                <a href="#"><img src="assets/img/gallery/socialEvents3.png" alt=""></a>
                            </div>
                            <div class="job-tittle">
                                <a href="#"><h4>Help for Children</h4></a>
                                <ul>
                                    <li><i class="far fa-clock"></i>8:30 - 9:30am</li>
                                    <li><i class="fas fa-sort-amount-down"></i>18.01.2021</li>
                                    <li><i class="fas fa-map-marker-alt"></i>Athens, Greece</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <div class="team-area pt-160 pb-160">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-7 col-md-10 col-sm-10">
                   
                    <div class="section-tittle section-tittle2 text-center mb-70">
                        <span>What we are doing</span>
                        <h2>Our Expert Volunteer Alwyes ready</h2>
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-team mb-30">
                        <div class="team-img">
                            <img src="assets/img/gallery/team1.png" alt="">
                           
                            <ul class="team-social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fas fa-globe"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-caption">
                            <h3><a href="instructor.html">Bruce Roberts</a></h3>
                            <p>Volunteer leader</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-team mb-30">
                        <div class="team-img">
                            <img src="assets/img/gallery/team2.png" alt="">
                           
                            <ul class="team-social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fas fa-globe"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-caption">
                            <h3><a href="instructor.html">Robart Rechard</a></h3>
                            <p>Volunteer leader</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-team mb-30">
                        <div class="team-img">
                            <img src="assets/img/gallery/team3.png" alt="">
                          
                            <ul class="team-social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fas fa-globe"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-caption">
                            <h3><a href="instructor.html">Brendon Tailor</a></h3>
                            <p>Volunteer leader</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-team mb-30">
                        <div class="team-img">
                            <img src="assets/img/gallery/team4.png" alt="">
                            
                            <ul class="team-social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fas fa-globe"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-caption">
                            <h3><a href="instructor.html">Walshr Hasgt</a></h3>
                            <p>Volunteer leader</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="wantToWork-area ">
        <div class="container">
            <div class="wants-wrapper w-padding2  section-bg" data-background="assets/img/gallery/section_bg01.png">
                <div class="row align-items-center justify-content-between">
                    <div class="col-xl-5 col-lg-9 col-md-8">
                        <div class="wantToWork-caption wantToWork-caption2">
                            <h2>Lets Chenge The World With Humanity</h2>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4">
                        <a href="#" class="btn white-btn f-right sm-left">Become A Volunteer</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div class="testimonial-area testimonial-padding">
        <div class="container">
          
            <div class="row d-flex justify-content-center">
                <div class="col-xl-8 col-lg-8 col-md-10">
                    <div class="h1-testimonial-active dot-style">
                        
                        <div class="single-testimonial text-center">
                            <div class="testimonial-caption ">
                              
                                <div class="testimonial-founder">
                                    <div class="founder-img mb-40">
                                        <img src="assets/img/gallery/testimonial.png" alt="">
                                        <span>Margaret Lawson</span>
                                        <p>Creative Director</p>
                                    </div>
                                </div>
                                <div class="testimonial-top-cap">
                                    <p>“I am at an age where I just want to be fit and healthy our bodies are our responsibility! So start caring for your body and it will care for you. Eat clean it will care for you and workout hard.”</p>
                                </div>
                            </div>
                        </div>
                       
                        <div class="single-testimonial text-center">
                            <div class="testimonial-caption ">
                              
                                <div class="testimonial-founder">
                                    <div class="founder-img mb-40">
                                        <img src="assets/img/gallery/testimonial.png" alt="">
                                        <span>Margaret Lawson</span>
                                        <p>Creative Director</p>
                                    </div>
                                </div>
                                <div class="testimonial-top-cap">
                                    <p>“I am at an age where I just want to be fit and healthy our bodies are our responsibility! So start caring for your body and it will care for you. Eat clean it will care for you and workout hard.”</p>
                                </div>
                            </div>
                        </div>
                     
                        <div class="single-testimonial text-center">
                            <div class="testimonial-caption ">
                              
                                <div class="testimonial-founder">
                                    <div class="founder-img mb-40">
                                        <img src="assets/img/gallery/testimonial.png" alt="">
                                        <span>Margaret Lawson</span>
                                        <p>Creative Director</p>
                                    </div>
                                </div>
                                <div class="testimonial-top-cap">
                                    <p>“I am at an age where I just want to be fit and healthy our bodies are our responsibility! So start caring for your body and it will care for you. Eat clean it will care for you and workout hard.”</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  

    <section class="home-blog-area section-padding30">
        <div class="container">
           
            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-6 col-md-9 col-sm-10">
                    <div class="section-tittle text-center mb-90">
                        <span>Our recent blog</span>
                        <h2>Latest News from our recent blog</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="home-blog-single mb-30">
                        <div class="blog-img-cap">
                            <div class="blog-img">
                                <img src="assets/img/gallery/home-blog1.png" alt="">
                                
                                <div class="blog-date text-center">
                                    <span>24</span>
                                    <p>Now</p>
                                </div>
                            </div>
                            <div class="blog-cap">
                                <p>Creative derector</p>
                                <h3><a href="blog_details.html">Footprints in Time is perfect House in Kurashiki</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="home-blog-single mb-30">
                        <div class="blog-img-cap">
                            <div class="blog-img">
                                <img src="assets/img/gallery/home-blog2.png" alt="">
                               
                                <div class="blog-date text-center">
                                    <span>24</span>
                                    <p>Now</p>
                                </div>
                            </div>
                            <div class="blog-cap">
                                <p>Creative derector</p>
                                <h3><a href="blog_details.html">Footprints in Time is perfect House in Kurashiki</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
 --> 
    <div class="count-down-area pt-25 section-bg" data-background="assets/img/gallery/section_bg02.png">
        <div class="container" id="products">
            <div class="row justify-content-center" >
                <div class="col-lg-12 col-md-12">
                    <div class="count-down-wrapper" >
                        <div class="row justify-content-between">
                        <?php
                                require_once('handle/db.php');
                                $perpage = 12;
                                if(isset($_GET['page']) & !empty($_GET['page'])){
                                    $curpage = $_GET['page'];
                                }else{
                                    $curpage = 1;
                                }
                                $start = ($curpage * $perpage) - $perpage;
                                $PageSql = "SELECT * FROM `UnderwriterList` limit 53";
                                $pageres = mysqli_query($conn, $PageSql);
                                $totalres = mysqli_num_rows($pageres);
                                 
                                $endpage = ceil($totalres/$perpage);
                                $startpage = 1;
                                $nextpage = $curpage + 1;
                                $previouspage = $curpage - 1;
                                 
                                $ReadSql = "SELECT * FROM `UnderwriterList` WHERE NOT (Name LIKE '%life%' or Name LIKE '%Pioneer Assurance Company Limited%' or Name LIKE '%Health%') ORDER BY Name LIMIT $start, $perpage";
                                
                                $res = mysqli_query($conn, $ReadSql);
                                while($r = mysqli_fetch_assoc($res)){
                                ?>  
                                    <div class="col-lg-3 col-md-6 col-sm-6" style="padding-bottom:20px;">
                                        <div class="card w3-hover-shadow w3-center">
                                            <img src="<?echo $r['path']?>" alt="Avatar" style="width:100%; height:100px;">
                                                <div class="containercard">
                                                    <h4><b><?echo $r["Name"]?></b></h4>
                                                   
                                                </div>
                                                <form action="dist/prodcategory.php" method="post">
                                                    <input id="underwriterid" name="underwriterid" type="hidden" value="<?echo $r['Name']?>">
                                                    <h3><input type="submit" value="View Products"></h3>
                                                </form>
                                            
                                        </div>                                        
                                    </div>
                                <?}?>                      
                        </div>
                        <nav aria-label="Page navigation" style="position:relative; left:0x;">
                                    <ul class="pagination">
                                    <?php if($curpage != $startpage){ ?>
                                        <li class="page-item">
                                            <a class="page-link" href="?page=<?php echo $startpage ?>#products" tabindex="-1" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">First</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <?php if($curpage >= 2){ ?>
                                        <li class="page-item"><a class="page-link" href="?page=<?php echo $previouspage ?>#products"><?php echo $previouspage ?></a></li>
                                    <?php } ?>
                                        <li class="page-item active"><a class="page-link" href="?page=<?php echo $curpage ?>#products"><?php echo $curpage ?></a></li>
                                    <?php if($curpage != $endpage){ ?>
                                        <li class="page-item"><a class="page-link" href="?page=<?php echo $nextpage ?>#products"><?php echo $nextpage ?></a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="?page=<?php echo $endpage ?>#products" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Last</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    </ul>
                                </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Count Down End -->
    </main>
    <footer>
        <div class="footer-wrapper section-bg2" data-background="assets/img/gallery/footer_bg.png">
            <!-- Footer Top-->
            <div class="footer-area footer-padding">
                <div class="container">
                    <div class="row d-flex justify-content-between">
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-footer-caption mb-50">
                            <div class="single-footer-caption mb-30">
                                <div class="footer-tittle">
                                    
                                    <div class="footer-logo mb-20" style="color:white;font-size:30px; font-weight: bold;">
                                        Iplus Insurance Agency Limited
                                        <!--<a href="index.html"><img src="assets/img/logo/logo2_footer.png" alt=""></a>-->
                                    </div>
                                    <div style="color:white;font-size:15px; font-weight;">
                                        PO BOX 29144
                                        00100 GPO

                                        Equity Bank Building Third floor suite 202, Ring Road Roundabout, Ngara Rd, Nairobi.<br>
                                        
                                    </div>
                                    <div style="color:#00ceff;font-size:15px; font-weight;">
                                        <a href=http://www.iplus.co.ke target="blank"> www.iplus.co.ke </a> <br>
                                        
                                    </div>
                                    <div style="color:red;"><a href=http://www.iplus.co.ke target="blank"> </a></div>
                                        
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                            <div class="single-footer-caption mb-50">
                                <div class="footer-tittle">
                                    <h4>Contact Info</h4>
                                    <ul>
                                        
                                        <li><a href="#">Phone : +254 733 566 464</a></li>
                                        <li><a href="#">Email : info@iplus.co.ke</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                            <div class="single-footer-caption mb-50">
                                <div class="footer-tittle">
                                    <h4>Important Link</h4>
                                    <ul>
                                        <li><a href="#"> View Products</a></li>
                                        <li><a href="#">Contact Us</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                            <div class="single-footer-caption mb-50">
                                <div class="footer-tittle">
                                    <h4>Newsletter</h4>
                                    <div class="footer-pera footer-pera2">
                                    <p>Subscribe to get our news letter</p>
                                </div>
                                <!-- Form -->
                                <div class="footer-form" >
                                    <div id="mc_embed_signup">
                                        <form target="_blank" action="#"
                                        method="get" class="subscribe_form relative mail_part">
                                            <input type="email" name="email" id="newsletter-form-email" placeholder="Email Address"
                                            class="placeholder hide-on-focus" onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = ' Email Address '">
                                            <div class="form-icon">
                                                <button type="submit" name="submit" id="newsletter-submit"
                                                class="email_icon newsletter-submit button-contactForm"><img src="assets/img/gallery/form.png" alt=""></button>
                                            </div>
                                            <div class="mt-10 info"></div>
                                        </form>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer-bottom -->
            <div class="footer-bottom-area">
                <div class="container">
                    <div class="footer-border">
                        <div class="row d-flex justify-content-between align-items-center">
                            <div class="col-xl-10 col-lg-9 ">
                                <div class="footer-copy-right">
                                    <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script>  Iplus Insurance Agency LTD | All Rights Reserved | Powerd by <i class="fa fa-hear" aria-hidden="true"></i> <a href=https://www.jendie.co.ke target="_blank"> Jendie Automobile LTD</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-3">
                                <div class="footer-social f-right">
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a  href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fas fa-globe"></i></a>
                                    <a href="#"><i class="fab fa-behance"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll Up -->
    <div id="back-top" >
        <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
    </div>

    <!-- JS here -->

    <script src="./assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="./assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="./assets/js/popper.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="./assets/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="./assets/js/owl.carousel.min.js"></script>
    <script src="./assets/js/slick.min.js"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="./assets/js/wow.min.js"></script>
    <script src="./assets/js/animated.headline.js"></script>
    <script src="./assets/js/jquery.magnific-popup.js"></script>

    <!-- Date Picker -->
    <script src="./assets/js/gijgo.min.js"></script>
    <!-- Nice-select, sticky -->
    <script src="./assets/js/jquery.nice-select.min.js"></script>
    <script src="./assets/js/jquery.sticky.js"></script>
    <!-- Progress -->
    <script src="./assets/js/jquery.barfiller.js"></script>
    
    <!-- counter , waypoint,Hover Direction -->
    <script src="./assets/js/jquery.counterup.min.js"></script>
    <script src="./assets/js/waypoints.min.js"></script>
    <script src="./assets/js/jquery.countdown.min.js"></script>
    <script src="./assets/js/hover-direction-snake.min.js"></script>

    <!-- contact js -->
    <script src="./assets/js/contact.js"></script>
    <script src="./assets/js/jquery.form.js"></script>
    <script src="./assets/js/jquery.validate.min.js"></script>
    <script src="./assets/js/mail-script.js"></script>
    <script src="./assets/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="./assets/js/plugins.js"></script>
    <script src="./assets/js/main.js"></script>
    
    </body>
</html>


<!--
UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/aar-logo.png'
WHERE ID=1;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/amaco-logo.jpeg'
WHERE ID=2;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/aig-logo.png'
WHERE ID=3;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/alianz-logo.png'
WHERE ID=4;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/apa-logo.png'
WHERE ID=5;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/britam-logo.png'
WHERE ID=8;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/cic-logo.png'
WHERE ID=12;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/coporate-logo.png'
WHERE ID=14;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/direct_line-logo.png'
WHERE ID=15;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/fidelity-logo.png'
WHERE ID=16;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/first_assurance-logo.png'
WHERE ID=17;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/ga-logo.png'
WHERE ID=18;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/geminia-logo.png'
WHERE ID=20;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/icea-logo.svg'
WHERE ID=21;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/intra-logo.png'
WHERE ID=23;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/invesco-logo.png'
WHERE ID=24;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/jubilee-logo.png'
WHERE ID=25;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/kenindia-logo.png'
WHERE ID=27;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/kenya_orient-logo.png'
WHERE ID=28;


UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/kuscco-logo.jpg'
WHERE ID=30;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/madison-logo.png'
WHERE ID=32;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/madison-logo.png'
WHERE ID=33;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/mayfair-logo.png'Health Assurance
WHERE ID=34;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/metropolitan-logo.png'
WHERE ID=10;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/mua-logo.png'
WHERE ID=39;


UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/occidental-logo.png'
WHERE ID=36;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/old-mutual-logo.png'
WHERE ID=37;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/Pacis-logo.jpeg'
WHERE ID=38;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/pioneer-logo.jpeg'
WHERE ID=40;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/pioneer-logo.jpeg'
WHERE ID=41;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/resolution-logo.jpeg'
WHERE ID=43;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/saham.jpeg'
WHERE ID=44;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/sanlam-logo.png'
WHERE ID=45;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/takaful-logo.jpeg'
WHERE ID=47;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/takaful-logo.jpeg'
WHERE ID=48;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/herritage-logo.png'
WHERE ID=49;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/jubilee-logo.png'
WHERE ID=50; 

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/kenya-alliance-logo.png'
WHERE ID=51;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/monarch-logo.png'
WHERE ID=52;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/trident-logo.jpeg'
WHERE ID=53;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/uap-logo.svg'
WHERE ID=54;

UPDATE UnderwriterList
SET path = 'assets/img/underwriterlogo/xplico-logo.jpeg'
WHERE ID=56;
-->