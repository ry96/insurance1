<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Year', 'Percentage'],
          ["Jan", 44],
          ["Feb", 31],
          ["March", 12],
          ["April", 10],
          ['May', 3],
          ["June",20],
          ["July",60],
          ["Aug",10],
          ["Sept",50],
          ["Oct",100],
          ["Nov",80],
          ["Dec",5],
        ]);

        var options = {
         
          legend: { position: 'none' },
          chart: {
            title: 'Total Revenue',
            subtitle: '2020' },
          axes: {
            x: {
              0: { side: 'bottom', label: 'Month'} // Top x-axis.
            }
          },
          bar: { groupWidth: "100%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
    </script>
  </head>
  <body>
    <div id="top_x_div" style="width: ; height: 400px;"></div>
  </body>
</html>
