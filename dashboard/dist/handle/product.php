<?php
    require "db.php";  
    if (isset($_POST["prodname"]) && (isset($_FILES['prodlogo']))){ 
        $productunderwriter = "../assets/productunderwriter/";
        if (!file_exists($productunderwriter)) {
            mkdir($productunderwriter, 0777, true);
        }
        $prodname = ucwords($_POST["prodname"]);
        $risk = ucwords($_POST["risk"]);
        $underwriter = $_POST["underwriter"];
        $coverage = ucwords($_POST["coverage"]);
        $shortrates = $_POST["shortrates"];
        $anualrates = $_POST["anualrates"];
        $clause = $_POST["clause"];
        $conditions = ucwords($_POST["conditions"]);
        $optionalbenefit0 = ucwords($_POST["optionalbenefit0"]);
        $optionalbenefit1 = $_POST["optionalbenefit1"];
        $optionalbenefit2 = $_POST["optionalbenefit2"];
        $arriskcovered =  (explode(". ", $_POST['risk']));
        $boolvehicletype = (int)$arriskcovered[0];
        $riskcovered = $arriskcovered[1];
        if ($boolvehicletype <= 2){
           $vehicletype = "MOTORCYCLE";
           #echo $vehicletype; 
        }elseif ($boolvehicletype > 2 && $boolvehicletype <= 4){
            $vehicletype = "TRICYCLE";
            #echo $vehicletype;
        }else {
            $vehicletype = "MOTORVEHICLE";
            #echo $vehicletype;
        } 
        if (isset($_POST["tonnage"])){
            $tonnage = $_POST["tonnage"];
            $resulttone = mysqli_query($conn, 'SELECT TONNAGE FROM Product WHERE TONNAGE = "'.$tonnage.'" LIMIT 1');
             if(mysqli_num_rows($resulttone) > 0){
                 
                 $responce = "Product :" . $prodname ."<br>with: 1 Riskcovered " . $riskcovered . "<br>Underwriter " . $underwriter . "<br>Coverage ". $coverage ."</b> <br>Already exists";
                  include "alert.php";
                  header( "refresh:2;url=../dashboard.php" );   
                 die();
             }  
        } else{
           $tonnage = "NA"; 
        }
        if (isset($_POST["minimum_premium"])){
           $minimum_premium = $_POST["minimum_premium"];
           $selectyear = $_POST["selectyear"];
        }else{
            $minimum_premium ="NA";
            $selectyear ="NA";
        }


        function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') {
        $str = '';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[mt_rand(0, $count-1)];
        }
        return $str; }

        $random_string = randString(10);
        $is_unique = false;


        $file_name = $_FILES['prodlogo']['name'];
        $file_tmp =$_FILES['prodlogo']['tmp_name'];
        $path = $productunderwriter .$random_string."_".$file_name; 
         
        if(move_uploaded_file($file_tmp, $path)){
            $logo = trim($path, './');   
        }
        $unique = $prodname . $riskcovered . $underwriter . $coverage;
       
        $result = mysqli_query($conn, 'SELECT ID FROM Product WHERE PRODUCT_IDENTIFIER = "'.$random_string.'" LIMIT 1');
        if ($result === false)   // if you don't get a result, then you're good
            $is_unique = true;
        else                     // if you DO get a result, keep trying
            $random_string = randString(10);
            $productidentifier =$random_string;

        $sql = "INSERT into Product (
            PRODUCT_NAME, 
            RISK_COVERED,
            UNDERWRITER,
            COVERAGE,
            SHORTTERM_RATES,
            ANNUAL_RATES, 
            CLAUSES, 
            CONDITIONS_AND_WARRANTIES, 
            OPTIONAL_ADDITIONAL_BENEFITS,
            OP_BMP, 
            OP_BR,
            VEHICLE_TYPE,
            PRODUCT_LOGO,
            YEAR_ACCEPTED,
            TONNAGE,
            MINIMUM_PREMIUM,
            PRODUCT_IDENTIFIER,
            UNIQUEID
            
        ) VALUES (
            '$prodname',
            '$riskcovered',
            '$underwriter',
            '$coverage',
            '$shortrates',
            '$anualrates',
            '$clause',
            '$conditions',
            '$optionalbenefit0',
            '$optionalbenefit1',
            '$optionalbenefit2',
            '$vehicletype',
            '$logo',
            '$selectyear',
            '$tonnage',
            '$minimum_premium', 
            '$productidentifier',
            '$unique'
        )";
        $sql2 = "INSERT into Product_additional_benefits (
            OPTIONAL_ADDITIONAL_BENEFITS,
            OP_BMP, 
            OP_BR,
            PRODUCT_IDENTIFIER
        )VALUES(
            '$optionalbenefit0',
            '$optionalbenefit1',
            '$optionalbenefit2',
            '$productidentifier' 
        )"; 
        
        mysqli_query($conn, $sql2);
       $duplicate = mysqli_query($conn, 'SELECT PRODUCT_NAME, RISK_COVERED, UNDERWRITER, COVERAGE, count(*) from Product group by PRODUCT_NAME, RISK_COVERED, UNDERWRITER, COVERAGE having count(*) > 0');
       if (mysqli_query($conn, $sql)) {
                $responce = "Product <b>$prodname</b> created successfully";
               
        }else{
            $responce = mysqli_error($conn);
            if (strpos($responce, 'Duplicate entry') !== false) {
                $responce = "Product :" . $prodname ."<br>with: 1 Riskcovered " . $riskcovered . "<br>Underwriter " . $underwriter . "<br>Coverage ". $coverage ."</b> <br>Already exists";
            } else {
               $responce = "An unknown error has occured";
            } 
        }
       
       include "alert.php";
       mysqli_close($conn);
       
        
    }
    
header( "refresh:2;url=../index.php" );
    