<?php
    require "db.php";
    if (isset($_POST['contact'])){
        $agentfilespath = "../assets/agentfilespath/";
        if (!file_exists($agentfilespath)) {
            mkdir($agentfilespath, 0777, true);
        }
        $contact =   $_POST['contact'];
        $company =   $_POST['company'];
        $reg =     $_POST['reg'];
        $kra =     $_POST['kra'];
        #$kracpy =   $_POST['kracpy'];
        $adress =   $_POST['adress'];
        $email =   $_POST['email'];
        $phone =   $_POST['phone'];
        $tax =   $_POST['tax'];
       
        $file_name = $_FILES['krafile']['name'];
        $file_tmp =$_FILES['krafile']['tmp_name'];
        $path = $agentfilespath . time(). $file_name;
        if(move_uploaded_file($file_tmp, $path)){
            $kracpy  = trim($path, './');   
        }
        
        $sql = "INSERT into Agent (
            Contact_Person,
            Company_Name,
            REG_Cert,
            KRA_Pin_number, 
            KRA_PIN_COPY,
            Physical_Address,
            Email_Address,
            Phone_number,
            Withholding_Tax
        ) VALUES (

            '$contact',
            '$company',
            '$reg',
            '$kra',
            '$kracpy',
            '$adress',
            '$email',
            '$phone',
            '$tax'
        )";
        if (mysqli_query($conn, $sql)) {
            $responce = "Agent <b>$contact</b> created successfully";
        } else {
            if(strpos(mysqli_error($conn), "Duplicate") !== false){
                $responce = "Agent <b>$contact</b>  Exisit";
            } else{
                $responce = mysqli_error($conn);
            }
            
        }
        include "alert.php";
        mysqli_close($conn);
    }
    
header( "refresh:2;url=../index.php" );