-- MariaDB dump 10.17  Distrib 10.4.14-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: jendie
-- ------------------------------------------------------
-- Server version	10.4.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Agent`
--

DROP TABLE IF EXISTS `Agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) NOT NULL,
  `INFORMAION` varchar(255) DEFAULT NULL,
  `ID_PASSPORT` varchar(255) DEFAULT NULL,
  `ID_PASSPORT_COPY` varchar(255) DEFAULT NULL,
  `KRA_PIN` varchar(255) DEFAULT NULL,
  `KRA_PIN_COPY` varchar(255) DEFAULT NULL,
  `IRA_LICENSE` varchar(255) DEFAULT NULL,
  `IRA_LICENSE_COPY` varchar(255) DEFAULT NULL,
  `PHYSICAL_ADDRESS` varchar(255) DEFAULT NULL,
  `PHONE_NUMBER` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `USERNAME` (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Agent`
--

LOCK TABLES `Agent` WRITE;
/*!40000 ALTER TABLE `Agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `Agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Client`
--

DROP TABLE IF EXISTS `Client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Client` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Fname` varchar(255) DEFAULT NULL,
  `Lname` varchar(255) DEFAULT NULL,
  `Cert_for` varchar(255) DEFAULT NULL,
  `KRA_PIN` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `PHONE` varchar(255) DEFAULT NULL,
  `OCCUPATION` varchar(255) DEFAULT NULL,
  `RESIDENCE` varchar(255) DEFAULT NULL,
  `PROFILE_PHOTO` varchar(255) DEFAULT NULL,
  `VEHICLE_DETAILS` varchar(255) DEFAULT NULL,
  `MNAME` varchar(255) DEFAULT NULL,
  `CertFile` varchar(255) DEFAULT NULL,
  `KRAFILE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uc_KRA_PIN` (`KRA_PIN`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Client`
--

LOCK TABLES `Client` WRITE;
/*!40000 ALTER TABLE `Client` DISABLE KEYS */;
INSERT INTO `Client` VALUES (1,'Erick','Ngumbau','325422','5656565656','erick.soi@hotmail.com','0712962787','Programmer','Embakasi','clientfiles/15927934262bhic_nl (2).csv','clientfiles/15927934263data_spider.py','Soi','clientfiles/15927934260action_doc.odt','clientfiles/15927934261bhic_nl.csv');
/*!40000 ALTER TABLE `Client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContactUs`
--

DROP TABLE IF EXISTS `ContactUs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContactUs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContactUs`
--

LOCK TABLES `ContactUs` WRITE;
/*!40000 ALTER TABLE `ContactUs` DISABLE KEYS */;
INSERT INTO `ContactUs` VALUES (1,'+254 722 301 062');
/*!40000 ALTER TABLE `ContactUs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Coverage`
--

DROP TABLE IF EXISTS `Coverage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Coverage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `cover` varchar(255) NOT NULL,
  `Value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Coverage`
--

LOCK TABLES `Coverage` WRITE;
/*!40000 ALTER TABLE `Coverage` DISABLE KEYS */;
INSERT INTO `Coverage` VALUES (1,'Third Party Only','third'),(2,'Third Party And Theft','thirdAndT'),(3,'Comprehensive','comprehensive');
/*!40000 ALTER TABLE `Coverage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Persons`
--

DROP TABLE IF EXISTS `Persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Persons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `recog` varchar(255) NOT NULL,
  `kra` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Persons`
--

LOCK TABLES `Persons` WRITE;
/*!40000 ALTER TABLE `Persons` DISABLE KEYS */;
/*!40000 ALTER TABLE `Persons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_NAME` varchar(255) DEFAULT NULL,
  `RISK_COVERED` varchar(255) DEFAULT NULL,
  `UNDERWRITER` varchar(255) DEFAULT NULL,
  `COVERAGE` varchar(255) DEFAULT NULL,
  `SHORTTERM_RATES` varchar(255) DEFAULT NULL,
  `ANNUAL_RATES` varchar(255) DEFAULT NULL,
  `CLAUSES` varchar(255) DEFAULT NULL,
  `CONDITIONS_AND_WARRANTIES` varchar(255) DEFAULT NULL,
  `OPTIONAL_ADDITIONAL_BENEFITS` varchar(255) DEFAULT NULL,
  `OP_BMP` varchar(255) DEFAULT NULL,
  `OP_BR` varchar(255) DEFAULT NULL,
  `VEHICLE_TYPE` varchar(255) DEFAULT NULL,
  `PRODUCT_LOGO` varchar(255) DEFAULT NULL,
  `PRODUCT_IDENTIFIER` varchar(255) DEFAULT NULL,
  `YEAR_ACCEPTED` varchar(255) DEFAULT NULL,
  `UNIQUEID` varchar(255) DEFAULT NULL,
  `TONNAGE` varchar(255) DEFAULT NULL,
  `MINIMUM_PREMIUM` varchar(255) DEFAULT NULL,
  `UNDERWRITER_EMAIL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUEID` (`UNIQUEID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product`
--

LOCK TABLES `Product` WRITE;
/*!40000 ALTER TABLE `Product` DISABLE KEYS */;
INSERT INTO `Product` VALUES (1,'Bimaplus(MOTORINSURANCE)','PSV (bodaboda)','The Monarch Insurance Company Limited','Third Party Only','THIRD PARTY','3500','TBA','TBA','EXCESS PROTECTOR','2500','0.25','MOTORCYCLE','images/productunderwriter/H43itOQziq_DIRECT LINE.png','RPhCv9sQg4','NA','Bimaplus(MOTORINSURANCE)PSV (bodaboda)The Monarch Insurance Company LimitedThird Party Only','NA','NA',NULL),(2,'Bimaplus(MOTORINSURANCE)','PSV (bodaboda)','The Monarch Insurance Company Limited','Comprehensive','COMPREHENSIVE','3','TBA','TBA','PERSONAL ACCIDENT','2500','2500','MOTORCYCLE','images/productunderwriter/he03H9W2GK_Monarch.png','LZzRqgi4c6','2005','Bimaplus(MOTORINSURANCE)PSV (bodaboda)The Monarch Insurance Company LimitedComprehensive','NA','7100',NULL),(3,'Bimaplus(MOTORINSURANCE)','Private','AIG Kenya Insurance Company Limited','Third Party Only','THIRD PARTY ONLY','7500','TBA','TBA','','','','MOTORVEHICLE','images/productunderwriter/rltNjiwCcc_AIG.png','pSxg3XIyPP','NA','Bimaplus(MOTORINSURANCE)PrivateAIG Kenya Insurance Company LimitedThird Party Only','NA','NA',NULL),(4,'Bimaplus(MOTORINSURANCE)','Comercial Own goods','AIG Kenya Insurance Company Limited','Comprehensive','COMPREHENSIVE','5','TBA','TBA','Excess Protector','2500','0.25','MOTORVEHICLE','images/productunderwriter/d9ue9UrnAW_AIG.png','W0UIkZ6bZT','2005','Bimaplus(MOTORINSURANCE)Comercial Own goodsAIG Kenya Insurance Company LimitedComprehensive','NA','30000',NULL),(5,'Bimaplus(MOTORINSURANCE)','Private','Trident Insurance Company Limited','Third Party Only','Any cost arising from any legal proceedings and the costs of repairing the other parties vehicle or any compensation granted to third party persons due to injury or death','7500','TBA','TBA','','','','MOTORVEHICLE','assets/productunderwriter/pAaZFN32Cj_TRIDENT INSURANCE.jfif','ggjEphcAuU','NA','Bimaplus(MOTORINSURANCE)PrivateTrident Insurance Company LimitedThird Party Only','NA','NA',NULL);
/*!40000 ALTER TABLE `Product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductList`
--

DROP TABLE IF EXISTS `ProductList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductList` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductList`
--

LOCK TABLES `ProductList` WRITE;
/*!40000 ALTER TABLE `ProductList` DISABLE KEYS */;
INSERT INTO `ProductList` VALUES (1,'Bimaplus(MOTORINSURANCE)');
/*!40000 ALTER TABLE `ProductList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product_additional_benefits`
--

DROP TABLE IF EXISTS `Product_additional_benefits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product_additional_benefits` (
  `PRODUCT_IDENTIFIER` varchar(255) DEFAULT NULL,
  `OPTIONAL_ADDITIONAL_BENEFITS` varchar(255) DEFAULT NULL,
  `OP_BMP` varchar(255) DEFAULT NULL,
  `OP_BR` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product_additional_benefits`
--

LOCK TABLES `Product_additional_benefits` WRITE;
/*!40000 ALTER TABLE `Product_additional_benefits` DISABLE KEYS */;
INSERT INTO `Product_additional_benefits` VALUES ('RPhCv9sQg4','EXCESS PROTECTOR','2500','0.25'),('RPhCv9sQg4','PASSENGER LEGAL LIABILITY','500','500'),('RPhCv9sQg4','WINDSCREEN','1000','10'),('LZzRqgi4c6','PERSONAL ACCIDENT','2500','2500'),('pSxg3XIyPP','','',''),('pSxg3XIyPP','PERSONAL ACCIDENT','2500','2500'),('W0UIkZ6bZT','Excess Protector','2500','0.25'),('W0UIkZ6bZT','PASSENGER LEGAL LIABILITY','500','500'),('W0UIkZ6bZT','POLITICAL VIOLENCE AND TERRORISM','2500','0.25'),('W0UIkZ6bZT','Personal Accident','1500','1500'),('W0UIkZ6bZT','WINDSCREEN','1000','10'),('I5HxM4XBcH','EXCESS PROTECTOR','5000','0.5'),('KrnHIPlD4L','EXCESS PROTECTOR','5000','0.5'),('JwZ3ybiYlN','EXCESS PROTECTOR','5000','0.5'),('ggjEphcAuU','','','');
/*!40000 ALTER TABLE `Product_additional_benefits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Restricted_Vehicles`
--

DROP TABLE IF EXISTS `Restricted_Vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Restricted_Vehicles` (
  `PRODUCT_IDENTIFIER` varchar(255) DEFAULT NULL,
  `Vehicle` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Restricted_Vehicles`
--

LOCK TABLES `Restricted_Vehicles` WRITE;
/*!40000 ALTER TABLE `Restricted_Vehicles` DISABLE KEYS */;
INSERT INTO `Restricted_Vehicles` VALUES ('RPhCv9sQg4','Isuzu'),('RPhCv9sQg4','Isuzu');
/*!40000 ALTER TABLE `Restricted_Vehicles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Underwriter`
--

DROP TABLE IF EXISTS `Underwriter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Underwriter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `LEGAL_ENTITY` varchar(255) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `WEBSITE` varchar(255) DEFAULT NULL,
  `LOGO` varchar(255) DEFAULT NULL,
  `LOCATION_OPTIONAL` varchar(255) DEFAULT NULL,
  `BANK_ACCOUNT_OPTIONAL` varchar(255) DEFAULT NULL,
  `UNIQUE_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uc_name` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Underwriter`
--

LOCK TABLES `Underwriter` WRITE;
/*!40000 ALTER TABLE `Underwriter` DISABLE KEYS */;
INSERT INTO `Underwriter` VALUES (1,'AIG KENYA INSURANCE COMPANY LIMITED','AIG Is A World Leading Property-casualty And General Insurance Organization.','P.O. Box 49460 -00100.Nairobi-Kenya','aigkenya@aig.com','www.aig.co.ke','','Eden Square ComplexChiromo Road. ','TBA','It%_VJ*kCbglMjif9h#n');
/*!40000 ALTER TABLE `Underwriter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UnderwriterList`
--

DROP TABLE IF EXISTS `UnderwriterList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UnderwriterList` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UnderwriterList`
--

LOCK TABLES `UnderwriterList` WRITE;
/*!40000 ALTER TABLE `UnderwriterList` DISABLE KEYS */;
INSERT INTO `UnderwriterList` VALUES (1,'AAR Insurance Company Limited','assets/img/underwriterlogo/aar-logo.png','knyaga@iplus.co.ke'),(2,'Africa Merchant Assurance Company Limited','assets/img/underwriterlogo/amaco-logo.jpeg','knyaga@iplus.co.ke'),(3,'AIG Kenya Insurance Company Limited','assets/img/underwriterlogo/aig-logo.png','knyaga@iplus.co.ke'),(4,'Allianz Insurance Company of Kenya Limited','assets/img/underwriterlogo/alianz-logo.png','knyaga@iplus.co.ke'),(5,'APA Insurance Limited','assets/img/underwriterlogo/apa-logo.png','knyaga@iplus.co.ke'),(6,'APA Life Assurance Company Limited',NULL,'knyaga@iplus.co.ke'),(7,'Barclays Life Assurance Kenya Limited',NULL,'knyaga@iplus.co.ke'),(8,'Britam General Insurance Company (K) Limited','assets/img/underwriterlogo/britam-logo.png','knyaga@iplus.co.ke'),(9,'Britam Life Assurance Company (K) Limited',NULL,'knyaga@iplus.co.ke'),(10,'Metropolitan Cannon General Insurance Company Limited','assets/img/underwriterlogo/metropolitan-logo.png','knyaga@iplus.co.ke'),(11,'Capex Life Assurance Company Limited',NULL,'knyaga@iplus.co.ke'),(12,'CIC General Insurance Company Limited','assets/img/underwriterlogo/cic-logo.png','knyaga@iplus.co.ke'),(13,'CIC Life Assurance Company Limited',NULL,'knyaga@iplus.co.ke'),(14,'Corporate Insurance Company Limited','assets/img/underwriterlogo/coporate-logo.png','knyaga@iplus.co.ke'),(15,'Directline Assurance Company Limited','assets/img/underwriterlogo/direct_line-logo.png','knyaga@iplus.co.ke'),(16,'Fidelity Shield Insurance Company Limited','assets/img/underwriterlogo/fidelity-logo.png','knyaga@iplus.co.ke'),(17,'First Assurance Company Limited','assets/img/underwriterlogo/first_assurance-logo.png','knyaga@iplus.co.ke'),(18,'GA Insurance Limited','assets/img/underwriterlogo/ga-logo.png','knyaga@iplus.co.ke'),(19,'GA Life Assurance Limited',NULL,'knyaga@iplus.co.ke'),(20,'Geminia Insurance Company Limited','assets/img/underwriterlogo/geminia-logo.png','knyaga@iplus.co.ke'),(21,'ICEA LION General Insurance Company Limited','assets/img/underwriterlogo/icea-logo.svg','knyaga@iplus.co.ke'),(22,'ICEA LION Life Assurance Company Limited',NULL,'knyaga@iplus.co.ke'),(23,'Intra Africa Assurance Company Limited','assets/img/underwriterlogo/intra-logo.png','knyaga@iplus.co.ke'),(24,'Invesco Assurance Company Limited','assets/img/underwriterlogo/invesco-logo.png','knyaga@iplus.co.ke'),(25,'Jubilee General Insurance Limited','assets/img/underwriterlogo/jubilee-logo.png','knyaga@iplus.co.ke'),(26,'Jubilee Health Insurance Limited',NULL,'knyaga@iplus.co.ke'),(27,'Kenindia Assurance Company Limited','assets/img/underwriterlogo/kenindia-logo.png','knyaga@iplus.co.ke'),(28,'Kenya Orient Insurance Limited','assets/img/underwriterlogo/kenya_orient-logo.png','knyaga@iplus.co.ke'),(29,'Kenya Orient Life Assurance Limited',NULL,'knyaga@iplus.co.ke'),(30,'KUSCCO Mutual Assurance Limited','assets/img/underwriterlogo/kuscco-logo.jpg','knyaga@iplus.co.ke'),(31,'Liberty Life Assurance Kenya Limited',NULL,'knyaga@iplus.co.ke'),(32,'Madison Insurance Company Kenya Limited','assets/img/underwriterlogo/madison-logo.png','knyaga@iplus.co.ke'),(33,'Madison General Insurance Kenya Limited','assets/img/underwriterlogo/madison-logo.png','knyaga@iplus.co.ke'),(34,'Mayfair Insurance Company Limited',NULL,'knyaga@iplus.co.ke'),(35,'Metropolitan Cannon Life Assurance Limited',NULL,'knyaga@iplus.co.ke'),(36,'Occidental Insurance Company Limited','assets/img/underwriterlogo/occidental-logo.png','knyaga@iplus.co.ke'),(37,'Old Mutual Assurance Company Limited','assets/img/underwriterlogo/old-mutual-logo.png','knyaga@iplus.co.ke'),(38,'Pacis Insurance Company Limited','assets/img/underwriterlogo/Pacis-logo.jpeg','knyaga@iplus.co.ke'),(39,'MUA Insurance ( Kenya) Limited 01','assets/img/underwriterlogo/mua-logo.png','knyaga@iplus.co.ke'),(40,'Pioneer General Insurance Company Limited','assets/img/underwriterlogo/pioneer-logo.jpeg','knyaga@iplus.co.ke'),(41,'Pioneer Assurance Company Limited','assets/img/underwriterlogo/pioneer-logo.jpeg','knyaga@iplus.co.ke'),(42,'Prudential Life Assurance Company Limited',NULL,'knyaga@iplus.co.ke'),(43,'Resolution Insurance Company Limited','assets/img/underwriterlogo/resolution-logo.jpeg','knyaga@iplus.co.ke'),(44,'Saham Assurance Company Kenya Limited','assets/img/underwriterlogo/saham.jpeg','knyaga@iplus.co.ke'),(45,'Sanlam General Insurance Company Limited','assets/img/underwriterlogo/sanlam-logo.png','knyaga@iplus.co.ke'),(46,'Sanlam Life Insurance Company Limited',NULL,'knyaga@iplus.co.ke'),(47,'Takaful Insurance of Africa Limited','assets/img/underwriterlogo/takaful-logo.jpeg','knyaga@iplus.co.ke'),(48,'Tausi Assurance Company Limited','assets/img/underwriterlogo/takaful-logo.jpeg','knyaga@iplus.co.ke'),(49,'The Heritage Insurance Company Limited','assets/img/underwriterlogo/herritage-logo.png','knyaga@iplus.co.ke'),(50,'The Jubilee Insurance Company of Kenya Limited','assets/img/underwriterlogo/jubilee-logo.png','knyaga@iplus.co.ke'),(51,'The Kenyan Alliance Insurance Company Limited','assets/img/underwriterlogo/kenya-alliance-logo.png','knyaga@iplus.co.ke'),(52,'The Monarch Insurance Company Limited','assets/img/underwriterlogo/monarch-logo.png','knyaga@iplus.co.ke'),(53,'Trident Insurance Company Limited','assets/img/underwriterlogo/trident-logo.jpeg','knyaga@iplus.co.ke'),(54,'UAP Insurance Company Limited','assets/img/underwriterlogo/uap-logo.svg','knyaga@iplus.co.ke'),(55,'UAP Life Assurance Limited',NULL,'knyaga@iplus.co.ke'),(56,'Xplico Insurance Company Limited','assets/img/underwriterlogo/xplico-logo.jpeg','knyaga@iplus.co.ke');
/*!40000 ALTER TABLE `UnderwriterList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Fname` varchar(255) DEFAULT NULL,
  `Lname` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `ID_no` varchar(255) DEFAULT NULL,
  `KRA` varchar(255) DEFAULT NULL,
  `Bank_No` varchar(255) DEFAULT NULL,
  `NSSF` varchar(255) DEFAULT NULL,
  `NHIF` varchar(255) DEFAULT NULL,
  `Signature` varchar(255) DEFAULT NULL,
  `ACCESS` varchar(255) DEFAULT NULL,
  `IDIMG` varchar(255) DEFAULT NULL,
  `KRADOC` varchar(255) DEFAULT NULL,
  `BANKIMG` varchar(255) DEFAULT NULL,
  `NSSFDOC` varchar(255) DEFAULT NULL,
  `NHIFDOC` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uc_ID_no` (`ID_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ValuationCenters`
--

DROP TABLE IF EXISTS `ValuationCenters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ValuationCenters` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Town` varchar(255) DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `Tel` varchar(255) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `ContactPerson` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ValuationCenters`
--

LOCK TABLES `ValuationCenters` WRITE;
/*!40000 ALTER TABLE `ValuationCenters` DISABLE KEYS */;
INSERT INTO `ValuationCenters` VALUES (1,'NAIROBI','2nd floor, rm 2B Muthithi Rd,Westlands','0202632578','info@regentautovaluers.co.ke','Imelda'),(2,'NAIROBI','Luther Plaza, University Way Roundabout, Nyerere Rd P.O Box 34365 – 00100 Nairobi, Kenya','0202603906,0722608210,0700394000,0700393777','lutheran@regentautovaluers.co.ke','Mr. Festus Kaleli'),(3,'NAIROBI','Muthaiga Square, 3rd Floor','0612303476,0728292912','pangani@regentautovaluers.co.ke','Mr. Alex'),(4,'NAIROBI','Westlands, Muthithi Rd, Opposite avocado Towers','0202603916,0711599665','westlands@regentautovaluers.co.ke','Mr. Robert Mwangi.'),(5,'ELDORET','WATERGATE PLAZA','0202134075,0700077009','eldoret@regentautovaluers.co.ke','Mr. Khaemba Andrew'),(6,'KISUMU','AWORI HOUSE','0202134077,0724228228','kisumu@regentautovaluers.co.ke','Mr. Enock Ochieng.'),(7,'MALINDI','Royal Complex Rm no G15,','0712840088','regentmalindi@gmail.com','Mr. Mwangi'),(8,'NAROK','NENKAI PLAZA','07184333330779338531,','narok@regentautovaluers.co.ke','Mr. Muli'),(9,'NAIROBI','Upperhill, Matumbato Rd','0202605723,0704879000','upperhill@regentautovaluers.co.ke','Mr. Munene'),(10,'NAITOBI','Utawala, Grey Park Heights, 1st Flr, Room A17','0770626463,0790884101','utawala@regentautovaluers.co.ke','Mr. Tabale'),(11,'NAIROBI','Buruburu Complex – Nairobi, Room B3','0202603915,0702501050','buruburu@regentautovaluers.co.ke','Mr. Elias Mwangi.'),(12,'NGONG','Along Ngong Road (from town) 50 Meters from China Center, At Jameson Court Block B Office No. 10','0713092158,0772733808','ngongroad@regentautovaluers.co.ke','Mr. Maina'),(13,'KITALE','AMBWERE PLAZA','0702512727','regentbungoma@gmail.com,kitale@regentautovaluers.co.ke','Mr. Kepha.'),(14,'MACHAKOS','PEMA HOUSE','0771569035,0728969509,0719723650,0720274150','machakos@regentautovaluers.co.ke','Mr. Muli'),(15,'THIKA','EQUITY PLAZA','0202693664,0712636368,0705198131','thika@regentautovaluers.co.ke','Mr. Nelson Kiirinya'),(16,'MERU','TWIN PLAZA','0202603909,0718094777','meru@regentautovaluers.co.ke','Mr. Njeru'),(17,'NAKURU','MASTERS PLAZA 4TH FLOOR :WING 2 :ROOM 1','0202134076,0727707017','nakuru@regentautovaluers.co.ke','Mr. Lawrence Macharia'),(18,'MOMBASA','YUNIS BUILDING 3RD FLOOR ROOM NO.4','0412220430,0727707016','mombasa@regentautovaluers.co.ke','Mr. Waweru'),(19,'NYERI','GATHII HSE','0612034027,0723697400','nyeri@regentautovaluers.co.ke','Mr. Mukundi.'),(20,'EMBU','NEEMA PLAZA','0202603918,0712839944','embu@regentautovaluers.co.ke','Mr. Mukangu'),(21,'KISII','UPENDO PLAZA','0715710257,0701714652','kisii@regentautovaluers.co.ke','Mr. Caleb Oyugi'),(22,'RUIRU','SANFRED HOUSE, SECOND FLOOR, RM 205','0772684059,0708156785','ruiru@regentautovaluers.co.ke','Mr. Kuko'),(23,'KITUI','KCB BUILDING 2ND FLOOR','0711599775','regentkitui@gmail.com','Mr. Sylvester Mulei'),(24,'KERUGOYA','','0704262478','kerugoya@regentautovaluers.co.ke','Mr. Munene'),(25,'NANYUKI','Nanyuki rd, Chadwick house ground floor','0704262613','nanyuki@regentautovaluers.co.ke','Mr. Murugami'),(26,'NAIVASHA','DOVE COMPLEX 1ST FLOOR ROOM 17, KARIUKI CHOTARA ROAD','0704262501,0772733809','naivasha@regentautovaluers.co.ke','Mr. Mwangi'),(27,'NAIROBI','Tymes Arcade 2nd Flr Rm 212, ONGATA RONGAI','254774086657','rongai@regentautovaluers.co.ke','Mr. Thiba.'),(28,'KAJIADO','Betty Plaza, 3rd floor, rm 108 ?, Kitengela','0772684060,0700158152','kitengela@regentautovaluers.co.ke','Mr. Adan Galgalo'),(29,'NYALI','','0700158124,0772762883','nyali@regentautovaluers.co.ke','Mr. Murangiri'),(30,'KAKAMEGA','Emmisioma Building 2nd Flr behind Post Bank, Along Cannon Awori Street','254790122193','kakamega@regentautovaluers.co.ke','Mr. Dennis Buyema.'),(31,'NYAHURURU','','0724276242','regentnyahururu@gmail.com','Mr. Nahashon Kariuki'),(32,'KERICHO','JEMI PLAZA ROOM NO.BA9, Next to Bank of Africa Opposite Garden Hotel','0712840202,0772762874','kericho@regentautovaluers.co.ke','Mr. Zablon'),(33,'MOMBASA ROAD','Nextgen Mall','0799396534,0778005571','nextgen@regentautovaluers.co.ke','Mr. Stanley Kioko.'),(34,'KIAMBU','Quick Matt, Komrades Business Center Room 7.','0775550802,0716601759','kiambu@regentautovaluers.co.ke','Mr. Maina'),(35,'KIKUYU','Ivory Towers, Room B4','0793276932,0771099216','kikuyu@regentautovaluers.co.ke','Mr. Daniel Kinyua');
/*!40000 ALTER TABLE `ValuationCenters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VehicleClass`
--

DROP TABLE IF EXISTS `VehicleClass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VehicleClass` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VehicleClass`
--

LOCK TABLES `VehicleClass` WRITE;
/*!40000 ALTER TABLE `VehicleClass` DISABLE KEYS */;
INSERT INTO `VehicleClass` VALUES (1,'MOTORCYCLE','Private'),(2,'MOTORCYCLE','PSV (bodaboda)'),(3,'TRICYCLE','Comercial Own goods'),(4,'TRICYCLE','PSV (tuktuk)'),(5,'MOTORVEHICLE','Private'),(6,'MOTORVEHICLE','Comercial Own goods'),(7,'MOTORVEHICLE','General Cartage Lorries,Trucks and Tankers'),(8,'MOTORVEHICLE','Agricultural and Forestry vehicles'),(9,'MOTORVEHICLE','Chauffeur driven'),(10,'MOTORVEHICLE','Motor trade'),(11,'MOTORVEHICLE','Institutional Vehicles'),(12,'MOTORVEHICLE','Driving school Vehicle'),(13,'MOTORVEHICLE','Tour Service Vehicles'),(14,'MOTORVEHICLE','PSV - Matatu'),(15,'MOTORVEHICLE','PSV - Taxi'),(16,'MOTORVEHICLE','Ambulance and fire fighters'),(17,'MOTORVEHICLE','Forklift,Crane, Rollers and Excavators');
/*!40000 ALTER TABLE `VehicleClass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurance`
--

DROP TABLE IF EXISTS `insurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plate` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `delivery` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurance`
--

LOCK TABLES `insurance` WRITE;
/*!40000 ALTER TABLE `insurance` DISABLE KEYS */;
/*!40000 ALTER TABLE `insurance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logbook`
--

DROP TABLE IF EXISTS `logbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Registration` varchar(255) DEFAULT NULL,
  `Chasis` varchar(255) DEFAULT NULL,
  `Make` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Body` varchar(255) DEFAULT NULL,
  `Fuel` varchar(255) DEFAULT NULL,
  `ManYear` varchar(255) DEFAULT NULL,
  `EngineNo` varchar(255) DEFAULT NULL,
  `Color` varchar(255) DEFAULT NULL,
  `RegDate` varchar(255) DEFAULT NULL,
  `GrossWeight` varchar(255) DEFAULT NULL,
  `Owners` varchar(255) DEFAULT NULL,
  `Passangers` varchar(255) DEFAULT NULL,
  `Loads` varchar(255) DEFAULT NULL,
  `Tax` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `Pin` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `KRA` varchar(255) DEFAULT NULL,
  `Mediums` varchar(255) DEFAULT NULL,
  `logbook` varchar(255) DEFAULT NULL,
  `IdNumber` varchar(255) DEFAULT NULL,
  `KRA_Doc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logbook`
--

LOCK TABLES `logbook` WRITE;
/*!40000 ALTER TABLE `logbook` DISABLE KEYS */;
INSERT INTO `logbook` VALUES (1,'','','','','','','','','','','','','','','','','','','','2020-12-13','on','','',''),(2,'','','','','','','','','','','','','','','','','','','','','','','',''),(3,'JG','G','G','G','Q','H','2','2020','','GGG','G','H','N','2','4','2','15900','','','2020-12-14','on','','',''),(4,'JG','','','','','','','','','','','','','','','','','','','2020-12-14','on','','',''),(5,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','','',''),(6,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','','',''),(7,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','','',''),(8,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','assets/logbooks/0712962787/logbook-_-0mpesa.PNG','assets/logbooks/0712962787/idnumber-_-1Capture4.PNG','assets/logbooks/0712962787/kra-_-2mpesa.PNG'),(9,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','assets/logbooks/0712962787/logbook-_-0Capture4.PNG','assets/logbooks/0712962787/idnumber-_-1mpesa.PNG','assets/logbooks/0712962787/kra-_-2mpesa.PNG'),(10,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','assets/logbooks/0712962787/logbook-_-0mpesa.PNG','assets/logbooks/0712962787/idnumber-_-1mpesa.PNG','assets/logbooks/0712962787/kra-_-2mpesa.PNG'),(11,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','assets/logbooks/0712962787/logbook-_-0mpesa.PNG','assets/logbooks/0712962787/idnumber-_-1mpesa.PNG','assets/logbooks/0712962787/kra-_-2mpesa.PNG'),(12,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','assets/logbooks/0712962787/logbook-_-0mpesa.PNG','assets/logbooks/0712962787/idnumber-_-1mpesa.PNG','assets/logbooks/0712962787/kra-_-2mpesa.PNG'),(13,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','455','RT','15900','','','GH','','assets/logbooks/+254722301062/logbook-_-0ABOUT US.pdf','assets/logbooks/+254722301062/idnumber-_-1ABOUT US.pdf',''),(14,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','455','RT','15900','','','GH','on','assets/logbooks/+254722301062/logbook-_-0ABOUT US.pdf','assets/logbooks/+254722301062/idnumber-_-1ABOUT US.pdf','assets/logbooks/+254722301062/kra-_-2ABOUT US.pdf'),(15,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','NAIROBI','RT','15900','','Kennedy Otieno Nyaga','GH','on','assets/logbooks/+254722301062/logbook-_-0ABOUT US.pdf','assets/logbooks/+254722301062/idnumber-_-1ABOUT US.pdf','assets/logbooks/+254722301062/kra-_-2ABOUT US.pdf'),(16,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','assets/logbooks/0712962787/logbook-_-0mpesa.PNG','assets/logbooks/0712962787/idnumber-_-1mpesa.PNG','assets/logbooks/0712962787/kra-_-2mpesa.PNG'),(17,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','assets/logbooks/0712962787/logbook-_-0mpesa.PNG','assets/logbooks/0712962787/idnumber-_-1mpesa.PNG','assets/logbooks/0712962787/kra-_-2mpesa.PNG'),(18,'KBB','5898','Voyage','Tesla','Tesla','Isuzu','300cc','1993','8988','Red','1993','98kg','6','9','9kg','Class 9','8','9999999','Erick Soi','300182157856','on','assets/logbooks/0712962787/logbook-_-0mpesa.PNG','assets/logbooks/0712962787/idnumber-_-1mpesa.PNG','assets/logbooks/0712962787/kra-_-2mpesa.PNG'),(19,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','10000','RT','15900','k','Kennedy Otieno Nyaga','GH','on','assets/logbooks/0722301062/logbook-_-0ABSA LIFE ASSURANCE.pdf','assets/logbooks/0722301062/idnumber-_-1ABSA LIFE ASSURANCE KENYA LTD.pdf','assets/logbooks/0722301062/kra-_-2ABSA LIFE ASSURANCE.pdf'),(20,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','1000','RT','15900','','','GH','on','assets/logbooks/+254722301062/logbook-_-0BRITAM LIFE ASSURANCE.pdf','assets/logbooks/+254722301062/idnumber-_-1BRITAM LIFE ASSURANCE.pdf','assets/logbooks/+254722301062/kra-_-2ABSA LIFE ASSURANCE.pdf'),(21,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','1000','RT','15900','','','GH','on','assets/logbooks/+254722301062/logbook-_-0BRITAM LIFE ASSURANCE.pdf','assets/logbooks/+254722301062/idnumber-_-1BRITAM LIFE ASSURANCE.pdf','assets/logbooks/+254722301062/kra-_-2ABSA LIFE ASSURANCE.pdf'),(22,'ffsfs','fssfs','trerer','rererr','rererer','rere','reerere','erere','fsfsfsf','fsfsf','gdgdgd','rerer','rerer','rerer','rerere','rerer','rere','rerere','ffdfdfd','g4t44rr','on','assets/logbooks/0712962787/logbook-_-0commercial Invoice.pdf','assets/logbooks/0712962787/idnumber-_-1commercial Invoice.pdf','assets/logbooks/0712962787/kra-_-2doc.pdf'),(23,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','NAIROBI','RT','15900','k','Kennedy Otieno Nyaga','GH','on','assets/logbooks/+254722301062/logbook-_-0WhatsApp Image 2020-12-21 at 6.46.19 PM.jpeg','assets/logbooks/+254722301062/idnumber-_-1WhatsApp Image 2020-12-21 at 6.46.23 PM (1).jpeg','assets/logbooks/+254722301062/kra-_-2WhatsApp Image 2020-12-21 at 6.46.13 PM (1).jpeg'),(24,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','NAIROBI','RT','15900','k','Kennedy Otieno Nyaga','GH','on','assets/logbooks/+254722301062/logbook-_-0ABSA LIFE ASSURANCE KENYA LTD.pdf','assets/logbooks/+254722301062/idnumber-_-1ABSA LIFE ASSURANCE KENYA LTD.pdf','assets/logbooks/+254722301062/kra-_-2ABSA LIFE ASSURANCE KENYA LTD.pdf'),(25,'E','Ry','R','R','R','R','R','R','T','T','T','T','T','Y','T','Y','R','','','R','on','assets/logbooks/0722301062/logbook-_-0IMG-20201224-WA0009.jpg','assets/logbooks/0722301062/idnumber-_-1IMG-20201224-WA0001.jpg','assets/logbooks/0722301062/kra-_-2IMG-20201224-WA0000.jpg'),(26,'ffsfs','fssfs','trerer','rererr','rererer','rere','reerere','erere','fsfsfsf','fsfsf','gdgdgd','rerer','rerer','rerer','rerere','rerer','rere','rerere','ffdfdfd','g4t44rr','on','assets/logbooks/0712962787/logbook-_-0doc_2.pdf','assets/logbooks/0712962787/idnumber-_-1doc_1.pdf','assets/logbooks/0712962787/kra-_-2doc_1.pdf'),(27,'rere','rere','rerer','rere','rerer','rerer','rere','rerer','rere','rerer','rerer','rerer','rerer','rerer','rere','rerer','rerer','rerer','rerer','rere','on','assets/logbooks/0712962787/logbook-_-0FIFA18_bug.jpg','assets/logbooks/0712962787/idnumber-_-1FIFA18_bug.jpg','assets/logbooks/0712962787/kra-_-2FIFA18_bug.jpg'),(28,'E','Ry','R','R','R','R','R','R','T','J','T','T','T','Y','T','Y','R','Ii','I','R','on','assets/logbooks/0722301062/logbook-_-0IMG-20201224-WA0003.jpg','assets/logbooks/0722301062/idnumber-_-1IMG-20201224-WA0000.jpg','assets/logbooks/0722301062/kra-_-2IMG-20201224-WA0000.jpg'),(29,'E','Ry','R','R','R','R','R','R','T','T','T','T','T','Y','T','Y','R','Ii','I','R','on','assets/logbooks/0722301062/logbook-_-0IMG-20201224-WA0000.jpg','assets/logbooks/0722301062/idnumber-_-1IMG-20201224-WA0001.jpg','assets/logbooks/0722301062/kra-_-2IMG-20201224-WA0001.jpg'),(30,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','NAIROBI','RT','15900','k','Kennedy Otieno Nyaga','GH','on','assets/logbooks/+254722301062/logbook-_-0Zoom_cm_fo42lnktZ9vvrZo4_mHG2hWir1yOatNo8R5atSzm4qsy9AYq0nI6Jh@XRV7hoqUbjmtteRW_k840cebe7f2ab5237_.exe','assets/logbooks/+254722301062/idnumber-_-1WhatsApp Image 2020-12-31 at 15.23.21.jpeg','assets/logbooks/+254722301062/kra-_-2WhatsApp Image 2020-12-31 at 15.23.21.jpeg'),(31,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','NAIROBI','RT','15900','','Kennedy Otieno Nyaga','GH','on','assets/logbooks/+254722301062/logbook-_-0COP.jpeg','assets/logbooks/+254722301062/idnumber-_-1COP.jpeg','assets/logbooks/+254722301062/kra-_-2COP.jpeg'),(32,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','NAIROBI','RT','15900','k','Kennedy Otieno Nyaga','12c','on','assets/logbooks/+254722301062/logbook-_-0AKPIA AKI.pdf','assets/logbooks/+254722301062/idnumber-_-1AKPIA AKI.pdf','assets/logbooks/+254722301062/kra-_-2AKPIA AKI.pdf'),(33,'JG','G','G','G','Q','H','2','2020','B','GGG','G','H','N','3','NAIROBI','RT','15900','','Kennedy Otieno Nyaga','hg5','on','assets/logbooks/+254722301062/logbook-_-0All Risks Insurance.pdf','assets/logbooks/+254722301062/idnumber-_-1All Risks Insurance.pdf','assets/logbooks/+254722301062/kra-_-2All Risks Insurance.pdf'),(34,'v','v','v','v',' v','b','v','v','v','v','vv','v','v','v','v','v','v',' v','v',' v','','assets/logbooks/0722301062/logbook-_-0Sti_Trace.log','assets/logbooks/0722301062/idnumber-_-1Sti_Trace.log','assets/logbooks/0722301062/kra-_-2Sti_Trace.log'),(35,'v','v','v','v',' v','b','v','v','v','v','vv','v','v','v','v','v','v',' v','v',' v','','assets/logbooks/0722301062/logbook-_-0Sti_Trace.log','assets/logbooks/0722301062/idnumber-_-1Sti_Trace.log','assets/logbooks/0722301062/kra-_-2Sti_Trace.log'),(36,'v','v','v','v',' v','b','v','v','v','v','vv','v','v','v','v','v','v',' v','v',' v','on','assets/logbooks/0722301062/logbook-_-0Sti_Trace.log','assets/logbooks/0722301062/idnumber-_-1Sti_Trace.log','assets/logbooks/0722301062/kra-_-2Sti_Trace.log'),(37,'eee','eee','eee','eee','eee','333','222','www','www','www','www','www','www','www','www','www','www','www','www','4343434','on','','',''),(38,'eee','eee','eee','eee','eee','333','222','www','www','www','www','www','www','www','www','www','www','www','www','4343434','','','',''),(39,'eee','eee','eee','eee','eee','333','222','www','www','www','www','www','www','www','www','www','www','www','www','4343434','','','',''),(40,'eee','eee','eee','eee','eee','333','222','www','www','www','www','www','www','www','www','www','www','www','www','4343434','on','','',''),(41,'eee','eee','eee','eee','eee','333','222','www','www','www','www','www','www','www','www','www','www','www','www','4343434','on','','',''),(42,'eee','eee','eee','eee','eee','333','222','www','www','www','www','www','www','www','www','www','www','www','www','4343434','on','','','');
/*!40000 ALTER TABLE `logbook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `role` varchar(255) DEFAULT NULL,
  `companyname` varchar(255) DEFAULT NULL,
  `contactperson` varchar(255) DEFAULT NULL,
  `krapin` varchar(255) DEFAULT NULL,
  `krapincopy` varchar(255) DEFAULT NULL,
  `emailaddress` varchar(255) DEFAULT NULL,
  `phonenumber` varchar(255) DEFAULT NULL,
  `physicaladdress` varchar(255) DEFAULT NULL,
  `idnumber` varchar(255) DEFAULT NULL,
  `idcopy` varchar(255) DEFAULT NULL,
  `iralicense` varchar(255) DEFAULT NULL,
  `iracopy` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-10 19:19:10
