<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../../index.php");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
    <?php
        require 'handle/db.php';
             
    ?>

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - SB Admin</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        <link href="css/animate.min.css" rel="stylesheet">
	    <link href="css/css/style.css" rel="stylesheet">
	    <link href="css/css/menu.css" rel="stylesheet">
	    <link href="css/css/icon_fonts/css/all_icons.min.css" rel="stylesheet">
        <style>
            #snackbar {
            visibility: hidden;
            min-width: 250px;
            margin-left: -125px;
            background-color: #333;
            color: #fff;
            text-align: center;
            border-radius: 2px;
            padding: 16px;
            position: fixed;
            z-index: 1;
            left: 50%;
            top: 100px;
            font-size: 17px;
            }

            #snackbar.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
            }

            @-webkit-keyframes fadein {
            from {bottom: 0; opacity: 0;} 
            to {bottom: 100px; opacity: 1;}
            }

            @keyframes fadein {
            from {top: 0; opacity: 0;}
            to {top: 100px; opacity: 1;}
            }

            @-webkit-keyframes fadeout {
            from {top: 100px; opacity: 1;} 
            to {top: 0; opacity: 0;}
            }

            @keyframes fadeout {
            from {top: 100px; opacity: 1;}
            to {top: 0; opacity: 0;}
            }
        </style>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="index.php">Bima Plus Dashboard</a>
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <a class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="../../index.php"><i class="fas fa-home"></i></a>
             
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="wallet.php">My Wallet</a>
                        <a class="dropdown-item" href="#">Activity Log</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="logout.php">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                            <a class="nav-link" href="index.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <div class="sb-sidenav-menu-heading">Interface</div>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Layouts
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="layout-static.html">Static Navigation</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">Light Sidenav</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Pages
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                                        Authentication
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="login.html">Login</a>
                                            <a class="nav-link" href="register.html">Register</a>
                                            <a class="nav-link" href="password.html">Forgot Password</a>
                                        </nav>
                                    </div>
                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                                        Error
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="401.html">401 Page</a>
                                            <a class="nav-link" href="404.html">404 Page</a>
                                            <a class="nav-link" href="500.html">500 Page</a>
                                        </nav>
                                    </div>
                                </nav>
                            </div>
                            <div class="sb-sidenav-menu-heading">Addons</div>
                            <a class="nav-link" href="charts.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                
                                Charts
                                
                            </a>
                            <a class="nav-link" href="tables.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Tables
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        <?echo $_SESSION["username"]?>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container" id = "home">
                        <h1 class="mt-4">Dashboard</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">View</li>
                        </ol>
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body">Products</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#products">View Products</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body">Agents</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#agents">View Agents</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                    <div class="card-body">Underwirters</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#underwirters">View Underwirters</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body">Clients</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#clients">View Clients</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Add</li>
                        </ol>
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body">Products</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" data-toggle="modal" data-target="#productmodal">Add Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body">Agents</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" data-toggle="modal" data-target="#agentmodal">Add Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                    <div class="card-body">Underwriters</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" data-toggle="modal" data-target="#underwritermodal">Add Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body">Clients</div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" data-toggle="modal" data-target="#clientmodal">Add Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

<!--Underwriter Model-->
                    <div class="modal fade" id="underwritermodal" tabindex="-1" role="dialog" aria-labelledby="underwriterModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <form action="handle/underwriter.php" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                            <h5 class="modal-title" id="underwriterModalLabel">New Underwriter</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                           
                            
                            <label for="Product" class="control-label">Choose Underwriter</label>
                                <select class="browser-default custom-select" id="underwriter" name="underwriter">
                                <?php
                                        
                                        $sql4 = 'SELECT Name FROM UnderwriterList';
                                        $result = mysqli_query($conn, $sql4);
                                        if(mysqli_num_rows($result) > 0){
                                            
                                            while($row = mysqli_fetch_array($result)){
                                                echo '<option>'.$row["Name"].'</option>';
                                            }
                                            echo "</optgroup>";
                                        }
                                        
                                    ?>
                                                    
                                                    
                                </select><br><br>
                            <input class="form-control form-control-lg" name="legal_entity" type="text" placeholder="Legal entity" required><br>
                            <input class="form-control form-control-lg" name="address" type="text" placeholder="Address" required><br>
                            <input class="form-control form-control-lg" name="email" type="text" placeholder="Email adaress" required><br>
                            <input class="form-control form-control-lg" name="website" type="text" placeholder="Website"><br>
                            <label for="Logo" class="control-label">Upload Logo</label>
                            <input class="form-control form-control-lg" name="logo" type="file" placeholder="Logo" required><br>
                            <input class="form-control form-control-lg" name="location" type="text" placeholder="Location optional" required><br>
                            <input class="form-control form-control-lg" name="account" type="text" placeholder="Bank Account" required><br>
                            
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                        </div>
                        </div>
                    </div>

<!--End of Underwriter Model-->
<!--Agent Model-->
                    <div class="modal fade" id="agentmodal" tabindex="-1" role="dialog" aria-labelledby="agentModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <form action="handle/agent.php" method="post" enctype="multipart/form-data"> 
                            <div class="modal-header">
                            <h5 class="modal-title" id="agentModalLabel">New Agent</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                            <form enctype="multipart/form-data">
                            <input class="form-control form-control-lg" name="contact" type="text" placeholder="Contact Person"><br>
                            <input class="form-control form-control-lg" name="company" type="text" placeholder="Company Name/Individual full names"><br>
                            <input class="form-control form-control-lg" name="reg" type="text" placeholder="ID/PP/REG. Cert. for company/businessr"><br>
                            <input class="form-control form-control-lg" name="kra" type="text" placeholder="KRA Pin number"><br>
                            <label for="profile" class="control-label">Upload KRA COPY</label>
                            <input class="form-control form-control-lg" name="krafile" type="file" placeholder="krafile" required ><br>              
                            <input class="form-control form-control-lg" name="adress" type="text" placeholder="Physical Address"><br>
                            <input class="form-control form-control-lg" name="email" type="text" placeholder="Email Address"><br>
                            <input class="form-control form-control-lg" name="phone" type="text" placeholder="Phone number"><br>
                            <input class="form-control form-control-lg" name="tax" type="text" placeholder="Withholding Tax (%)"><br>

                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                        </div>
                        </div>
                    </div>
<!--End of Agent Model-->

<!--Product Model-->
                        <div class="modal fade" id="productmodal" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <form action="handle/product.php" method="post"  enctype="multipart/form-data"> 

                                <div class="modal-header">
                                <h5 class="modal-title" id="productModalLabel">New Product</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                <form  enctype="multipart/form-data">
                                <label for="Product" class="control-label">Choose a Product</label>
                                <select class="browser-default custom-select" id="prodname" name="prodname">
                                                    <?php
                                                            $sql4 = 'select ProductName from ProductList';
                                                            $result = mysqli_query($conn, $sql4);
                                                            if(mysqli_num_rows($result) > 0){
                                                                
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'.$row["ProductName"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            
                                                        ?>
                                                    
                                                    
                                </select><br><br>
                                <label for="risc" class="control-label">Choose Risk to be covered</label>
                                <select class="browser-default custom-select" id="risk" onchange="myFunction()" name="risk">
                                                    <?php
                                                            $sql1 = 'select * from VehicleClass where type = "MOTORCYCLE"';
                                                            $result = mysqli_query($conn, $sql1);
                                                            
                                                            if(mysqli_num_rows($result) > 0){
                                                                echo '<optgroup label="1. MOTORCYCLE">';
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            $sql2 = 'select * from VehicleClass where type = "TRICYCLE"';
                                                            $result = mysqli_query($conn, $sql2);
                                                            
                                                            if(mysqli_num_rows($result) > 0){
                                                                echo '<optgroup label="2. TRICYCLE">';
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            $sql3 = 'select * from VehicleClass where type = "MOTORVEHICLE"';
                                                            $result = mysqli_query($conn, $sql3);
                                                            if(mysqli_num_rows($result) > 0){
                                                                echo '<optgroup label="3. MOTORVEHICLE">';
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'. $row["ID"].".   ".$row["class"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            } 
                                                        ?>
                                </select><br><br>
                                <label for="Product" class="control-label">Choose Underwriter</label>
                                <select class="browser-default custom-select" id="underwriter" name="underwriter">
                                                    <?php
                                                            $sql4 = 'select Name from UnderwriterList;';
                                                            $result = mysqli_query($conn, $sql4);
                                                            if(mysqli_num_rows($result) > 0){
                                                                
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'.$row["Name"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            
                                                        ?>
                                                    
                                                    
                                </select><br><br>
                                <div class="custom-file">
                                <input class="form-control form-control-lg custom-file-input" name="prodlogo" type="file" required><br>
                                <label class="custom-file-label" for="customFile">Upload Underwriter Logo</label>
                                </div>
                                <label for="Product" class="control-label">Choose Coverage</label>
                                <select class="browser-default custom-select" id="coverage" onchange="myFunction()" name="coverage">
                                                    <?php
                                                            $sql4 = 'select cover from Coverage';
                                                            $result = mysqli_query($conn, $sql4);
                                                            if(mysqli_num_rows($result) > 0){
                                                                
                                                                while($row = mysqli_fetch_array($result)){
                                                                    echo '<option>'.$row["cover"].'</option>';
                                                                }
                                                                echo "</optgroup>";
                                                            }
                                                            
                                                        ?>
                                                    
                                                    
                                </select><br><br>
                                                <div id="suminsured"></div>
                                                <div id="suminsured1"></div>
                                                <div id="suminsured2"></div>
                                                <div id="suminsured3"></div>
                                                <br><br>
                                                <div id="tonnage"></div>
                                                <div id="tonnage1"></div>
                                                
                                                <script>
                                                    function myFunction() {
                                                        var x = document.getElementById("coverage");
                                                        var i = x.selectedIndex;
                                                        console.log(i); 
                                                        if (i == 2 ) {
                                                            document.getElementById("suminsured").innerHTML ='<label for="profile" class="control-label">Enter Amount Insured</label>';

                                                            document.getElementById("suminsured1").innerHTML = '<input class="form-control form-control-lg" name="minimum_premium" type="number" placeholder="Minimum Premium Insured" required><br>';
                                                            document.getElementById("suminsured2").innerHTML = '<label for="profile" class="control-label">Enter Year of Manufacture</label>';
                                                            document.getElementById("suminsured3").innerHTML = '<select name="selectyear" class="form-control form-control-lg"> <?php for ($i = date('Y'); $i >= 1950; $i--){echo "<option>$i</option>"; }?></select><br>'
                                                    
                                                        }
                                                        
                                                        if (i == 0 || i == 1) {
                                                            document.getElementById("suminsured").innerHTML = '';
                                                            document.getElementById("suminsured1").innerHTML = '';
                                                            document.getElementById("suminsured2").innerHTML = '';
                                                            document.getElementById("suminsured3").innerHTML = '';
                                                            
                                                            
                                                        }
                                                        if (i == 0 || i == 2){
                                                            document.getElementById("tonnage").innerHTML = '';
                                                            document.getElementById("tonnage1").innerHTML = '';
                                                        }

                                                
                                                
                                                        var z = document.getElementById("risk");
                                                        var y = z.selectedIndex;
                                                        console.log(y);
                                                        if ((y == 5 || y == 6 || y == 2)&&(i == 1)) {
                                                            document.getElementById("tonnage").innerHTML ='<label for="profile" class="control-label">Enter Tonnage</label>';

                                                            document.getElementById("tonnage1").innerHTML = '<input class="form-control form-control-lg" name="tonnage" type="number" placeholder="Tonnage" required><br>';
                                                        }else{
                                                                document.getElementById("tonnage").innerHTML =" ";
                                                                document.getElementById("tonnage1").innerHTML = " ";
                                                        }
                                                    }

                                                </script>


                                <textarea class="form-control form-control-lg" name="policybenefits" type="textarea"  placeholder="Policy Limits And Benefits" rows="6"></textarea><br>
                                
                                <label for="Str">Enter Short Term Rates</label>
                                    
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">1 Month</th>
                                        <th scope="col">2 Months</th>
                                        <th scope="col">3 Months</th>
                                        <th scope="col">4 Months</th>
                                        <th scope="col">5 Months</th>
                                        <th scope="col">12 Months</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th> <input class="form-control form-control-sm" name="shorttermrates" type="number" step="any" placeholder="STR"><br></th>
                                        <th> <input class="form-control form-control-sm" name="shorttermrates1" type="number" step="any" placeholder="STR"><br></th>
                                        <th> <input class="form-control form-control-sm" name="shorttermrates2" type="number" step="any" placeholder="STR"><br></th>
                                        <th> <input class="form-control form-control-sm" name="shorttermrates3" type="number" step="any" placeholder="STR"><br></th>
                                        <th> <input class="form-control form-control-sm" name="shorttermrates4" type="number" step="any" placeholder="STR"><br></th>
                                        <th> <input class="form-control form-control-sm" name="shorttermrates5" type="number" step="any" placeholder="STR"><br></th>
                                    </tr>
                                    
                                    </tbody>
                                    ffdfdfdf
                                </table><br>
                                <input class="form-control form-control-lg" name="anualrates" type="number" step="any" placeholder="Annual rates" required><br>
                                <input class="form-control form-control-lg" name="clause" type="text" placeholder="Clauses" required><br>
                                <input class="form-control form-control-lg" name="conditions" type="text" placeholder="Conditions and waranties" required><br>
                                <p>
                                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Optional Benefits
                                    </a>

                                </p>
                                <div class="collapse" id="collapseExample">
                                    <div class="card card-body">
                                <input class="form-control form-control-lg" name="optionalbenefit0" type="text" placeholder="EG Access protector"><br>
                                <input class="form-control form-control-lg" name="optionalbenefit1" type="number" step="any" placeholder="Optional BeneMinimum Premium"><br>
                                <input class="form-control form-control-lg" name="optionalbenefit2" type="number" step="any" placeholder="Optional Benefits Rate(%)"><br>

                                    </div>
                                </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                                </form>

                            </div>
                            </div>
                        </div>
<!--End of Product Model-->
<!--Client Model-->
                        <div class="modal fade" id="clientmodal" tabindex="-1" role="dialog" aria-labelledby="clientModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="handle/client.php" method="post" enctype="multipart/form-data"> 
                                <div class="modal-header">
                                <h5 class="modal-title" id="agentModalLabel">New Client</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                <form enctype="multipart/form-data">
                                <input class="form-control form-control-lg" name="fname" type="text" placeholder="First Name/business name"><br>
                                <input class="form-control form-control-lg" name="mname" type="text" placeholder="Middle Names/company /business name"><br>
                                <input class="form-control form-control-lg" name="lname" type="text" placeholder="Last Name/company /business name"><br>
                                <input class="form-control form-control-lg" name="reg" type="text" placeholder="ID/PP/REG. Cert. for Companies/business"><br>
                                <label for="profile" class="control-label">Upload ID/PP/REG. Cert. for Companies/business</label>
                                <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="ID/PP/REG. Cert. for Companies/business"><br>
                                <input class="form-control form-control-lg" name="kra" type="text" placeholder="KRA PIN"><br>
                                <label for="profile" class="control-label">Uload KRA PIN</label>
                                <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="KRA PIN"><br>
                                <input class="form-control form-control-lg" name="email" type="text" placeholder="EMAIL"><br>
                                <input class="form-control form-control-lg" name="phone" type="text" placeholder="PHONE"><br>
                                <input class="form-control form-control-lg" name="occupation" type="text" placeholder="OCCUPATION"><br>
                                <input class="form-control form-control-lg" name="residence" type="text" placeholder="RESIDENCE"><br>
                                <label for="profile" class="control-label">Upload profile picture(optional)</label>
                                <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="PROFILE PHOTO/LOGO"><br>
                                <label for="logbook" class="control-label">Upload Logbook (optional)</label>
                                <input class="form-control form-control-lg" name="clientFiles[]" type="file" placeholder="VEHICLE DETAILS .LOGBOOK"><br>

                                
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                            </div>
                            </div>
                        </div>
<!--End of Client Model-->

                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-area mr-1"></i>
                                        Area Chart Example
                                    </div>
                                    <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar mr-1"></i>
                                        Bar Chart Example
                                    </div>
                                    <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1" id="underwirters"></i>
                                <a href="#home">Underwriters</a>
                            </div>
                            <div class="card-body">dsds
                                <div class="row">
                                    <?php		
                                        $record_per_page = 6;
                                        $page = '';
                                        if(isset($_GET["page"])){
                                            $page = $_GET["page"];
                                            $search = @$_GET['page']; 
                                        }else {
                                            $page = 1;
                                        }

                                        $start_from = ($page-1)*$record_per_page;			
                                        $UnderwriterQuery  = "SELECT * FROM `UnderwriterList` WHERE NOT (Name LIKE '%life%' or Name LIKE '%Pioneer Assurance Company Limited%' or Name LIKE '%Health%') ORDER BY Name LIMIT $start_from, $record_per_page";
                                        $UnderwriterResult  = mysqli_query($conn, $UnderwriterQuery);
                                        while($UnderwriterRow = mysqli_fetch_assoc($UnderwriterResult)){
                                        ?>
                                        <div class="col-md-4 col-sm-6 wow fadeIn animated" data-wow-delay="0.2s">
                                            <div class="img_wrapper">
                                                <div class="img_container" style="height:233px;">
                                                    <a>
                                                        <img src="<?echo $UnderwriterRow['path']?>" width="100%" height="100px" class="img-responsive" alt="">
                                                        <div class="short_info">
                                                            <h3><?echo $UnderwriterRow["Name"]?></h3>
                                                            <em>Comprehensive &amp; Third-party</em>
                                                            <p>
                                                                <?echo $UnderwriterRow["description"]?>
                                                            </p>
                                                                <form action="underwriter.php" method="post">
                                                                    <input type="hidden" name="description" value="<?echo $UnderwriterRow['description']?>">
                                                                    <input type="hidden" name="underwriter" value="<?echo $UnderwriterRow['Name']?>">
                                                                    <button type="submit" class="btn btn-lg btn-huge btn-danger">View Products</button>
                                                                </form>

                                                            <div class="score_wp">
                                                                <div class="score">7.5</div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- End img_wrapper -->
                                        </div>
                                        <?}?> 


                                    </div>

                                    <nav class="pagination-wrapper">
                                        <ul class="pagination">
                                            <?
                                                $page_query = "SELECT * FROM `UnderwriterList` WHERE NOT (Name LIKE '%life%' or Name LIKE '%Pioneer Assurance Company Limited%' or Name LIKE '%Health%') ORDER BY Name DESC";
                                                $page_result = mysqli_query($conn, $page_query);
                                                $total_records = mysqli_num_rows($page_result);
                                                $total_pages = ceil($total_records/$record_per_page);
                                                $start_loop = $page;
                                                $difference = $total_pages - $page;
                                            
                                                if($difference <= 5){
                                                    $start_loop = $total_pages - 5;
                                                }
                                                #$active = "active";
                                                $end_loop = $start_loop + 4;
                                                if($page > 1){
                                                    
                                                    echo "<li><a href='?page=1#underwriters'>First</a></li>";
                                                    echo "<li><a href='?page=".($page - 1)."#underwriters'><<</a></li>";
                                                }
                                                for($i=$start_loop; $i<=$end_loop; $i++){
                                                    $active = $i == $page ? 'class="active"' : '';     
                                                    echo "<li $active><a href='?page=".$i."#underwriters'>".$i."</a><li>";
                                                }
                                                if($page <= $end_loop){
                                                    echo "<li><a href='?page=".($page + 1)."#underwriters'>>></a></li>";
                                                    echo "<li><a href='?page=".$total_pages."#underwriters'>Last</a></li>";
                                                }
                                            ?>
                                        </ul>
                                    </nav>
                                </div>
                            





                                <!--<div class="count-down-area pt-25 section-bg" data-background="assets/img/gallery/section_bg02.png">
                                    <div class="container" id="underwriters">
                                        <div class="row justify-content-center" >
                                            <div class="col-lg-12 col-md-12">
                                                <div class="count-down-wrapper" >
                                                    <div class="row justify-content-between">
                                                    <?php
                                                            require_once('handle/db.php');
                                                            $perpage = 12;
                                                            if(isset($_GET['page']) & !empty($_GET['page'])){
                                                                $curpage = $_GET['page'];
                                                            }else{
                                                                $curpage = 1;
                                                            }
                                                            $start = ($curpage * $perpage) - $perpage;
                                                            $PageSql = "SELECT * FROM `UnderwriterList` limit 53";
                                                            $pageres = mysqli_query($conn, $PageSql);
                                                            $totalres = mysqli_num_rows($pageres);
                                                            
                                                            $endpage = ceil($totalres/$perpage);
                                                            $startpage = 1;
                                                            $nextpage = $curpage + 1;
                                                            $previouspage = $curpage - 1;
                                                            
                                                            $ReadSql = "SELECT * FROM `UnderwriterList` WHERE NOT (Name LIKE '%life%' or Name LIKE '%Pioneer Assurance Company Limited%' or Name LIKE '%Health%') ORDER BY Name LIMIT $start, $perpage";
                                                            
                                                            $res = mysqli_query($conn, $ReadSql);
                                                            while($r = mysqli_fetch_assoc($res)){
                                                                $uuu = "test";
                                                            ?>  
                                                                <div class="col-lg-3 col-md-6 col-sm-6" style="padding-bottom:20px;">
                                                                    <div class="card w3-hover-shadow w3-center">
                                                                        <img src="<?echo "../../" . $r['path']?>" alt="Avatar" style="width:100%; height:100px;">
                                                                            <div class="containercard">
                                                                                <h4><b><?echo $r["Name"]?></b></h4>
                                                                            
                                                                            </div>
                                                                            <form action="../../dist/prodcategory.php" method="post">
                                                                                <input id="underwriterid" name="underwriterid" type="hidden" value="<?echo $r['Name']?>">
                                                                                <h3><input type="submit" class="btn btn-outline-info btn-lg btn-block" value="View Products"></h3>
                                                                                
                                                                            </form>
                                                                        
                                                                    </div>   
                                                                                                         
                                                                </div>
                                                                <div class="modal fade" id="<? echo $uuu?>" tabindex="-1" role="dialog" aria-labelledby="underwriterModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">dsds</div>
                                                                    <div class="modal-content lgh1">Prod</div>
                                                                </div>
                                                            <?}?> 
                                                                                 
                                                    </div>
                                                    <nav aria-label="Page navigation" style="position:relative; left:0x;">
                                                                <ul class="pagination">
                                                                <?php if($curpage != $startpage){ ?>
                                                                    <li class="page-item">
                                                                        <a class="page-link" href="?page=<?php echo $startpage ?>#products" tabindex="-1" aria-label="Previous">
                                                                            <span aria-hidden="true">&laquo;</span>
                                                                            <span class="sr-only">First</span>
                                                                        </a>
                                                                    </li>
                                                                <?php } ?>
                                                                <?php if($curpage >= 2){ ?>
                                                                    <li class="page-item"><a class="page-link" href="?page=<?php echo $previouspage ?>#products"><?php echo $previouspage ?></a></li>
                                                                <?php } ?>
                                                                    <li class="page-item active"><a class="page-link" href="?page=<?php echo $curpage ?>#products"><?php echo $curpage ?></a></li>
                                                                <?php if($curpage != $endpage){ ?>
                                                                    <li class="page-item"><a class="page-link" href="?page=<?php echo $nextpage ?>#products"><?php echo $nextpage ?></a></li>
                                                                    <li class="page-item">
                                                                        <a class="page-link" href="?page=<?php echo $endpage ?>#products" aria-label="Next">
                                                                            <span aria-hidden="true">&raquo;</span>
                                                                            <span class="sr-only">Last</span>
                                                                        </a>
                                                                    </li>
                                                                <?php } ?>
                                                                </ul>
                                                            </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->

                                <!---
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Risk</th>
                                                <th>Underwriter</th>
                                                <th>Coverage</th>
                                                <th>Rates</th>
                                                <th>Benefits</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Risk</th>
                                                <th>Underwriter</th>
                                                <th>Coverage</th>
                                                <th>Rates</th>
                                                <th>Year</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            
                                            <?php 
                                                $product_sql = "SELECT * FROM Product";
                                                    if($result = mysqli_query($conn, $product_sql)){
                                                        if(mysqli_num_rows($result) > 0){
                                                            while($row = mysqli_fetch_array($result)){
                                            ?>
                                                                <tr>
                                                                    <td><?php echo $row['PRODUCT_NAME']?></td>
                                                                    <td><?php echo $row['RISK_COVERED']?></td>
                                                                    <td><?php echo $row['UNDERWRITER']?></td>
                                                                    <td><?php echo $row['COVERAGE']?></td>
                                                                    <td><?php echo $row['SHORTTERM_RATES']?></td>
                                                                    <td><?php echo $row['YEAR_ACCEPTED']?></td>
                                                                </tr>
                                                            <?php 
                                                        }
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                --->
                        </div>

                        
                        <div class="card mb-4" id="products">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                <a href="#home">Products</a>
                            </div>
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Legal Entity</th>
                                                <th>Coverage</th>
                                                <th>Email</th>
                                                <th>Vehicle Type</th>
                                                <th>RISK_COVERED</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Legal Entity</th>
                                                <th>Coverage</th>
                                                <th>Email</th>
                                                <th>Vehicle Type</th>
                                                <th>RISK_COVERED</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <?php 
                                                $product_sql = "SELECT * FROM Product";
                                                    if($result = mysqli_query($conn, $product_sql)){
                                                        if(mysqli_num_rows($result) > 0){
                                                            while($row = mysqli_fetch_array($result)){
                                                                $underwriter =  $row["UNDERWRITER"];  
                                                                $email_sql = "SELECT EMAIL_ADDRESS FROM UnderwriterList WHERE Name LIKE '%$underwriter%'";
                                                                $email_result = mysqli_query($conn, $email_sql);
                                                                $email_row = mysqli_fetch_array($email_result, MYSQLI_NUM);
                                                                $email_address = $email_row[0];
                                                                ?>
                                                                <tr style="cursor: zoom-in;" class="btn-primary" data-toggle="modal" data-target=".modal<?echo $row['ID']?>">
                                                                    <td><?php echo $row['PRODUCT_NAME']?></td>
                                                                    <td><?php echo $row['UNDERWRITER']?></td>
                                                                    <td><?php echo $row['COVERAGE']?></td>
                                                                    <td><?php echo $email_address?></td>
                                                                    <td><?php echo $row['VEHICLE_TYPE']?></td>
                                                                    <td><?php echo $row['RISK_COVERED']?></td>
                                                                </tr>
                                                                
                                                                <div class="modal fade modal<?echo $row['ID']?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content">
                                                                            <div class="row">
                                                                                <div class="col6">
                                                                                    <img src="../dist/assets/bimaplus_logo/bimaplus.png" height="200" width="200" class="rounded" alt="bimaplus_logo">
                                                                                </div>
                                                                                
                                                                                <div class="col container">
                                                                                   <?echo $row["PRODUCT_NAME"]?>
                                                                                    <div id="">
                                                                                        <div id="">
                                                                                            <main>
                                                                                                <div class="">
                                                                                                    <div class="">
                                                                                                        <div class="">
                                                                                                            <div class="card shadow-lg border-0 rounded-lg mt-5">
                                                                                                                <div class="container">
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                       Risk Covered:
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                       <?echo $row["RISK_COVERED"]?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                        Under Writer:
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                       <?echo $row["UNDERWRITER"]?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                        Coverrage:
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                       <?echo $row["COVERAGE"]?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                        Policy Limits And Benefits:
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                       <?echo $row["OPTIONAL_ADDITIONAL_BENEFITS"]?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                        Annual Rates:
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                       <?echo $row["ANNUAL_RATES"]?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                        Clauses:
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                       <?echo $row["CLAUSES"]?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                        Conditions and Waranties:
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                       <?echo $row["CONDITIONS_AND_WARRANTIES"]?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                        Year Accepted:
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                       <?echo $row["YEAR_ACCEPTED"]?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                        Tonnage:
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                       <?echo $row["TONNAGE"]?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                       Minimum Premium:
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                       <?echo $row["MINIMUM_PREMIUM"]?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-6">
                                                                                                                        Email:
                                                                                                                        
                                                                                                                        </div>
                                                                                                                        <div class="col">
                                                                                                                            <? echo $email_address;?>
                                                                                                                            <br>
                                                                                                                                
                                                                                                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                                                  
                                                                                                                                Edit Email
                                                                                                                            </a>
                                                                                                                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                                                                                                <form action="handle/edit_email.php" method="post" >
                                                                                                                                    <fieldset style="width:200px">
                                                                                                                                        <input type="text" id="email"  name="email" class="form-control" placeholder="New email value">
                                                                                                                                        <input type="hidden" id="underwriter_name"  name="underwriter_name" value="<?echo $underwriter?>"  class="form-control">
                                                                                                                                        <input type="hidden"  id="prid_identifier" name="prid_identifier" value="<?echo $row["PRODUCT_IDENTIFIER"]?>" class="form-control">
                                                                                                                                        <button type="submit" class="btn btn-primary">Submit</button>

                                                                                                                                    </fieldset>
                                                                                                                                </form>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="nav-item dropdown" >
                                                                                                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                                                            Add Restricted Vehicles
                                                                                                                        </a>
                                                                                                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                                                                                            <form action="handle/edit_vehicle.php" method="post" >
                                                                                                                                <fieldset style="width:200px">
                                                                                                                                    <input type="text" id="restricted"  name="restricted" class="form-control" placeholder="Enter restricted vehicle">
                                                                                                                                    <input type="hidden" id="underwriter_name"  name="underwriter_name" value="<?echo $underwriter?>"  class="form-control">
                                                                                                                                    <input type="hidden"  id="prid_identifier" name="prid_identifier" value="<?echo $row["PRODUCT_IDENTIFIER"]?>" class="form-control">
                                                                                                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                                                                                                </fieldset>
                                                                                                                            </form>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    Vehicle
                                                                                                                    <div class="container">
                                                                                                                        <div class="row">
                                                                                                                                
                                                                                                                            <div class="col-8">Underwriter</div>
                                                                                                                            <div class="col">Vehicle</div>
                                                                                                                            <?php
                                                                                                                                $restricted_vehicle_sql = 'SELECT * FROM Restricted_Vehicles WHERE PRODUCT_IDENTIFIER = "'.$row['PRODUCT_IDENTIFIER'].'"'; 
                                                                                                                                if($result1 = mysqli_query($conn, $restricted_vehicle_sql)){
                                                                                                                                    if(mysqli_num_rows($result1) > 0){
                                                                                                                                        while($vehicle_row = mysqli_fetch_array($result1)){
                                                                                                                                            ?>
                                                                                                                                                <div class="w-100"></div>
                                                                                                                                                <div class="col-8"><?echo $underwriter?></div>
                                                                                                                                                <div class="col"><?echo $vehicle_row["Vehicle"]?></div>
                                                                                                                                             <?
                                                                                                                                            }
                                                                                                                                            ?>   
                                                                                                                                        <?
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            ?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    <div class="nav-item dropdown" >
                                                                                                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                                                            Add Optional Benefits
                                                                                                                        </a>
                                                                                                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                                                                                        <form action="handle/product_benefits.php" method="post" >
                                                                                                                        <fieldset style="width:200px">
                                                                                                                            <div class="form-group">
                                                                                                                            <input type="text" id=""  name="prodopt1" class="form-control" placeholder="EG Access protector">
                                                                                                                            </div>
                                                                                                                            <div class="form-group">
                                                                                                                            <input type="number" step="any" id=""  name="prodopt2" class="form-control" placeholder="Optional BeneMinimum Premium">
                                                                                                                            </div>
                                                                                                                            <div class="form-group">
                                                                                                                            <input type="number" step="any" id="" name="prodopt3"  class="form-control" placeholder="Optional Benefits Rate(%)">
                                                                                                                            </div>
                                                                                                                            <div class="form-group">
                                                                                                                            <input readonly type="text"  id="" name="prodopt4" value="<?echo $row["PRODUCT_IDENTIFIER"]?>" class="form-control">
                                                                                                                            </div>
                                                                                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                                                                                        </fieldset>
                                                                                                                        </form>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <hr>
                                                                                                                    Benefit
                                                                                                                    <div class="container">
                                                                                                                        <div class="row">
                                                                                                                                
                                                                                                                            <div class="col">Name</div>
                                                                                                                            <div class="col">Benefits Rate</div>
                                                                                                                            <div class="col">Min Premium</div>
                                                                                                                            <?php
                                                                                                                                $additionalsql = 'SELECT * FROM Product_additional_benefits WHERE PRODUCT_IDENTIFIER = "'.$row['PRODUCT_IDENTIFIER'].'"'; 
                                                                                                                                $OPTIONAL_ADDITIONAL_BENEFITS = [];
                                                                                                                                $OP_BR = [];
                                                                                                                                $OP_BMP = []; 
                                                                                                                                if($result1 = mysqli_query($conn, $additionalsql)){
                                                                                                                                    if(mysqli_num_rows($result1) > 0){
                                                                                                                                        while($row1 = mysqli_fetch_array($result1)){
                                                                                                                                            array_push($OPTIONAL_ADDITIONAL_BENEFITS, $row1["OPTIONAL_ADDITIONAL_BENEFITS"]);
                                                                                                                                            array_push($OP_BR, $row1["OP_BR"]);
                                                                                                                                            array_push($OP_BMP, $row1["OP_BMP"]);   
                                                                                                                                            } 
                                                                                                                                            $combined = array_map(null, $OPTIONAL_ADDITIONAL_BENEFITS, $OP_BR, $OP_BMP);
                                                                                                                                                
                                                                                                                                            
                                                                                                                                            
                                                                                                                                            foreach ($combined as $val){?>
                                                                                                                                                <div class="w-100"></div>
                                                                                                                                                <div class="col"><?echo $val[0]?></div>
                                                                                                                                                <div class="col"><?echo $val[1]?></div>
                                                                                                                                                <div class="col"><?echo $val[2]?></div>
                                                                                                                                                <?
                                                                                                                                            }
                                                                                                                                            ?>   
                                                                                                                                        <?
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            ?>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>                                                       
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </main>
                                                                                        </div>
                                                                                        <div id="layoutAuthentication_footer">
                                                                                            <footer class="py-4 bg-light mt-auto">
                                                                                                        <button type="button" class="btn btn-secondary btn-lg btn-block" data-dismiss="modal">Close</button>
                                                                                            </footer>
                                                                                        </div>
                                                                                    </div>


                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php 
                                                        }
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1" id="agents"></i>
                                <a href="#home">Agents</a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Contact Person</th>
                                                <th>Krs Pin</th>
                                                <th>Adress</th>
                                                <th>Phone Number</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>User Name</th>
                                                <th>Contact Person</th>
                                                <th>Krs Pin</th>
                                                <th>Adress</th>
                                                <th>Phone Number</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                                $product_sql = "SELECT * FROM users";
                                                    if($result = mysqli_query($conn, $product_sql)){
                                                        if(mysqli_num_rows($result) > 0){
                                                            while($row = mysqli_fetch_array($result)){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $row['username']?></td>
                                                                    <td><?php echo $row['contactperson']?></td>
                                                                    <td><?php echo $row['krapin']?></td>
                                                                    <td><?php echo $row['physicaladdress']?></td>
                                                                    <td><?php echo $row['phonenumber']?></td>
                                                                    <td><?php echo $row['emailaddress']?></td>
                                                                    <td><?php echo $row['role']?></td>
                                                                    <td><button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target=#modal<?echo $row['id']?>>Edit</button></td>
                                                                   
                                                                   
                                                                </tr>

                                                                <div class="modal fade" id=modal<?echo $row['id']?> tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <form action="handle/edit_agent.php" method="post" >
                                                                                <div class="modal-header">
                                                                                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                
        
                                                                                    <div class="form-group">
                                                                                        <label for="exampleFormControlSelect1">Change User</label>
                                                                                        <select class="form-control" name = "update_agent" id="exampleFormControlSelect1">
                                                                                            <option>Admin</option>
                                                                                            <option>Agent</option>
                                                                                            <option>Agent-Admin</option>                                                        
                                                                                        </select>
                                                                                    </div> 
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                                    <input type="hidden"  id="" name="agent_id" value="<?echo $row["id"]?>" class="form-control">
                                                                                    <input type="hidden"  id="" name="username" value="<?echo $row["username"]?>" class="form-control">
                                                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                                                </div>                                                                            
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php 
                                                        }
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                            
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1" id="clients"></i>
                                <a href="#home">Clients</a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>KRA Pin</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Residence</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>KRA Pin</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Residence</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php 
                                                $product_sql = "SELECT * FROM Client";
                                                    if($result = mysqli_query($conn, $product_sql)){
                                                        if(mysqli_num_rows($result) > 0){
                                                            while($row = mysqli_fetch_array($result)){
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $row['Fname']?></td>
                                                                    <td><?php echo $row['Lname']?></td>
                                                                    <td><?php echo $row['KRA_PIN']?></td>
                                                                    <td><?php echo $row['EMAIL']?></td>
                                                                    <td><?php echo $row['PHONE']?></td>
                                                                    <td><?php echo $row['RESIDENCE']?></td>
                                                                </tr>
                                                            <?php 
                                                        }
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                            <?
                                print_r($_SESSION);
                                if(isset($_SESSION["toast"])){
                                    echo '<script type="text/javascript">alert("You have been credited a Ksh '.$_SESSION["commission"].' commission");</script>';
                                    unset($_SESSION['toast']); 
                                }
                                ?>
                    <div id="snackbar">Some text some message..</div>
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>

        <script> $('.toast').toast('show');</script>
        <script src="js/scripts.js"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/datatables-demo.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script>
            function snackBar() {
                var x = document.getElementById("snackbar");
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 12000);
            }
        </script>
    </body>
</html>
