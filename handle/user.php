<?php
    require "db.php";  
    if (isset($_POST["fname"])){
        $fname = ucwords($_POST["fname"]);
        $lname = ucwords($_POST["lname"]);
        $phone = $_POST["phone"];
        $email = $_POST["email"];
        $id = $_POST["id"];
        #$idimg = $_POST["idimg"];
        $kra = $_POST["kra"];
        #$kradoc = $_POST["kradoc"];
        $bank = $_POST["bank"];
        #$bankimg = $_POST["bankimg"];
        $nssf = $_POST["nssf"];
        #$nssfdoc = $_POST["nssfdoc"];
        $nhif = $_POST["nhif"];
        #$nhifdoc = $_POST["nhifdoc"];
        #$signature = $_POST["signature"];

        $docs = array();
        foreach($_FILES['userFiles']['tmp_name'] as $key=>$tmp_name){
            $file_name = $key.$_FILES['userFiles']['name'][$key];
            $file_tmp =$_FILES['userFiles']['tmp_name'][$key];
            $path = '../userfiles/'.time(). $file_name;
            if(move_uploaded_file($file_tmp, $path)){
                array_push($docs, $path);    
            }
            
        }
        $idimg = trim($docs[0], './'); 
        $kradoc = trim($docs[1], './');
        $bankimg = trim($docs[2], './');
        $nssfdoc = trim($docs[3], './');
        $nhifdoc = trim($docs[4], './');
        $signature = trim($docs[5], './');
        
        
        $sql = "INSERT INTO User (
            Fname,
            Lname,
            Phone,
            Email,
            ID_no,
            IDIMG,
            KRA,
            KRADOC,
            Bank_No,
            BANKIMG,
            NSSF,
            NSSFDOC,
            NHIF,
            NHIFDOC,
            Signature 
        ) VALUES (
            '$fname',
            '$lname',
            '$phone',
            '$email',
            '$id',
            '$idimg',
            '$kra',
            '$kradoc',
            '$bank',
            '$bankimg',
            '$nssf',
            '$nssfdoc',
            '$nhif',
            '$nhifdoc',
            '$signature'
        )";
        if (mysqli_query($conn, $sql)) {
            $responce = "User <b>$fname</b> created successfully";
        } else {
            if(strpos(mysqli_error($conn), "Duplicate") !== false){
                $responce = "User <b>$fname</b> Already Exists";
            } else{
                $responce = mysqli_error($conn);
            }
            
        }
        include "alert.php";
        mysqli_close($conn);
        
    }
    
header( "refresh:2;url=../dashboard.php" );