<?php
    require "db.php";
    if (isset($_POST['fname'])){
        $fname  =$_POST['fname'];
        $mname  =$_POST['mname'];
        $lname  =$_POST['lname'];
        $reg  =$_POST['reg'];
        #$regfile  =$_POST['regfile'];
        $kra  =$_POST['kra'];
        #$krafile  =$_POST['krafile'];
        $email  =$_POST['email'];
        $phone  =$_POST['phone'];
        $occupation  =$_POST['occupation'];
        $residence  =$_POST['residence'];
        #$logo  =$_POST['logo'];
        #$logbook  =$_POST['logbook'];

        $docs = array();
        foreach($_FILES['clientFiles']['tmp_name'] as $key=>$tmp_name){
            $file_name = $key.$_FILES['clientFiles']['name'][$key];
            $file_tmp =$_FILES['clientFiles']['tmp_name'][$key];
            $path = '../clientfiles/'.time(). $file_name;
            if(move_uploaded_file($file_tmp, $path)){
                array_push($docs, $path);    
            }
            
        }
        $regfile = trim($docs[0], './'); 
        $krafile = trim($docs[1], './');
        $logo = trim($docs[2], './');
        $logbook = trim($docs[3], './');
       

        $sql = "INSERT into Client ( 
            Fname,
            MNAME, 
            Lname, 
            Cert_for,  
            CertFile,
            KRA_PIN,  
            KRAFILE,
            EMAIL, 
            PHONE,
            OCCUPATION, 
            RESIDENCE, 
            PROFILE_PHOTO, 
            VEHICLE_DETAILS
        ) VALUES (
            '$fname',
            '$mname',
            '$lname',
            '$reg',
            '$regfile',
            '$kra',
            '$krafile',
            '$email',
            '$phone',
            '$occupation',
            '$residence',
            '$logo',
            '$logbook'

        )";
        if (mysqli_query($conn, $sql)) {
            $responce = "Client <b>$fname</b> created successfully";
        } else {
            if(strpos(mysqli_error($conn), "Duplicate") !== false){
                $responce = "Client <b>$fname</b> Exists";
            } else{
                $responce = mysqli_error($conn);
            }
            
        }
        include "alert.php";
        mysqli_close($conn);
    }
    
header( "refresh:2;url=../dashboard.php" );

